#import <UIKit/UIKit.h>
 

@interface UIColor (AventaPlus)

//+ (UIColor*)AP_basicLightColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_basicDarkColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_basicLightJoinButtonColor:(CGFloat)transparent;
+ (UIColor*)AP_backgroundFollowButtonColor:(CGFloat)transparent;
+ (UIColor*)AP_briefTextColorWithTransparent:(CGFloat)transparent;

+ (UIColor*)AP_pagingUnselectedColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_pagingSelectedColorWithTransparent:(CGFloat)transparent;

+ (UIColor*)AP_basicColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_basicDarkTextColorWithTransparent:(CGFloat)transparent;

+ (UIColor*)AP_mediaSelectedBorderColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_borderColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_bulbshareTextPlaceholderColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_imagePlaceholderGrayColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_registerPlaceholderGrayColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_insertIncorrectColorWithTransparent:(CGFloat)transparent;
+ (UIColor*)AP_insertcorrectColorWithTransparent:(CGFloat)transparent;
+ (CGColorRef)AP_borderRedColor;
+ (UIColor*) AP_ToolsGrayColor;
- (BOOL)color:(UIColor *)color1 isEqualToColor:(UIColor *)color2 withTolerance:(CGFloat)tolerance;
    
@end
