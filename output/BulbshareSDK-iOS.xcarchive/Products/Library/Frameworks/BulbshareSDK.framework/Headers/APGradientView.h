#import <UIKit/UIKit.h>

@interface APGradientView : UIView {}
- (void)configureGradientLayerFromTop;
- (void)configureGradientLayerFromBottom;
@end
