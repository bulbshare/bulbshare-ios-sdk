#import <UIKit/UIKit.h>
 
@class APTools;

@interface APTools : NSObject

+ (APTools*)sharedInstance;

- (UIColor*)hexColorForName:(NSString *)name;
- (NSString*)imageForName:(NSString *)name;
- (NSString*)stringForName:(NSString *)name;


//alerts
-(void)showFraudErrorWithTitle:(NSString*)title message:(NSString*)message controller:(UIViewController*)controller;
-(void)showRejectionErrorWithTitle:(NSString*)title message:(NSString*)message controller:(UIViewController*)controller;
-(void)showVPNErrorWithTitle:(NSString*)title message:(NSString*)message controller:(UIViewController*)controller;

-(void)showVideoLengthLimitErrorWithTitle:(NSString*)title message:(NSString*)message controller:(UIViewController*)controller;
@end
