@interface UIImage (AventaPlus)

- (UIImage *)removeRotation;
- (UIImage *)fixOrientation;
- (UIImage *)flipHorizontal;
- (UIImage *)setImageOrientationTo:(int)orientation withDevice:(int)device;
+ (UIImage *)resizeImage:(UIImage*)image toTarget:(CGFloat)target;
+ (UIImage *)imageFromView:(UIView *)theView withSize:(CGSize)size;

@end
