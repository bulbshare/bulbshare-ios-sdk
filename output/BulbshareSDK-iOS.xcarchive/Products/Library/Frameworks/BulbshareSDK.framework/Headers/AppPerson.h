//
//  AppPerson.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 24. 08. 15.
//  Copyright (c) 2015 Aventa Plus d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface AppPerson : Person

+ (AppPerson*)sharedInstance;

@end
