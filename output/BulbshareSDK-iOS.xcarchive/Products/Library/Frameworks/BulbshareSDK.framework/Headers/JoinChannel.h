//
//  JoinChannel.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 23/08/2018.
//  Copyright © 2018 Aventa Plus d.o.o. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface JoinChannel : NSObject

@property int joinChannelId;
@property (nonatomic, strong) NSString* joinChannelRef;
@property (nonatomic, strong) NSString* joinChannelName;
@property (nonatomic, strong) NSString* joinChannelSpaceLeft;
@property (nonatomic, strong) NSString* joinChannelLogoURL;
@property (nonatomic, strong) NSString* joinChannelImageURL;
@property (nonatomic, strong) NSString* joinChannelEligibilityURL;
@property (nonatomic, strong) NSString* joinChannelEligibilityBriefRef;
@property (nonatomic, strong) NSString* joinChannelTitle;
@property (nonatomic, strong) NSString* joinChannelDescription;
@property (nonatomic, strong) NSString* joinChannelConfirmationDescription;
@property int joinChannelState;
@property BOOL joinChannelIsLineSpacerVisible;
@property BOOL joinChannelIsEligibilityVisible;
@property BOOL joinChannelIsAccessCodeVisible;

@end
