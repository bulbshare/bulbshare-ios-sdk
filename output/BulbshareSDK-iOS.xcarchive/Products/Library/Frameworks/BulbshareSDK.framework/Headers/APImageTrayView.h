//
//  APImageTrayView.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 21/07/2017.
//  Copyright © 2017 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Photos;

@class APImageTrayView;
@protocol APImageTrayViewDelegate <NSObject>
    - (void)didSelectImage:(UIImage*)image atIndex:(int)index;
    - (void)didSelectVideo:(AVAsset*)asset withImage:(UIImage*)image atIndex:(int)index;
    - (void)openPhotoLibrary;
    - (void)didSelectImageWithError;
    - (void)didSelectVideoWithError;
    //- (void)didSelectImage:(PHAsset *)asset atIndex:(int)index;
@end

@interface APImageTrayView : UIView <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak) id<APImageTrayViewDelegate> delegate;
@property (nonatomic) int captureMode;

- (void)setData;
- (UIImage*)fetchPhotoForAssetAtIndex:(int)index;

@end

