//
//  Brief.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 03/06/16.
//  Copyright © 2016 Aventa Plus d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "BaseBrand.h"

@interface Brief : NSObject

@property int briefID;
@property (nonatomic, strong) NSString* briefref;

@property (nonatomic, strong) BaseBrand* baseBrand;
@property (nonatomic, strong) BaseBrand* promptBrand;

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* briefDescription;
@property (nonatomic, strong) NSString* bannerImageURL;
@property (nonatomic, strong) NSString* thumbnailBannerImageURL;
@property (nonatomic, strong) NSString* bannerVideoURL;
@property (nonatomic, strong) NSString* bannerText;
@property (nonatomic, strong) NSString* cover_photo;
@property (nonatomic, strong) NSString* cover_video_mobile;
@property (nonatomic, strong) NSString* ad_1_image;
@property (nonatomic, strong) NSString* ad_1_href;
@property (nonatomic, strong) NSString* ad_2_image;
@property (nonatomic, strong) NSString* ad_2_href;
@property (nonatomic, strong) NSString* ad_2_text_overlay;
@property (nonatomic, strong) NSString* reward_image;
@property (nonatomic, strong) NSString* reward_text;
@property BOOL bs_picture_required;
@property (nonatomic, strong) NSString* bs_picture_text;
@property BOOL bs_video_required;
@property (nonatomic, strong) NSString* bs_video_text;
@property BOOL bs_comment_required;
@property (nonatomic, strong) NSString* bs_comment_text;
@property (nonatomic, strong) NSDate* start;
@property (nonatomic, strong) NSDate* end;
@property int briefType; //0 = short video, 1 = long video
@property int likesCount;
@property int commentsCount;
@property int bulbSharesCount; // number of current responses for normal brief
@property int type;
@property int responses_count;  // number of current responses for survey brief
@property int user_responses_count; //number of responses user did send
@property int max_responses_count; //number of responses user can send
@property int max_responses; //number of responses per brief
@property BOOL is_activated;
@property BOOL was_liked_by_me;
@property BOOL can_submit_bulbshares;
@property BOOL can_answer_poll;
@property BOOL is_private;
@property BOOL is_access_granted;
@property BOOL did_submit_response;
@property BOOL is_audition_brief;
@property (nonatomic, strong) NSString* feed_title;
@property (nonatomic, strong) NSString* intro_text;

@end
