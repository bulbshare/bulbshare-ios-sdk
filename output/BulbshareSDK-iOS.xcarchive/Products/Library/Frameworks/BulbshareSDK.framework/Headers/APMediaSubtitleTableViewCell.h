//
//  APMediaSubtitleTableViewCell.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 15/02/2018.
//  Copyright © 2018 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APMediaSubtitleTableViewCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UILabel *textLabel;
@end
