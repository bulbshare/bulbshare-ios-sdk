#import <UIKit/UIKit.h>

@interface APThemeManager : UIView

+ (APThemeManager*) sharedInstance;

- (UIColor*) getColorByName:(NSString*)colorName;
- (void) setColor:(NSMutableDictionary*)colorDict byName:(NSString*)colorName;
- (void) setDefaultColors;
- (void) setNewThemeColors:(NSData*)data forBrand:(NSString*)brandid;

- (void)setCommentStyles;
- (NSMutableArray*)getCommentStyles;
- (UIColor*)getCommentStyleFontColor:(int)index;
- (UIColor*)getCommentStyleBackgroundColor:(int)index;
@end
