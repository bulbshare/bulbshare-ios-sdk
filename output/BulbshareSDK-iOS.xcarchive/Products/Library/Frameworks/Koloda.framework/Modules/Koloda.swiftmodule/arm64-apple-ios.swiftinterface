// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target arm64-apple-ios12.1 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Koloda
import CoreGraphics
import Foundation
@_exported import Koloda
import Swift
import UIKit
import _Concurrency
import pop
public enum DragSpeed : Foundation.TimeInterval {
  case slow
  case moderate
  case `default`
  case fast
  public init?(rawValue: Foundation.TimeInterval)
  public typealias RawValue = Foundation.TimeInterval
  public var rawValue: Foundation.TimeInterval {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) public class DraggableCardView : UIKit.UIView, UIKit.UIGestureRecognizerDelegate {
  @_Concurrency.MainActor(unsafe) public var rotationMax: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var rotationAngle: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var scaleMin: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var contentView: UIKit.UIView? {
    get
  }
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public var frame: CoreGraphics.CGRect {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @objc deinit
  @_Concurrency.MainActor(unsafe) @objc public func gestureRecognizer(_ gestureRecognizer: UIKit.UIGestureRecognizer, shouldReceive touch: UIKit.UITouch) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIKit.UIGestureRecognizer) -> Swift.Bool
}
public enum VisibleCardsDirection : Swift.Int {
  case top, bottom
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol KolodaViewDataSource : AnyObject {
  func kolodaNumberOfCards(_ koloda: Koloda.KolodaView) -> Swift.Int
  func kolodaSpeedThatCardShouldDrag(_ koloda: Koloda.KolodaView) -> Koloda.DragSpeed
  func koloda(_ koloda: Koloda.KolodaView, viewForCardAt index: Swift.Int) -> UIKit.UIView
  func koloda(_ koloda: Koloda.KolodaView, viewForCardOverlayAt index: Swift.Int) -> Koloda.OverlayView?
}
extension Koloda.KolodaViewDataSource {
  public func koloda(_ koloda: Koloda.KolodaView, viewForCardOverlayAt index: Swift.Int) -> Koloda.OverlayView?
  public func kolodaSpeedThatCardShouldDrag(_ koloda: Koloda.KolodaView) -> Koloda.DragSpeed
}
public protocol KolodaViewDelegate : AnyObject {
  func koloda(_ koloda: Koloda.KolodaView, allowedDirectionsForIndex index: Swift.Int) -> [Koloda.SwipeResultDirection]
  func koloda(_ koloda: Koloda.KolodaView, shouldSwipeCardAt index: Swift.Int, in direction: Koloda.SwipeResultDirection) -> Swift.Bool
  func koloda(_ koloda: Koloda.KolodaView, didSwipeCardAt index: Swift.Int, in direction: Koloda.SwipeResultDirection)
  func kolodaDidRunOutOfCards(_ koloda: Koloda.KolodaView)
  func koloda(_ koloda: Koloda.KolodaView, didSelectCardAt index: Swift.Int)
  func kolodaShouldApplyAppearAnimation(_ koloda: Koloda.KolodaView) -> Swift.Bool
  func kolodaShouldMoveBackgroundCard(_ koloda: Koloda.KolodaView) -> Swift.Bool
  func kolodaShouldTransparentizeNextCard(_ koloda: Koloda.KolodaView) -> Swift.Bool
  func koloda(_ koloda: Koloda.KolodaView, draggedCardWithPercentage finishPercentage: CoreGraphics.CGFloat, in direction: Koloda.SwipeResultDirection)
  func kolodaDidResetCard(_ koloda: Koloda.KolodaView)
  func kolodaSwipeThresholdRatioMargin(_ koloda: Koloda.KolodaView) -> CoreGraphics.CGFloat?
  func koloda(_ koloda: Koloda.KolodaView, didShowCardAt index: Swift.Int)
  func koloda(_ koloda: Koloda.KolodaView, didRewindTo index: Swift.Int)
  func koloda(_ koloda: Koloda.KolodaView, shouldDragCardAt index: Swift.Int) -> Swift.Bool
  func kolodaPanBegan(_ koloda: Koloda.KolodaView, card: Koloda.DraggableCardView)
  func kolodaPanFinished(_ koloda: Koloda.KolodaView, card: Koloda.DraggableCardView)
}
extension Koloda.KolodaViewDelegate {
  public func koloda(_ koloda: Koloda.KolodaView, shouldSwipeCardAt index: Swift.Int, in direction: Koloda.SwipeResultDirection) -> Swift.Bool
  public func koloda(_ koloda: Koloda.KolodaView, allowedDirectionsForIndex index: Swift.Int) -> [Koloda.SwipeResultDirection]
  public func koloda(_ koloda: Koloda.KolodaView, didSwipeCardAt index: Swift.Int, in direction: Koloda.SwipeResultDirection)
  public func kolodaDidRunOutOfCards(_ koloda: Koloda.KolodaView)
  public func koloda(_ koloda: Koloda.KolodaView, didSelectCardAt index: Swift.Int)
  public func kolodaShouldApplyAppearAnimation(_ koloda: Koloda.KolodaView) -> Swift.Bool
  public func kolodaShouldMoveBackgroundCard(_ koloda: Koloda.KolodaView) -> Swift.Bool
  public func kolodaShouldTransparentizeNextCard(_ koloda: Koloda.KolodaView) -> Swift.Bool
  public func koloda(_ koloda: Koloda.KolodaView, draggedCardWithPercentage finishPercentage: CoreGraphics.CGFloat, in direction: Koloda.SwipeResultDirection)
  public func kolodaDidResetCard(_ koloda: Koloda.KolodaView)
  public func kolodaSwipeThresholdRatioMargin(_ koloda: Koloda.KolodaView) -> CoreGraphics.CGFloat?
  public func koloda(_ koloda: Koloda.KolodaView, didShowCardAt index: Swift.Int)
  public func koloda(_ koloda: Koloda.KolodaView, didRewindTo index: Swift.Int)
  public func koloda(_ koloda: Koloda.KolodaView, shouldDragCardAt index: Swift.Int) -> Swift.Bool
  public func kolodaPanBegan(_ koloda: Koloda.KolodaView, card: Koloda.DraggableCardView)
  public func kolodaPanFinished(_ koloda: Koloda.KolodaView, card: Koloda.DraggableCardView)
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class KolodaView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) public var alphaValueOpaque: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var alphaValueTransparent: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var alphaValueSemiTransparent: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var shouldPassthroughTapsWhenNoVisibleCards: Swift.Bool
  @_Concurrency.MainActor(unsafe) public var rotationMax: CoreGraphics.CGFloat?
  @_Concurrency.MainActor(unsafe) public var rotationAngle: CoreGraphics.CGFloat?
  @_Concurrency.MainActor(unsafe) public var scaleMin: CoreGraphics.CGFloat?
  @_Concurrency.MainActor(unsafe) public var appearanceAnimationDuration: Foundation.TimeInterval
  @_Concurrency.MainActor(unsafe) public var backgroundCardFrameAnimationDuration: Foundation.TimeInterval
  @_Concurrency.MainActor(unsafe) public var reverseAnimationDuration: Foundation.TimeInterval
  @_Concurrency.MainActor(unsafe) public var countOfVisibleCards: Swift.Int
  @_Concurrency.MainActor(unsafe) public var backgroundCardsTopMargin: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var backgroundCardsScalePercent: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var visibleCardsDirection: Koloda.VisibleCardsDirection
  @_Concurrency.MainActor(unsafe) public var isLoop: Swift.Bool
  @_Concurrency.MainActor(unsafe) public var currentCardIndex: Swift.Int {
    get
  }
  @_Concurrency.MainActor(unsafe) public var countOfCards: Swift.Int {
    get
  }
  @_Concurrency.MainActor(unsafe) weak public var dataSource: Koloda.KolodaViewDataSource? {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) weak public var delegate: Koloda.KolodaViewDelegate?
  @_Concurrency.MainActor(unsafe) public var animator: Koloda.KolodaViewAnimator {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public var isAnimating: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) public var isRunOutOfCards: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @_Concurrency.MainActor(unsafe) public func layoutDeck()
  @_Concurrency.MainActor(unsafe) open func frameForCard(at index: Swift.Int) -> CoreGraphics.CGRect
  @_Concurrency.MainActor(unsafe) public func applyAppearAnimationIfNeeded()
  @_Concurrency.MainActor(unsafe) public func revertAction(direction: Koloda.SwipeResultDirection? = nil)
  @_Concurrency.MainActor(unsafe) public func reconfigureCards()
  @_Concurrency.MainActor(unsafe) public func reloadData()
  @_Concurrency.MainActor(unsafe) public func swipe(_ direction: Koloda.SwipeResultDirection, force: Swift.Bool = false)
  @_Concurrency.MainActor(unsafe) public func resetCurrentCardIndex()
  @_Concurrency.MainActor(unsafe) public func viewForCard(at index: Swift.Int) -> UIKit.UIView?
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func point(inside point: CoreGraphics.CGPoint, with event: UIKit.UIEvent?) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) public func insertCardAtIndexRange(_ indexRange: Swift.CountableRange<Swift.Int>, animated: Swift.Bool = true)
  @_Concurrency.MainActor(unsafe) public func removeCardInIndexRange(_ indexRange: Swift.CountableRange<Swift.Int>, animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) public func reloadCardsInIndexRange(_ indexRange: Swift.CountableRange<Swift.Int>)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
open class KolodaViewAnimator {
  public typealias AnimationCompletionBlock = ((Swift.Bool) -> Swift.Void)?
  public init(koloda: Koloda.KolodaView)
  open func animateAppearance(_ duration: Foundation.TimeInterval, completion: Koloda.KolodaViewAnimator.AnimationCompletionBlock = nil)
  open func applyReverseAnimation(_ card: Koloda.DraggableCardView, direction: Koloda.SwipeResultDirection?, duration: Foundation.TimeInterval, completion: Koloda.KolodaViewAnimator.AnimationCompletionBlock = nil)
  open func applyScaleAnimation(_ card: Koloda.DraggableCardView, scale: CoreGraphics.CGSize, frame: CoreGraphics.CGRect, duration: Foundation.TimeInterval, completion: Koloda.KolodaViewAnimator.AnimationCompletionBlock = nil)
  open func applyAlphaAnimation(_ card: Koloda.DraggableCardView, alpha: CoreGraphics.CGFloat, duration: Foundation.TimeInterval = 0.2, completion: Koloda.KolodaViewAnimator.AnimationCompletionBlock = nil)
  open func applyInsertionAnimation(_ cards: [Koloda.DraggableCardView], completion: Koloda.KolodaViewAnimator.AnimationCompletionBlock = nil)
  open func applyRemovalAnimation(_ cards: [Koloda.DraggableCardView], completion: Koloda.KolodaViewAnimator.AnimationCompletionBlock = nil)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class OverlayView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) open var overlayState: Koloda.SwipeResultDirection?
  @_Concurrency.MainActor(unsafe) open func update(progress: CoreGraphics.CGFloat)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
public enum SwipeResultDirection : Swift.String {
  case left
  case right
  case up
  case down
  case topLeft
  case topRight
  case bottomLeft
  case bottomRight
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
extension Koloda.SwipeResultDirection : Swift.CaseIterable {
  public typealias AllCases = [Koloda.SwipeResultDirection]
  public static var allCases: [Koloda.SwipeResultDirection] {
    get
  }
}
extension Koloda.DragSpeed : Swift.Equatable {}
extension Koloda.DragSpeed : Swift.Hashable {}
extension Koloda.DragSpeed : Swift.RawRepresentable {}
extension Koloda.VisibleCardsDirection : Swift.Equatable {}
extension Koloda.VisibleCardsDirection : Swift.Hashable {}
extension Koloda.VisibleCardsDirection : Swift.RawRepresentable {}
extension Koloda.SwipeResultDirection : Swift.Equatable {}
extension Koloda.SwipeResultDirection : Swift.Hashable {}
extension Koloda.SwipeResultDirection : Swift.RawRepresentable {}
