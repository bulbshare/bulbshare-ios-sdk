import Foundation
/// this meathod defines the structure class which has - testID,profileID,partnerID
public struct BSAuthenticationInput: Encodable {
    let email: String
    let password: String

    /// this meathod is used to initialize the IDs getting from API
    /// - Parameters:
    ///   - email: it is the email
    ///   - password: it is the password
    public init(email: String, password: String) {
        self.email = email
        self.password = password
    }
}
