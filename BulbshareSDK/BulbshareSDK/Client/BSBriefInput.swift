import Foundation
/// this meathod defines the structure class which has - privateBrandId
public struct BSBriefInput: Encodable{
    let privateBrandId: Int
    
    /// this method is used to initialize the IDs getting from API
    /// - Parameters:
    ///   - privateBrandId: it is the privateBrandId
    public init(privateBrandId: Int){
        self.privateBrandId = privateBrandId
        
    }
}
