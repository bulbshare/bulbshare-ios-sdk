import Foundation
/// this meathod defines the structure class which has - bulbshareRef
public struct BSSingleBulbshareInput: Encodable {
    let bulbshareRef: String

    /// this meathod is used to initialize the IDs getting from API
    /// - Parameters:
    ///   - bulbshareRef: it is the brief Ref ID
    public init(bulbshareRef: String) {
        self.bulbshareRef = bulbshareRef
    }
}
