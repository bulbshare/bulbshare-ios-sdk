import Foundation
/// this meathod defines the structure class which has - privateBrandId
public struct BSBulbshareListInput: Encodable{
    let briefRef: String
    let count: Int
    let sortType: Int

    /// this meathod is used to initialize the IDs getting from API
    /// - Parameters:
    ///   - briefRef: it is the brief Ref ID
    public init(briefRef: String, count: Int, sortType: Int) {
        self.briefRef = briefRef
        self.count = count
        self.sortType = sortType
    }
}
