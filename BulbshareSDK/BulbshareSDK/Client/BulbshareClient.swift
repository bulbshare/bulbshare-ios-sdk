import UIKit


var sdkConfig: BSSDKConfig = BSSDKConfig()

public class BulbshareClient {
    private static var sharedInstance: BulbshareClient!
    private init(config: BSSDKConfig) {
        sdkConfig = config
        BulbshareClient.sharedInstance = self
    }
    public static func shared(config: BSSDKConfig) -> BulbshareClient {
        switch sharedInstance {
        case let instance?:
            sdkConfig = config
            return instance
        default:
            sharedInstance = BulbshareClient(config: config)
            return sharedInstance
        }
    }
    // MARK: User Authentication
    /**
     lazy for authenitcation
     */
    fileprivate lazy var userAuthentication: UserAuthenticationInteractor = {
        let provider = UserAuthenticationInteractor(delegate: self)
        return provider
    }()
    
    /**
     This delegate is for user authentication protocols methods which will be used to send data from SDK to client app
     */
    public weak var authenticationDelegate: BSAuthenticatioinDelegate?
    
    /**
     This method is responsible to authenticate user
     */
    public func authenticate(input: BSAuthenticationInput) {
        //   if userAuthentication.checkIfUserAlreadyLoggedin() == false {
        userAuthentication.authenticate(input: input)
        //  }
    }
    
    // MARK: Show Feed
    /**
     lazy for Feed
     */
    
    fileprivate lazy var feedInteractor: FeedInteractor = {
        let provider = FeedInteractor(delegate: self)
        return provider
    }()
    /**
     This delegate is for Feed protocols methods which will be used to send data from SDK to client app
     */
    public weak var feedDelegate: BSFeedDelegate?
    /**
     This method is responsible to show feed
     */
    public func showFeed(navigation: UINavigationController,input: BSBriefInput) {
        feedInteractor.showFeeds(navigation: navigation, input: input)
    }
    
    // MARK: - Show Poll Brief data
    fileprivate lazy var pollBriefInteractor: PollBriefInteractor = {
        let provider = PollBriefInteractor(delegate: self)
        return provider
    }()
    
    /**
     This delegate is for poll brief protocols methods which will be used for call back for success or failure  from SDK to client app
     */
    public weak var pollBriefDelegate: BSPollBriefDdelegate?
    
    
    public func showPollBrief(navigation: UINavigationController, input: String) {
        pollBriefInteractor.showPollBrief(navigation: navigation, briefID: input)
    }
    
    // MARK: Show Brief
    /**
     lazy for Brief
     */
    fileprivate lazy var briefInteractor: BriefInteractor = {
        let provider = BriefInteractor(delegate: self)
        return provider
    }()
    /**
     This delegate is for Brief protocols methods which will be used to send data from SDK to client app
     */
    public weak var briefDelegate: BSBriefDelegate?
    /**
     This method is responsible to show Brief
     */
    public func showBrief(navigation: UINavigationController,input: BSBriefInput){
        briefInteractor.showBrief(navigation: navigation, input: input)
    }
    
    // MARK: Show Brief Intro
    fileprivate lazy var briefIntroInteractor: BriefIntroInteractor = {
        
        let provider = BriefIntroInteractor(delegate: self)
        return provider
    }()
    
    public weak var briefIntroDelegate: BSBriefIntroDelegate?
    
    public func showBriefIntro(navigation: UINavigationController, input: BSBriefRefInput){
        briefIntroInteractor.showBriefIntro(navigation: navigation, briefID: input.briefRef)
    }
    // MARK: Show Single Bulbshare
    fileprivate lazy var singleBulbshareInteractor: SingleBulbshareInteractor = {
        
        let provider = SingleBulbshareInteractor(delegate: self)
        return provider
    }()
    
    public weak var singleBulbshareDelegate: BSSingleBulbshareDelegate?
    
    public func showSingleBulbshare(navigation: UINavigationController, input: BSSingleBulbshareInput){
        singleBulbshareInteractor.showSingleBulbshare(navigation: navigation, input: input)
    }
    
    // MARK: Show Bulbshare List
        /**
            lazy for Bulbshare List
         */
        fileprivate lazy var bulbshareListInteractor: BulbshareListInteractor = {
            let provider = BulbshareListInteractor(delegate: self)
            return provider
        }()
        /**
         This delegate is for Bulbshare List protocols methods which will be used to send data from SDK to client app
         */
        public weak var bulbshareListDelegate: BSBulbshareDelegate?
        /**
         This method is responsible to show Bulbshare
         */
        public func showBulbshareList(navigation: UINavigationController, input: BSBulbshareListInput){
            bulbshareListInteractor.showBulbshareList(navigation: navigation, input: input)
        }

    
    // MARK: Show Brief Comment
    fileprivate lazy var commentInteractor: CommentViewInteractor = {
        
        let provider = CommentViewInteractor(delegate: self)
        return provider
        
    }()
    
    public weak var briefCommentDelegate: BSBriefCommentDelegate?
    
    public func showBriefComment(navigation: UINavigationController, input: BSBriefRefInput){
        commentInteractor.showCommentView(navigation: navigation, briefID: input.briefRef)
    }
}
// MARK: User Authentication Delegate
extension BulbshareClient: UserAuthenticationDelegate {
    
    /**
     Callback delegate for authentication to the SDK
     */
    func authenticateSuccessfully() {
        self.authenticationDelegate?.onSuccessAuthentication()
        // do some steps
    }
    func throwError(error: Error) {
        DispatchQueue.main.async {
            self.authenticationDelegate?.onSDKError(error: error)
        }
    }
    
}

// MARK: Poll Brief Response Delegate
extension BulbshareClient: PollBriefResponseDelegate {
    func throwError(error: SDKError) {
        self.pollBriefDelegate?.onSDKError(error: error)
    }
    
    func gotPollBriefDetailsSuccessfully() {
        self.pollBriefDelegate?.onSuccessPollBriefLoaded()
    }
}

// MARK: Brief Delegate
extension BulbshareClient: BriefResponseDelegate {
    func briefThrowError(error: SDKError) {
        self.briefDelegate?.onSDKError(error: error)
    }
    
    func briefDataGotSuccessfully() {
        self.briefDelegate?.onSuccessBriefLoaded()
    }
}

// MARK: Bulbshare List Delegate
extension BulbshareClient: BulbshareListResponseDelegate {
    func bulbshareListDataGotSuccessfully() {
        self.bulbshareListDelegate?.onSuccessBriefLoaded()
    }
    
    func bulbshareListThrowError(error: SDKError) {
        self.bulbshareListDelegate?.onSDKError(error: error)
    }
}

// MARK: Brief Intro Delegate
extension BulbshareClient: BriefIntroResponseDelegate {
    func showPollBrief(id: String, navigation: UINavigationController) {
        self.showPollBrief(navigation: navigation, input: id)
    }
    
    func briefIntroThrowError(error: SDKError) {
        self.briefIntroDelegate?.onSDKError(error: error)
    }
    
    func briefIntroDataGotSuccessfully() {
        self.briefIntroDelegate?.onSuccessBriefIntroLoaded()
    }
}

// MARK: Single Bulbshare Delegate
extension BulbshareClient: SingleBulbshareResponseDelegate {
    func singleBulbshareDataGotSuccessfully() {
        self.singleBulbshareDelegate?.onSuccessSingleBulbshareLoaded()
    }
    
    func singleBulbshareThrowError(error: SDKError) {
        self.singleBulbshareDelegate?.onSDKError(error: error)
    }
}

// MARK: Brief  Comment Delegate
extension BulbshareClient: BriefCommentResponseDelegate {
    func briefCommentThrowError(error: SDKError) {
        self.briefCommentDelegate?.onSDKError(error: error)
    }
    
    func briefCommentDataGotSuccessfully() {
        self.briefCommentDelegate?.onSuccessBriefCommentLoaded()
    }
}

// MARK: Brief  Feed Delegate
extension BulbshareClient: FeedResponseDelegate {
    func feedDataGotOnSuccess() {
        self.feedDelegate?.onSuccessFeedLoaded()
    }
    
    func feedThrowError(error: SDKError) {
        self.feedDelegate?.onSDKError(error: error)
    }
    
    func showBriefIntroController(value: String, navigation: UINavigationController) {
        briefIntroInteractor.showBriefIntro(navigation: navigation, briefID: value)
    }    
}
