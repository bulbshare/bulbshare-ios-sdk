import Foundation
/// this meathod defines the structure class which has - testID,profileID,partnerID
public struct BSBriefRefInput: Encodable {
    let briefRef: String

    /// this meathod is used to initialize the IDs getting from API
    /// - Parameters:
    ///   - briefRef: it is the brief Ref ID
    public init(briefRef: String) {
        self.briefRef = briefRef
    }
}
