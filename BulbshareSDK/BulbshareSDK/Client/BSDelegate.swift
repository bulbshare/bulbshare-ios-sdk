import Foundation

/**
 Login delegate from the SDK
 */
public protocol BSAuthenticatioinDelegate: BSErrorDelegate {

    /**
     Callback delegate if login successfully to the SDK
     */
    func onSuccessAuthentication()

}
public protocol BSBriefDelegate: BSErrorDelegate {
    /**
     Callback delegate if Brief Loaded successfully to the SDK
     */

    func onSuccessBriefLoaded()
}

public protocol BSErrorDelegate: AnyObject {
    /**
     Callback delegate for throwing an error if occured any
     
     - parameter error: Error
     */
    func onSDKError(error: Error)
    
}

public protocol BSBulbshareDelegate: BSErrorDelegate {
    /**
     Callback delegate if Bulbshare Loaded successfully to the SDK
     */
    
    func onSuccessBriefLoaded()
}


public protocol BSPollBriefDdelegate: BSErrorDelegate {

    /**
     Callback delegate if login successfully to the SDK
     */
    func onSuccessPollBriefLoaded()

}

public protocol BSBriefIntroDelegate: BSErrorDelegate {
    
    func onSuccessBriefIntroLoaded()
    
}
public protocol BSSingleBulbshareDelegate: BSErrorDelegate {
    
    /**
     Callback delegate if Single Bulbshare Loaded successfully to the SDK
     */
    
    func onSuccessSingleBulbshareLoaded()
    
}

public protocol BSBriefCommentDelegate: BSErrorDelegate {
    
    func onSuccessBriefCommentLoaded()
    
}

public protocol BSFeedDelegate: BSErrorDelegate {

    /**
     Callback delegate if Feed Loaded successfully to the SDK
     */
    func onSuccessFeedLoaded()

}
