import Foundation
class ReportPresenter {
    // MARK: - Local Variables
    fileprivate weak var delegate: SingleBulbshareDelegate?
    fileprivate var singleBulbshareManger: SingleBulbshareManager = SingleBulbshareManager()
    var reasonArray = [Report.reportReasonOne,Report.reportReasonTwo,Report.reportReasonThree,Report.reportReasonFour,Report.reportReasonFive,Report.reportReasonSix,Report.reportReasonSeven,Report.reportReasonEight,Report.reportReasonNine,Report.reportReasonTen]
    func reportBulbshare(ref: String,reason: String){
        singleBulbshareManger.reportBulbshare(ref: ref, reason: reason) { [weak self] (response,error) in
            if response != nil{
                Logger.debug(arg: "\(String(describing: response))")
            } else if let errorTemp = error as? SDKError{
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
}
