import Foundation

enum ReportBulbshareAPI{
    case reportBulbshare(model: APIRequest<ReportBulbshareRequestModel>)
}
extension ReportBulbshareAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .reportBulbshare:
            return "reportinappropriatecontent"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .reportBulbshare(let model):
            return ReportBulbshareRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
