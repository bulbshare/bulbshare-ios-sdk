import Foundation
struct ReportBulbshareRequestModel: Codable {
    let ref: String
    let reason: String
    
}

class ReportBulbshareRequest {
    func params(model: APIRequest<ReportBulbshareRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
