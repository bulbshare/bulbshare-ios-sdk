import Foundation
class ReportBulbshareManager{
    fileprivate var service: ReportBulbshareService = ReportBulbshareService()
    func reportBulbshare(ref: String,reason: String, completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        let arg = ReportBulbshareRequestModel(ref: ref, reason: reason)
        let model = APIRequest<ReportBulbshareRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.reportBulbshare(model: model) { response, error in
            completion(response,error)
        }
    }
}
