import Foundation
class ReportBulbshareService {
    private let reportBulbshareRouter = ServiceManager<ReportBulbshareAPI>()
    func reportBulbshare(
        model: APIRequest<ReportBulbshareRequestModel>,
        completion: @escaping (_ response: Bool?,
                               _ error: Error?) -> Void) {
        reportBulbshareRouter.request(.reportBulbshare(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard (response as? Data) != nil else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                completion(true, nil)
            }
        }
    }
}
