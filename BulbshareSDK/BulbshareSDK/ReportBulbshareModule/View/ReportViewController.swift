import UIKit

class ReportViewController: BaseViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var topHeading: UILabel!
    @IBOutlet weak var reportTableView: UITableView!
    fileprivate var presenter: ReportPresenter!
    // MARK: - Local Variables
    var bulbshareRef: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ReportPresenter()
        self.reportTableView.delegate = self
        self.reportTableView.dataSource = self
        registerCell()
        }
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func registerCell(){
        self.reportTableView.register(UINib(nibName: Report.reportTableCell, bundle: SDKBundles.sdkBundle), forCellReuseIdentifier: Report.reportTableCell)
    }
    
}
extension ReportViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.reasonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Report.reportTableCell, for: indexPath) as? ReportTableViewCell else{
            return UITableViewCell()
        }
        cell.reasonLbl.text = self.presenter.reasonArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showAlertController(withTitle: Alert.reportTitle, message: Alert.reportMessage, acceptTitle: Alert.reportAccept, acceptCompletionBlock: {
            [weak self] in
            self?.presenter.reportBulbshare(ref: (self?.bulbshareRef)!, reason: (self?.presenter.reasonArray[indexPath.row])!)
            self?.dismiss(animated: true, completion: nil)
        }, cancelTitle: Alert.Cancel, cancelCompletionBlock: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
