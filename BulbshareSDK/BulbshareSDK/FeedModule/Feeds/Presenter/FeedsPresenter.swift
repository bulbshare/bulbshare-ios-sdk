import Foundation

class FeedsPresenter {
    fileprivate weak var delegate: FeedsDelegate?
    fileprivate var feedsHeadingTitles = ["BRIEFS","BULBSHARES"]
    init(delegate: FeedsDelegate) {
        self.delegate = delegate
    }
    func getHeadingTitles(){
        self.delegate?.getFeeds(data: feedsHeadingTitles)
    }
}
