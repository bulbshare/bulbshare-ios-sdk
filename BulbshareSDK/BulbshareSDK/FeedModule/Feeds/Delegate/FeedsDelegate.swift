import Foundation

protocol FeedsDelegate: AnyObject {
    func getFeeds(data: [String])
}
