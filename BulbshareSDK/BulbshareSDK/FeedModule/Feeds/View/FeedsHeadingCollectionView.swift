import UIKit

class FeedsHeadingCollectionView: UICollectionView{
    var model: [String]!
    var selectedIndex = 1
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        registerCell()
    }
    func bindFeeds(model: [String]?){
        if let intromodel = model{
            self.model = intromodel
        }
    }
    func registerCell(){
        self.register(UINib(nibName: Feeds.FeedsHeadingCollectionViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: Feeds.FeedsHeadingCollectionViewCell)
    }
}
extension FeedsHeadingCollectionView: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Feeds.FeedsHeadingCollectionViewCell, for: indexPath) as! FeedsHeadingCollectionViewCell
        cell.bind(headingName: self.model[indexPath.row])
        if indexPath.row == selectedIndex{
            cell.isSelected = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.reloadData()
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
        self.selectedIndex = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Int(UIScreen.main.bounds.width)/self.model.count, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
