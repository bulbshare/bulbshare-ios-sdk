import UIKit

class FeedViewController: BaseViewController{
    @IBOutlet weak var briefView: UIView!
    @IBOutlet weak var feedsHeadingCollectionView: FeedsHeadingCollectionView!
    var presenter: FeedsPresenter!
    var input: BSBriefInput!
    var joinButtonHandler: ((String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = FeedsPresenter(delegate: self)
        presenter.getHeadingTitles()
        loadBrief()
    }
    func loadBrief(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Brief", bundle: SDKBundles.sdkBundle)
        let vc = storyBoard.instantiateViewController(withIdentifier: "BriefViewController") as! BriefViewController
        vc.input = self.input
        
        vc.feedJoinButtonHandler = { [weak self] value in
            self?.joinButtonHandler?(value)
        }
        self.addChild(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: self.briefView.frame.size.width, height: self.briefView.frame.size.height)
        self.briefView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
}
extension FeedViewController: FeedsDelegate {
    func getFeeds(data: [String]) {
        feedsHeadingCollectionView.bindFeeds(model: data)
        self.callBackToInteractor?(true, nil)
    }
}
