import UIKit
class FeedInteractor {
    
    fileprivate weak var delegate: FeedResponseDelegate!
    
    init(delegate: FeedResponseDelegate) {
        self.delegate = delegate
    }
    
    func showFeeds(navigation: UINavigationController,input: BSBriefInput) {
        let storyBoard: UIStoryboard = UIStoryboard(name: Feeds.Feed, bundle: SDKBundles.sdkBundle)
        let viewController = storyBoard.instantiateViewController(withIdentifier: Feeds.FeedViewController) as! FeedViewController
        viewController.input = input
        viewController.callBackToInteractor  = { [weak self] (value, error) in
            if value {
                self?.delegate.feedDataGotOnSuccess()
            } else if let error = error {
                self?.delegate.feedThrowError(error: error)
            }
        }
        viewController.joinButtonHandler = { [weak self] value in
            self?.delegate.showBriefIntroController(value: value, navigation: navigation)
        }
        navigation.pushViewController(viewController, animated: false)
    }
}
