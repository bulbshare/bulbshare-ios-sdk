import UIKit
class FeedsHeadingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var headingName: UILabel!
    override var isSelected: Bool {
        didSet {
            if isSelected == true{
                headingName.textColor = UIColor(red: 62.0/255.0, green: 61.0/255.0, blue: 74.0/255.0, alpha: 1.0)
                bottomView.isHidden = false
                bottomView.backgroundColor = UIColor(red: 74.0/255.0, green: 223.0/255.0, blue: 224.0/255.0, alpha: 1.0)
            } else{
                headingName.textColor = UIColor(red: 162.0/255.0, green: 162.0/255.0, blue: 168.0/255.0, alpha: 1.0)
                bottomView.isHidden = true
            }
        }
    }
    func bind(headingName: String){
        self.headingName.text = headingName
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
