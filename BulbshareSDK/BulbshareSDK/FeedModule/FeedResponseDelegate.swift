import Foundation
import UIKit


protocol FeedResponseDelegate: AnyObject {
    /**
     Callback delegate if data load successfully
     */
    func feedDataGotOnSuccess()
    
    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func feedThrowError(error: SDKError)
    
    /**
     Callback delegate to show brief
     
     - parameter value: String
     */
    func showBriefIntroController(value: String, navigation: UINavigationController)
}
