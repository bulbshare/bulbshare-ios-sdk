import UIKit

class BulbshareListBaseCollectionViewCell: UICollectionViewCell {
    var callBackToController:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func setData(model: BulbshareModel, row: Int) {
    }
    
     
    func validate() {
    }
    
    func setBulbshareHeaderData(model: BulbshareModel?){
        
    }
    
    func setBulbshareDetailData(model: [BulbshareViewModel],row: Int,currentVC: CurrentVC){
        
    }
    func videoPlay(videoFile: String) {
        
    }
    func videoPause() {
        
    }
}
