import Foundation
import UIKit

class BulbshareListPresenter {
    // MARK: - Variables
    fileprivate weak var delegate: BulbshareListDelegate!
    fileprivate var bulbshareListManager: BulbshareListManager = BulbshareListManager()
    fileprivate var singleBulbshareManger: SingleBulbshareManager = SingleBulbshareManager()
    var respArray: [[BulbshareViewModel]] = []
    // MARK: - Initializers
    init(delegate: BulbshareListDelegate){
        self.delegate = delegate
    }
    /**
     this method is used to get bulbshare list
     */
    func getBulbshareList(input: BSBulbshareListInput,currentPage: Int){
        if currentPage == 1{
            self.delegate?.showLoader()
            self.respArray.removeAll()
        }
        bulbshareListManager.getBulbshareList(input: input, page: currentPage) { [weak self] (response, error) in
            self?.delegate?.hideLoader()
            if let response = response{
                self?.mapBulbshareView(model: response)
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    
    func mapBulbshareView(model: BulbshareListModel) {
        let result = BulbshareListMapper.map(model: model)
        let responseData = result.0
        let row = result.1
        print("Response: \(responseData.count)")
        //self.respArray = responseData
        self.respArray += responseData
        self.delegate?.setupView(data: responseData, numberOfRows: row)
    }
    
    func getLikeUnlike(ref: String,wasLikedByMe: Bool, index: Int){
        if wasLikedByMe{
            singleBulbshareManger.likeUnlikeBulbshare(ref: ref, type: "unlike") { [weak self] (response,error) in
                if response != nil{
                    Logger.debug(arg: "\(String(describing: response))")
                } else if let errorTemp = error as? SDKError {
                    self?.delegate?.showError(error: errorTemp)
                }
            }
            // 3 lines
        } else {
            singleBulbshareManger.likeUnlikeBulbshare(ref: ref, type: "like") { [weak self] (response,error) in
                if response != nil{
                    Logger.debug(arg: "\(String(describing: response))")
                } else if let errorTemp = error as? SDKError {
                    self?.delegate?.showError(error: errorTemp)
                }
            }
            // 3 lines
        }
        // 2 lines
        //self.respArray[index].model?.bulbshares.first.wasLikedByMe = !wasLikedByMe
        //self.delegate.updateBulbshareList(data: respArray, index: index)
    }
    func deleteBulbshare(ref: String,index:Int){
        singleBulbshareManger.deleteBulbshare(ref: ref) { [weak self] (response,error) in
            if response != nil{
                Logger.debug(arg: "\(String(describing: response))")
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
        self.delegate.updateBulbshareList(data: self.respArray, index: index)
    }
}
