import Foundation
import UIKit

class BulbshareViewModel {
    var model: BulbshareListModel?
    var cellType: BulbshareListBaseCollectionViewCell.Type = BulbshareListBaseCollectionViewCell.self
    var cellHeight: CGFloat
    var rows: Int
    var image: String
    var comment: String
    var video: String
    var fontColor: Color
    var bgColor: Color

    // MARK: - initializer
    init(model: BulbshareListModel? = nil, cellType: BulbshareListBaseCollectionViewCell.Type, cellHeight: CGFloat, rows: Int = 0, image: String = "", comment: String = "", video: String = "", fontColor: Color = Color(r: 0, g: 0, b: 0, a: 1), bgColor: Color = Color(r: 0, g: 0, b: 0, a: 1)) {
        self.model = model
        self.cellType = cellType
        self.cellHeight = cellHeight
        self.rows = rows
        self.image = image
        self.comment = comment
        self.video = video
        self.fontColor = fontColor
        self.bgColor = bgColor
    }
}
