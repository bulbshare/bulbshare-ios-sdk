import Foundation

enum BulbshareAPI{
    case getBulbshare(model: APIRequest<BulbshareListRequestModel>)
}
extension BulbshareAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .getBulbshare:
            return "getbulbsharesandthemebybrief"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .getBulbshare(let model):
            return BulbshareRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
