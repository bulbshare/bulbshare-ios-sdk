import Foundation


// MARK: - BulbshareListModel
struct BulbshareListModel: Codable {
    let brandTheme: Theme
    let bulbshares: [BulbshareModel]

    enum CodingKeys: String, CodingKey {
        case brandTheme = "brand_theme"
        case bulbshares
    }
}

// MARK: - BulbshareModel
struct BulbshareModel: Codable {
    let bulbshareref: String
    let user: BulbshareUser
    let brief: BriefData
    let picture, video, title, comment: String
    let commentStyle: CommentStyle
    var likesCount, commentsCount: Int
    let submittedOn: String
    let wasLikedByMe, isSharable: Bool

    enum CodingKeys: String, CodingKey {
        case bulbshareref, user, brief, picture, video, title, comment
        case commentStyle = "comment_style"
        case likesCount = "likes_count"
        case commentsCount = "comments_count"
        case isSharable = "is_shareable"
        case submittedOn = "submitted_on"
        case wasLikedByMe = "was_liked_by_me"
    }
}
