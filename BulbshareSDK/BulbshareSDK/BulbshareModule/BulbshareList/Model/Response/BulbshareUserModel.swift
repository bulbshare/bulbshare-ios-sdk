import Foundation

// MARK: - BulbshareUser
struct BulbshareUser: Codable {
    let userref, name, avatar, displayName: String
    let likesCount, followersCount, bulbsharesCount, followFriendsCount: Int
    let followBrandsCount: Int

    enum CodingKeys: String, CodingKey {
        case userref, name, avatar
        case displayName = "display_name"
        case likesCount = "likes_count"
        case followersCount = "followers_count"
        case bulbsharesCount = "bulbshares_count"
        case followFriendsCount = "follow_friends_count"
        case followBrandsCount = "follow_brands_count"
    }
}
