import Foundation

// MARK: - CommentStyle
struct CommentStyle: Codable {
    let bgColor, fontColor: Color
    let textAlign: String?

    enum CodingKeys: String, CodingKey {
        case bgColor = "bg_color"
        case fontColor = "font_color"
        case textAlign = "text_align"
    }
}
