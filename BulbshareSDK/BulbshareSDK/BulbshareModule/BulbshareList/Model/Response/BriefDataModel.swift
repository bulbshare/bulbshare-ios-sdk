import Foundation

// MARK: - BriefData
struct BriefData: Codable {
    let briefref, name, coverPhoto, coverVideoMobile: String
    let coverVideoWeb, bannerImage, feedTitle: String
    let likesCount, bulbsharesCount: Int
    let isPrivate: Bool
    let brandname, introText: String

    enum CodingKeys: String, CodingKey {
        case briefref, name
        case introText = "intro_text"
        case feedTitle = "feed_title"
        case coverPhoto = "cover_photo"
        case coverVideoMobile = "cover_video_mobile"
        case coverVideoWeb = "cover_video_web"
        case bannerImage = "banner_image"
        case likesCount = "likes_count"
        case bulbsharesCount = "bulbshares_count"
        case isPrivate = "is_private"
        case brandname
    }
}
