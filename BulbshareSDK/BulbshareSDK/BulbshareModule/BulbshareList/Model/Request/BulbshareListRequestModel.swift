import Foundation

// MARK: - BulbshareListRequestModel
struct BulbshareListRequestModel: Codable {
    let briefref: String
    let count, page, sortType: Int

    enum CodingKeys: String, CodingKey {
        case briefref, page, count
        case sortType = "sort_type"
    }
}

class BulbshareRequest {
    func params(model: APIRequest<BulbshareListRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
