/**
 Login callbacks from the SDK
 */
protocol BulbshareListResponseDelegate: AnyObject {
    /**
     Callback delegate if data load successfully
     */
   func bulbshareListDataGotSuccessfully()

    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func bulbshareListThrowError(error: SDKError)
}
