import Foundation
import UIKit

class BulbshareListInteractor {
    
    fileprivate weak var delegate: BulbshareListResponseDelegate!
    
    init(delegate: BulbshareListResponseDelegate) {
        self.delegate = delegate
    }
    
    func showBulbshareList(navigation: UINavigationController,input: BSBulbshareListInput) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "BulbshareList", bundle: SDKBundles.sdkBundle)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "BulbshareListViewController") as! BulbshareListViewController
        viewController.input = input
        viewController.callBackToInteractor  = { [weak self] (value, error) in
            if value {
                self?.delegate.bulbshareListDataGotSuccessfully()
            } else if let error = error {
                self?.delegate.bulbshareListThrowError(error: error)
            }
        }
        navigation.pushViewController(viewController, animated: false)
    }
}
