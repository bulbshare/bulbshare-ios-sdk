import Foundation

class BulbshareListManager {
    fileprivate var service: BulbshareListService = BulbshareListService()
    func getBulbshareList(
        input: BSBulbshareListInput,page: Int,
        completion: @escaping (_ response: BulbshareListModel?,
                               _ error: Error?) -> Void) {
        let arg = BulbshareListRequestModel(briefref: input.briefRef, count: input.count, page: page, sortType: input.sortType)
        let model = APIRequest<BulbshareListRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.getBulbshareList(model: model) { response, error in
            if let responses = response {
                completion(responses, error)
            } else {
                completion(nil, error)
            }
        }
    }
}
