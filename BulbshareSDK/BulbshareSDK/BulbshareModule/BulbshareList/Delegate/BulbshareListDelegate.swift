import Foundation

protocol BulbshareListDelegate: BaseDelegate {
    func getPaginationBulbshareList(pageindex: Int)
    func updateBulbshareList(data: [[BulbshareViewModel]]?,index: Int?)
    func setupView(data: [[BulbshareViewModel]], numberOfRows: [Int])
}

protocol BulbshareDelegate: BaseDelegate{
    func closeBriefIntroView()
}
