import Foundation

class BulbshareListService {
    private let router = ServiceManager<BulbshareAPI>()
    func getBulbshareList(
        model: APIRequest<BulbshareListRequestModel>,
        completion: @escaping (_ response: BulbshareListModel?,
                               _ error: Error?) -> Void) {
        router.request(.getBulbshare(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<BulbshareListModel>.self, from: response)
                Logger.debug(arg: "makeAuthenticationAPICall: \(responseData.data)")
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            } catch DecodingError.keyNotFound(let key, let context) {
                Logger.debug(arg: "could not find key \(key) in JSON: \(context.debugDescription)")
            } catch DecodingError.valueNotFound(let type, let context) {
                Logger.debug(arg: "could not find type \(type) in JSON: \(context.debugDescription)")
            } catch DecodingError.typeMismatch(let type, let context) {
                Logger.debug(arg: "type mismatch for type \(type) in JSON: \(context.debugDescription)")
            } catch DecodingError.dataCorrupted(let context) {
                Logger.debug(arg: "data found to be corrupted in JSON: \(context.debugDescription)")
            } catch let error as NSError {
                Logger.debug(arg: "Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)")
            }
        }
    }
}
