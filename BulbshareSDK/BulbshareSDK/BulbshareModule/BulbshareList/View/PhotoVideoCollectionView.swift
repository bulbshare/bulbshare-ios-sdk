import Foundation
import UIKit

class PhotoVideoCollectionView: UICollectionView {
    var model: [BulbshareListModel] = []
    var viewModel: [BulbshareViewModel]!
    weak var bulbshareDelegate: BulbshareDelegate?
    var numberOfRow = [Int]()
    var currentCell: Int = 0
    var pageHandler: ((Int) -> Void)?
    var currentVC: CurrentVC!
    var sliderHandler: ((IndexPath) -> Void)?
    var currentSelectedIndex: IndexPath = IndexPath(row: 0, section: 0)
    var videoLink: String = ""
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        registerCell()
    }
    func registerCell(){
        self.register(UINib(nibName: "TextCollectionViewCell", bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: "TextCollectionViewCell")
        self.register(UINib(nibName: "PhotoCollectionViewCell", bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        self.register(UINib(nibName: "VideoCollectionViewCell", bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: "VideoCollectionViewCell")
    }
    func bindBulbshare(model: [BulbshareViewModel], row: [Int], currentCell: Int, currentVC: CurrentVC){
        self.currentCell = currentCell
        self.viewModel = model
        self.currentVC = currentVC
        self.numberOfRow = row
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}
extension PhotoVideoCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if currentCell < numberOfRow.count {
            return numberOfRow[currentCell]
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: BulbshareListBaseCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: self.viewModel[indexPath.row].cellType), for: indexPath) as? BulbshareListBaseCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.setBulbshareDetailData(model: viewModel, row: indexPath.row, currentVC: self.currentVC)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageHandler?(indexPath.row)
        if let videoCell = cell as? VideoCollectionViewCell  {
            videoCell.videoPlay(videoFile: self.viewModel[indexPath.row].video)
            self.currentSelectedIndex = indexPath
            self.videoLink = self.viewModel[indexPath.row].video
        }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let videoCell = cell as? VideoCollectionViewCell  {
            videoCell.videoPause()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: self.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
           var visibleRect = CGRect()
               visibleRect.origin = self.contentOffset
               visibleRect.size = self.bounds.size
               let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
               guard let indexPath = self.indexPathForItem(at: visiblePoint) else { return }
           self.sliderHandler?(indexPath)
       }
}
