import UIKit
import AVKit

class VideoCollectionViewCell: BulbshareListBaseCollectionViewCell {

    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var videoView: UIView!
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    override func awakeFromNib() {
        super.awakeFromNib()
        reloadButton.isHidden = false
    }
    override func setBulbshareDetailData(model: [BulbshareViewModel], row: Int, currentVC: CurrentVC) {
        if model[row].video == "" {
            reloadButton.isHidden = false
        } else {
            reloadButton.isHidden = true
            self.videoView.frame = self.bounds
            self.videoView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            let videoUrl = URL(string: model[row].video)
            player = AVPlayer(url: videoUrl!)
            playerLayer = AVPlayerLayer(player: player)
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            playerLayer.frame = CGRect(x: 0, y: 0, width: videoView.frame.width, height: videoView.frame.height)
            playerLayer.videoGravity = .resizeAspectFill
            self.videoView.layer.insertSublayer(playerLayer, at: 0)
    }
    }
    override func videoPlay(videoFile: String) {
            player.seek(to: CMTime.zero)
            player.play()
    }
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        player.seek(to: CMTime.zero)
        player.play()
    }
    override func videoPause() {
        if player != nil {
            player.pause()
        }
    }
    deinit {
        player = nil
        playerLayer = nil
        playerLayer.removeFromSuperlayer()
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func reloadTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("dataReload"), object: nil, userInfo: nil)
    }
}
