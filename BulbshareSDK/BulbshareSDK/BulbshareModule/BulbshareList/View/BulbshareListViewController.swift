import UIKit

class BulbshareListViewController: BaseViewController {
    
    @IBOutlet weak var joinBrief: UIButton!
    @IBOutlet weak var bulbshareTitle: UILabel!
    @IBOutlet weak var bulbshareTableView: BulbshareListTableView!
    // MARK: - Local variables
    fileprivate var pageIndex = 1
    var refreshControl = UIRefreshControl()
    var input: BSBulbshareListInput!
    var feedTitle: String = ""
    fileprivate var presenter: BulbshareListPresenter!
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = BulbshareListPresenter(delegate: self)
        self.getBulbshareList(input: self.input, currentPage: self.pageIndex)
        refreshControl.addTarget(self, action: #selector(pullToRefresh(sender:)), for: .valueChanged)
        refreshControl.tintColor = UIColor(hexString: ColorConstants.lightGreenColor)
        bulbshareTableView.refreshControl = refreshControl
       bulbshareTableViewHandler()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadBulbshareListData(_:)), name: NSNotification.Name(rawValue: "dataReload"), object: nil)
    }
    func bulbshareTableViewHandler(){
        bulbshareTableView.photoImageHandler = { [weak self] bulbshareRef, model, row, currentCell in
            let storyBoard: UIStoryboard = UIStoryboard(name: "SingleBulbshare", bundle: SDKBundles.sdkBundle)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SingleBulbshareViewController") as! SingleBulbshareViewController
            nextVC.model = model
            nextVC.row = row
            nextVC.currentCell = currentCell
            nextVC.input = BSSingleBulbshareInput(bulbshareRef: bulbshareRef)
            self?.navigationController?.pushViewController(nextVC, animated: true)
        }
        bulbshareTableView.likeHandler = { [weak self] bulbshareRef,index,wasLikedByMe in
            self?.presenter.getLikeUnlike(ref: bulbshareRef, wasLikedByMe: wasLikedByMe, index: index)
        }
        bulbshareTableView.commentHandler = { [weak self] bulbshareRef in
            Logger.debug(arg: "the ref is \(bulbshareRef)")
        }
        bulbshareTableView.shareHandler = { [weak self] bulbshareRef in
            Logger.debug(arg: "the ref is \(bulbshareRef)")
        }
        bulbshareTableView.reportDeleteHandler = { [weak self] bulbshareRef,userRef,index in
            if userRef == KeychainAccessHandler().getUserRef(){
                self?.showActionSheetAlert(title: Alert.deleteBulbshare) { [weak self] in
                    self?.showAlertController(withTitle: Alert.AreYouSure, message: Alert.deleteMessage, acceptTitle: Alert.Ok, acceptCompletionBlock: {
                        [weak self] in
                        self?.presenter.deleteBulbshare(ref: bulbshareRef,index: index)
                        self?.getBulbshareList(input: self!.input, currentPage: self!.pageIndex)
                    }, cancelTitle: Alert.Cancel, cancelCompletionBlock: nil)
                }
            } else {
                self?.showActionSheetAlert(title: Alert.reportTitle) { [weak self] in
                    let storyboard = UIStoryboard(name: "Report", bundle: SDKBundles.sdkBundle)
                    let vc = storyboard.instantiateViewController(withIdentifier: Report.reportViewController) as! ReportViewController
                    vc.bulbshareRef = bulbshareRef
                    self?.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    @objc func reloadBulbshareListData(_ notification: NSNotification) {
        self.getBulbshareList(input: self.input, currentPage: self.pageIndex)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.bulbshareTitle.text = self.feedTitle
        self.getBulbshareList(input: self.input, currentPage: self.pageIndex)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func pullToRefresh(sender: UIRefreshControl){
        pageIndex = 1
        self.getBulbshareList(input: self.input, currentPage: pageIndex)
        sender.endRefreshing()
    }
    private func getBulbshareList(input: BSBulbshareListInput,currentPage: Int){
        self.presenter.getBulbshareList(input: input, currentPage: currentPage)
    }
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func joinBriefTapped(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "CreateBulbshare", bundle: SDKBundles.sdkBundle)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CreateBulbshareViewController") as! CreateBulbshareViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
extension BulbshareListViewController: BulbshareListDelegate {
    func setupView(data: [[BulbshareViewModel]], numberOfRows: [Int]) {
        bulbshareTableView.bindBulbshare(model: data, row: numberOfRows, bulbsharedelegate: self, currentPage: pageIndex)
        callBackToInteractor?(true,nil)
    }
    
    func getPaginationBulbshareList(pageindex: Int) {
        self.pageIndex = pageindex
        self.presenter.getBulbshareList(input: self.input, currentPage: pageIndex)
    }
    
    func updateBulbshareList(data: [[BulbshareViewModel]]?, index: Int?) {
        if let model = data,let index = index{
            self.bulbshareTableView.updateBulbshare(model: model, index: index)
            DispatchQueue.main.async {
                self.bulbshareTableView.reloadData()
            }
        }
    }
}
