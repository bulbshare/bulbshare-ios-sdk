import UIKit

class TextCollectionViewCell: BulbshareListBaseCollectionViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setBulbshareDetailData(model: [BulbshareViewModel], row: Int, currentVC: CurrentVC) {
        self.descriptionLabel.textColor = UIColor(red: CGFloat(model[row].fontColor.r)/255.0, green: CGFloat(model[row].fontColor.g)/255.0, blue: CGFloat(model[row].fontColor.b)/255.0, alpha: CGFloat(model[row].fontColor.a)/1.0)
        self.descriptionLabel.backgroundColor = UIColor(red: CGFloat(model[row].bgColor.r)/255.0, green: CGFloat(model[row].bgColor.g)/255.0, blue: CGFloat(model[row].bgColor.b)/255.0, alpha: CGFloat(model[row].bgColor.a)/1.0)
            self.descriptionLabel.text = " \(model[row].comment)"
    }
}
