import Foundation
import UIKit

class BulbshareListTableView: UITableView {
    
    // MARK: - Local variables
    var dataModel: [[BulbshareViewModel]] = []
    fileprivate weak var bulbshareListdelegate: BulbshareListDelegate!
    var currentPage = 1
    var totalCount = 50
    var numberOfRow = [Int]()
    var photoImageHandler: ((String,[BulbshareViewModel],[Int],Int) ->Void)?
    var commentHandler: ((String) -> Void)?
    var likeHandler: ((String,Int,Bool) -> Void)?
    var shareHandler: ((String) -> Void)?
    var reportDeleteHandler: ((String,String,Int) -> Void)?
    var spinner = UIActivityIndicatorView(style: .gray)
    // MARK: - Life cycle methods
    override func awakeFromNib() {
        backgroundColor = .clear
        self.delegate = self
        self.dataSource = self
        spinner.color = UIColor(red: 53.0/255.0, green: 220.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        registerCell()
    }
    func registerCell(){
        self.register(UINib(nibName: "BulbshareTableViewCell", bundle: SDKBundles.sdkBundle), forCellReuseIdentifier: "BulbshareTableViewCell")
    }
    func updateBulbshare(model: [[BulbshareViewModel]],index: Int){
        self.dataModel.removeAll()
        self.dataModel = model
        DispatchQueue.main.async {
            self.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    func bindBulbshare(model: [[BulbshareViewModel]], row: [Int], bulbsharedelegate: BulbshareListDelegate, currentPage: Int){
        if currentPage == 1 {
            self.currentPage = currentPage
            self.dataModel.removeAll()
        }
        self.dataModel += model
        self.numberOfRow = row
        if model.count == 0 {
            self.totalCount = self.dataModel.count
        }
        self.bulbshareListdelegate = bulbsharedelegate
        DispatchQueue.main.async {
            self.reloadData()
        }
        spinnerSetup()
    }
    func spinnerSetup() {
        DispatchQueue.main.async {
            self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.bounds.width, height: CGFloat(44))
        }
    }
}
extension BulbshareListTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BulbshareTableViewCell", for: indexPath) as? BulbshareTableViewCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.bindData(model: self.dataModel[indexPath.row], row: self.numberOfRow, currentCell: indexPath.row)
        cell.photoImageHandler = { [weak self] bulbshareRef,model,row,currentCell in
            self?.photoImageHandler?(bulbshareRef,model,row,currentCell)
        }
        cell.commentHandler = { [weak self] bulbshareRef in
            self?.commentHandler?(bulbshareRef)
        }
        cell.likeHandler = { [weak self] bulbshareRef,index,wasLikedByMe in
            self?.likeHandler?(bulbshareRef,index,wasLikedByMe)
        }
        cell.shareHandler = { [weak self] bulbshareRef in
            self?.shareHandler?(bulbshareRef)
        }
        cell.reportDeleteHandler = { [weak self] bulbshareRef,userRef in
            self?.reportDeleteHandler?(bulbshareRef,userRef,indexPath.row)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? BulbshareTableViewCell{
            cell.photoVideoCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height*0.62
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.dataModel.count - 1 && indexPath.row+1 < self.totalCount {
            currentPage += 1
            self.bulbshareListdelegate?.getPaginationBulbshareList(pageindex: currentPage)
            showSpinner()
        } else {
            hideSpinner()
        }
    }
    func showSpinner() {
        spinner.startAnimating()
        self.tableFooterView = spinner
        self.tableFooterView?.isHidden = false
    }
    
    func hideSpinner() {
        spinner.stopAnimating()
        self.tableFooterView?.isHidden = true
    }
}
