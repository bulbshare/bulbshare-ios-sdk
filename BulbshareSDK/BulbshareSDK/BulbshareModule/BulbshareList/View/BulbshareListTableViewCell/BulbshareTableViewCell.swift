import UIKit

class BulbshareTableViewCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var threeDotsBtn: UIButton!
    @IBOutlet weak var userPostTime: UILabel!
    @IBOutlet weak var pageControls: UIPageControl!
    @IBOutlet weak var bottomBlackView: UIView!
    @IBOutlet weak var bulbShareBtn: UIButton!
    @IBOutlet weak var userDesc: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var userPostImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var photoVideoCollectionView: PhotoVideoCollectionView!
    var likeCount: Int!
    var currentCell: Int!
    var model: [BulbshareViewModel]!
    var row: [Int]!
    var bulbshareRef: String!
    var wasLikedByMe: Bool!
    var userRef: String!
    var photoImageHandler: ((String,[BulbshareViewModel],[Int],Int) -> Void)?
    var commentHandler: ((String) -> Void)?
    var likeHandler: ((String,Int,Bool) -> Void)?
    var shareHandler: ((String) -> Void)?
    var reportDeleteHandler: ((String,String) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        bulbShareBtn.layer.cornerRadius = (bulbShareBtn.frame.height)/2
        userImageView.layer.cornerRadius = (userImageView.frame.height)/2
    }
    func bindData(model: [BulbshareViewModel], row: [Int], currentCell: Int) {
        self.currentCell = currentCell
        self.model = model
        self.row = row
        self.likeCount = model.first?.model?.bulbshares[currentCell].likesCount
        self.bulbshareRef = model.first?.model?.bulbshares[currentCell].bulbshareref
        self.wasLikedByMe = model.first?.model?.bulbshares[currentCell].wasLikedByMe
        self.userRef = model.first?.model?.bulbshares[currentCell].user.userref
        pageControls.numberOfPages = row[currentCell]
        self.likeBtn.setTitle("\(model.first?.model?.bulbshares[currentCell].likesCount)", for: .normal)
        photoVideoCollectionView.bindBulbshare(model: model, row: row, currentCell: self.currentCell, currentVC: .bulbshareList)
        self.pageControls.currentPage = 0
        photoVideoCollectionView.pageHandler = { [weak self] index in
            self?.pageControls.currentPage = index
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Briefs.dateFormat
        let submittedOn = dateFormatter.date(from: model.first?.model?.bulbshares[currentCell].submittedOn ?? "")
        userPostTime.text = submittedOn!.timeAgoDisplay()
        userName.text = model.first?.model?.bulbshares[currentCell].user.displayName
        userDesc.text = model.first?.model?.bulbshares[currentCell].brief.introText
        likeBtn.setTitle(String(model.first?.model?.bulbshares[currentCell].likesCount ?? 0), for: .normal)
        commentBtn.setTitle(String(model.first?.model?.bulbshares[currentCell].commentsCount ?? 0), for: .normal)
        bulbShareBtn.isHidden = !(model.first?.model?.bulbshares[currentCell].isSharable ?? false)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        self.photoVideoCollectionView.isUserInteractionEnabled = true
        self.photoVideoCollectionView.addGestureRecognizer(tapGestureRecognizer)
        DispatchQueue.main.async {
            self.userImageView.sd_setShowActivityIndicatorView(true)
            self.userImageView.sd_setIndicatorStyle(.gray)
            self.userImageView.sd_setImage(with: URL(string: model.first?.model?.bulbshares[currentCell].user.avatar ?? ""), placeholderImage: UIImage(named: "profile_placeholder"))
        }
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
            self.photoImageHandler?(self.bulbshareRef,self.model,self.row,self.currentCell)
        if let cell = photoVideoCollectionView.cellForItem(at: photoVideoCollectionView.currentSelectedIndex ) as? VideoCollectionViewCell{
            cell.videoPause()
        }
        }
    @IBAction func commentTapped(_ sender: UIButton) {
        self.commentHandler?(self.bulbshareRef)
    }
    @IBAction func likeTapped(_ sender: UIButton) {
        //self.likeHandler?(self.bulbshareRef,self.wasLikedByMe)
        if self.wasLikedByMe{
            self.likeBtn.setImage(UIImage(named: Briefs.like), for: .normal)
            self.likeCount -= 1
        } else{
            self.likeBtn.setImage(UIImage(named: Briefs.liked), for: .normal)
            self.likeCount += 1
        }
        self.likeBtn.setTitle("\(likeCount!)", for: .normal)
        self.wasLikedByMe = !self.wasLikedByMe
        self.likeHandler?(self.bulbshareRef, self.currentCell, self.wasLikedByMe)
    }
    @IBAction func shareTapped(_ sender: UIButton) {
        self.shareHandler?(self.bulbshareRef)
    }
    @IBAction func reportDeleteTapped(_ sender: UIButton) {
        self.reportDeleteHandler?(self.bulbshareRef,self.userRef)
    }
}
