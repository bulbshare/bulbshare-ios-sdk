import UIKit

class PhotoCollectionViewCell: BulbshareListBaseCollectionViewCell {

    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        reloadButton.isHidden = false
    }
    override func setBulbshareDetailData(model: [BulbshareViewModel], row: Int, currentVC: CurrentVC) {
        switch currentVC{
        case .bulbshareList: self.photoImageView.contentMode = .scaleAspectFill
        case .singleBulbshare: self.photoImageView.contentMode = .scaleAspectFit
            self.bgView.backgroundColor = UIColor(red: CGFloat(model[row].bgColor.r)/255.0, green: CGFloat(model[row].bgColor.g)/255.0, blue: CGFloat(model[row].bgColor.b)/255.0, alpha: CGFloat(model[row].bgColor.a)/1.0)
            self.bgView.backgroundColor = UIColor().rgbToHex(red: model[row].bgColor.r, green: model[row].bgColor.g, blue: model[row].bgColor.b, alpha: model[row].bgColor.a)
        }
        DispatchQueue.main.async {
            self.photoImageView.sd_setShowActivityIndicatorView(true)
            self.photoImageView.sd_setIndicatorStyle(.gray)
            if model[row].image == "" {
                self.reloadButton.isHidden = false
            } else {
                self.reloadButton.isHidden = true
                self.photoImageView.sd_setImage(with: URL(string: model[row].image), placeholderImage: UIImage(named: "profile_placeholder"))
            }
        }
    }
    
    @IBAction func reloadTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("dataReload"), object: nil, userInfo: nil)
    }
}
