import Foundation
import UIKit

class BulbshareListMapper {
    
    class func map(model: BulbshareListModel) -> ([[BulbshareViewModel]],[Int]) {
        var array: [BulbshareViewModel] = []
        var numberOfCell = 0
        var numberOfRows = [Int]()
        var arrayModel: [[BulbshareViewModel]] = []
        for item in model.bulbshares {
            numberOfCell = 0
            array = []
            if item.picture != "" {
                let type = BulbshareViewModel(model: model, cellType: PhotoCollectionViewCell.self, cellHeight: 0, image: item.picture)
                 array.append(type)
                numberOfCell += 1
            }
            if item.comment != "" {
                let type = BulbshareViewModel(model: model, cellType: TextCollectionViewCell.self, cellHeight: 0, comment: item.comment, fontColor: Color(r: item.commentStyle.fontColor.r, g: item.commentStyle.fontColor.g, b: item.commentStyle.fontColor.b, a: item.commentStyle.fontColor.a), bgColor: Color(r: item.commentStyle.bgColor.r, g: item.commentStyle.bgColor.g, b: item.commentStyle.bgColor.b, a: item.commentStyle.bgColor.a))
                 array.append(type)
                numberOfCell += 1
            }
            if item.video != "" {
                let type = BulbshareViewModel(model: model, cellType: VideoCollectionViewCell.self, cellHeight: 0, video: item.video)
                 array.append(type)
                numberOfCell += 1
            }
            arrayModel.append(array)
            
            numberOfRows.append(numberOfCell)
        }
        return (arrayModel, numberOfRows)
    }
}
