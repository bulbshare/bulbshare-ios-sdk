import Foundation
import UIKit
/// public protocol used to create a method of sdk version
public protocol SDKSettingsProtocol {
    static func sdkVersion() -> String
}
/// class defined to initialise the sdk version
public class  SDKSettings: SDKSettingsProtocol {
    public static func sdkVersion() -> String {
        guard let value = SDKBundles.sdkBundle?.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return ""
        }
        return (value)
    }
    internal class func apiVersion() -> String {
        guard let value = SDKBundles.sdkBundle?.infoDictionary?[Keys.apiVersion] as? String else {
            return ""
        }
        return (value)
    }
}
