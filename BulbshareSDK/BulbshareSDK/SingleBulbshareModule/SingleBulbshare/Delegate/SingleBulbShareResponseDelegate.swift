/**
 Login callbacks from the SDK
 */
protocol SingleBulbshareResponseDelegate: AnyObject {
    /**
     Callback delegate if data load successfully
     */
    func singleBulbshareDataGotSuccessfully()
    
    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func singleBulbshareThrowError(error: SDKError)
}

