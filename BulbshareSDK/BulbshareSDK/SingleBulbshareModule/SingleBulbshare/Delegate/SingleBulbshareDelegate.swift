import Foundation

protocol SingleBulbshareDelegate: BaseDelegate {
    func getSingleBulbshare(data: SingleBulbshareViewModel)
}
