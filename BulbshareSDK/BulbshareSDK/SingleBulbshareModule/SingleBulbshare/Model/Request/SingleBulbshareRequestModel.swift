import Foundation
struct CommonBulbshareReqModel: Codable {
    let bulbshareref: String
}
class CommonBulbshareRequest {
    func params(model: APIRequest<CommonBulbshareReqModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
