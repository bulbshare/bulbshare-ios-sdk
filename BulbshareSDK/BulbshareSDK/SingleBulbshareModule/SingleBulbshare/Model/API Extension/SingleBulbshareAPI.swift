import Foundation

enum SingleBulbshareAPI{
    case getSingleBulbshares(model: APIRequest<CommonBulbshareReqModel>)
}
extension SingleBulbshareAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .getSingleBulbshares:
            return "getbulbshare"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .getSingleBulbshares(let model):
            return CommonBulbshareRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
