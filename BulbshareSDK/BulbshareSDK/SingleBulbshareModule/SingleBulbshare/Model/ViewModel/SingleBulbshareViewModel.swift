import UIKit
struct SingleBulbshareViewModel {
    var wasLikedByMe: Bool?
    var commentCount: String?
    var likeCountString: String?
    var profilePhoto: String?
    var userName: String?
    var briefName: String?
    var timeAgo: String?
    var videoUrl: String?
    var imageUrl: String?
    var commentText: String?
    var commentTextAlignment: String?
    var commentColor: UIColor?
    var commentBackgroundColor: UIColor?
    var isShareable: Bool?
    var bulbshareRef: String?
    var likeBtnImage: UIImage?
    var likeCount: Int?
    var totalCount: Int?
    var contentArray: [String]?
    var userRef: String?
}
class SingleBulbshareMapper{
    class func SingleBulbshareMap(model: SingleBulbshareResponse?) -> SingleBulbshareViewModel{
        if let model = model {
            return SingleBulbshareViewModel.init(wasLikedByMe: model.wasLikedByMe, commentCount: self.setBtnCount(count: model.commentsCount), likeCountString: self.setBtnCount(count: model.likesCount), profilePhoto: model.user.avatar ?? "", userName: model.user.display_name ?? "", briefName: model.brief.name, timeAgo: Date().timeAgo(postedTime: model.submittedOn), commentText: model.comment, commentTextAlignment: "",commentBackgroundColor: UIColor().rgbToHex(red: model.commentStyle.bgColor.r, green: model.commentStyle.bgColor.g, blue: model.commentStyle.bgColor.b,alpha: model.commentStyle.bgColor.a),isShareable: model.isShareable,bulbshareRef: model.bulbshareref,likeBtnImage: self.likeBtnImage(wasLikedByMe: model.wasLikedByMe),likeCount: model.likesCount,contentArray: self.typesOfData(model: model),userRef: model.user.userref)
        } else{
            return SingleBulbshareViewModel.init(wasLikedByMe: false, commentCount: "", likeCountString: "", profilePhoto: "", userName: "", briefName: "", timeAgo: "")
        }
    }
    class func setBtnCount(count: Int) -> String{
        if count > 999{
            return Briefs.limit
        } else{
            return "\(count)"
        }
    }
    class func likeBtnImage(wasLikedByMe: Bool) -> UIImage{
        if wasLikedByMe == true{
            return UIImage(named: Briefs.liked)!
        } else{
            return UIImage(named: Briefs.like)!
        }
    }
    class func typesOfData(model: SingleBulbshareResponse?) -> [String]{
        var contentArray: [String] = []
        contentArray.append(model!.comment)
        if let picture = model?.picture{
            if picture != ""{
                contentArray.append(picture)
            }
            
        }
        if let video = model?.video{
            if video != ""{
                contentArray.append(video)
            }
            
        }
        return contentArray
    }
    class func commentAlignment(commentText: String) -> NSTextAlignment{
        switch commentText{
        case "left": return .left
        default: return .left
        }
    }
}
