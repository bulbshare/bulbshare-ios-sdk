import Foundation

// MARK: - Singl eBulb share Response
struct SingleBulbshareResponse: Codable {
    let bulbshareref: String
    let user: CommentUser
    let brief: SingleBulbshareData
    let picture: String
    let video: String
    let videoTn: String?
    let title, comment: String
    let commentStyle: CommentStyle
    let externalAnalysisRef: String?
    let likesCount, commentsCount: Int
    let isShareable: Bool
    let submittedOn, organisationid: String
    let videoIndexID, textInsights, videoInsights: String?
    let wasLikedByMe: Bool

    enum CodingKeys: String, CodingKey {
        case bulbshareref, user, brief, picture, video
        case videoTn = "video_tn"
        case title, comment
        case commentStyle = "comment_style"
        case externalAnalysisRef = "external_analysis_ref"
        case likesCount = "likes_count"
        case commentsCount = "comments_count"
        case isShareable = "is_shareable"
        case submittedOn = "submitted_on"
        case organisationid
        case videoIndexID = "video_index_id"
        case textInsights = "text_insights"
        case videoInsights = "video_insights"
        case wasLikedByMe = "was_liked_by_me"
    }
}
// MARK: - Brief
struct SingleBulbshareData: Codable {
    let briefref, name, internalName: String
    let coverPhoto: String
    let coverVideoMobile, coverVideoWeb, introText, feedTitle: String
    let bannerImage: String
    let ad2_Text: String
    let bsAllowLongVideo: Bool
    let showFeedPreview: String
    let commsSentOn: String?
    let introductionTitle, introductionSubtitle, introductionBody, introductionImage: String
    let audition: Bool
    let auditionTitle, auditionSubtitle, auditionSuccessImage, auditionSuccessText: String
    let trackingURL: String
    let trackingFailureURL: String?
    let type: Int
    let briefType: String?
    let likesCount, bulbsharesCount, responsesCount, commentsCount: Int
    let responseLimit, maxResponses: Int
    let status: String
    let isPrivate, isPublic: Bool
    let isHidden, isEnrichment, organisationid, templateBriefID: String
    let translateResponses, translateResponsesEngine, translateFrom, translateTo: String
    let projectID, templateOrganisationID: String?
    let languageID: String
    let bvProductID: String?
    let bvProductName: String
    let bvIncentivised: String?
    let brandname: String
    let projectName: String?
    let languageName, code, typeDesc: String

    enum CodingKeys: String, CodingKey {
        case briefref, name
        case internalName = "internal_name"
        case coverPhoto = "cover_photo"
        case coverVideoMobile = "cover_video_mobile"
        case coverVideoWeb = "cover_video_web"
        case introText = "intro_text"
        case feedTitle = "feed_title"
        case bannerImage = "banner_image"
        case ad2_Text = "ad_2_text"
        case bsAllowLongVideo = "bs_allow_long_video"
        case showFeedPreview = "show_feed_preview"
        case commsSentOn = "comms_sent_on"
        case introductionTitle = "introduction_title"
        case introductionSubtitle = "introduction_subtitle"
        case introductionBody = "introduction_body"
        case introductionImage = "introduction_image"
        case audition
        case auditionTitle = "audition_title"
        case auditionSubtitle = "audition_subtitle"
        case auditionSuccessImage = "audition_success_image"
        case auditionSuccessText = "audition_success_text"
        case trackingURL = "tracking_url"
        case trackingFailureURL = "tracking_failure_url"
        case type
        case briefType = "brief_type"
        case likesCount = "likes_count"
        case bulbsharesCount = "bulbshares_count"
        case responsesCount = "responses_count"
        case commentsCount = "comments_count"
        case responseLimit = "response_limit"
        case maxResponses = "max_responses"
        case status
        case isPrivate = "is_private"
        case isPublic = "is_public"
        case isHidden = "is_hidden"
        case isEnrichment = "is_enrichment"
        case organisationid
        case templateBriefID = "template_brief_id"
        case translateResponses = "translate_responses"
        case translateResponsesEngine = "translate_responses_engine"
        case translateFrom = "translate_from"
        case translateTo = "translate_to"
        case projectID = "project_id"
        case templateOrganisationID = "template_organisation_id"
        case languageID = "language_id"
        case bvProductID = "bv_product_id"
        case bvProductName = "bv_product_name"
        case bvIncentivised = "bv_incentivised"
        case brandname
        case projectName = "project_name"
        case languageName = "language_name"
        case code
        case typeDesc = "type_desc"
    }
}




// MARK: - Color
struct Color: Codable {
    let r, g, b, a: Int

    enum CodingKeys: String, CodingKey {
        case r = "R"
        case g = "G"
        case b = "B"
        case a = "A"
    }
}
