import Foundation
class SingleBulbshareManager{
    fileprivate var service: SingleBulbshareService = SingleBulbshareService()
    fileprivate var likeUnlikeBulbshareManager: LikeUnlikeBulbshareManager = LikeUnlikeBulbshareManager()
    fileprivate var deleteBulbshareManager: DeleteBulbshareManager = DeleteBulbshareManager()
    fileprivate var reportBulbshareManager: ReportBulbshareManager = ReportBulbshareManager()
    func getSingleBulbshare(
        input: BSSingleBulbshareInput,
        completion: @escaping (_ response: SingleBulbshareViewModel?,
                               _ error: Error?) -> Void) {
        let arg = CommonBulbshareReqModel(bulbshareref: input.bulbshareRef)
        let model = APIRequest<CommonBulbshareReqModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.getSingleBulbshare(model: model) { response, error in
            if let value = response {
                completion(SingleBulbshareMapper.SingleBulbshareMap(model: value),error)
            } else{
                completion(nil,error)
            }
        }
    }
    func likeUnlikeBulbshare(ref: String,type: String,completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        likeUnlikeBulbshareManager.likeBulbshare(ref: ref, type: type, completion: completion)
    }
    func deleteBulbshare(ref: String, completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        deleteBulbshareManager.deleteBulbshare(ref: ref, completion: completion)
    }
    func reportBulbshare(ref: String,reason: String,completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        reportBulbshareManager.reportBulbshare(ref: ref, reason: reason, completion: completion)
    }
}
