import Foundation

class SingleBulbsharePresenter{
    // MARK: - Local Variables
    fileprivate weak var delegate: SingleBulbshareDelegate?
    fileprivate var singleBulbshareManger: SingleBulbshareManager = SingleBulbshareManager()
    // MARK: - Initializers
    init(delegate: SingleBulbshareDelegate){
        self.delegate = delegate
    }
    func getSingleBulbshare(input: BSSingleBulbshareInput){
        self.delegate?.showLoader()
        singleBulbshareManger.getSingleBulbshare(input: input ) { [weak self] (response,error) in
            self?.delegate?.hideLoader()
            if let response = response {
                self?.delegate?.getSingleBulbshare(data: response)
            } else if let errorTemp = error as? SDKError{
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    func getLikeUnlike(ref: String,wasLikedByMe: Bool){
        singleBulbshareManger.likeUnlikeBulbshare(ref: ref, type: wasLikedByMe ? Feeds.unlike : Feeds.like) { [weak self] (response,error) in
            if response != nil{
                Logger.debug(arg: "\(String(describing: response))")
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    func deleteBulbshare(ref: String){
        singleBulbshareManger.deleteBulbshare(ref: ref) { [weak self] (response,error) in
            if response != nil{
                Logger.debug(arg: "\(String(describing: response))")
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    
}
