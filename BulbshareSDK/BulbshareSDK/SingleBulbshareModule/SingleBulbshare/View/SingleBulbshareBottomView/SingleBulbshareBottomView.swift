import UIKit

class SingleBulbshareBottomView: UIView {
    
    // MARK: - Local properties
    private var setView: UIView!
    
    // MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    var bulbshareRef: String!
    var wasLikedByMe: Bool!
    var likeCount: Int!
    var likeBtnHandler: ((String,Bool) -> Void)?
    var commentBtnHandler: ((String) -> Void)?
    var shareBtnHandler: ((String) -> Void)?
    let gradient = CAGradientLayer()
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    func bindData(model: SingleBulbshareViewModel){
        self.likeBtn.setTitle(model.likeCountString, for: .normal)
        self.likeBtn.setImage(model.likeBtnImage, for: .normal)
        self.likeCount = model.likeCount
        self.commentBtn.setTitle(model.commentCount, for: .normal)
        self.shareBtn.isHidden = !model.isShareable!
        self.bulbshareRef = model.bulbshareRef
        self.wasLikedByMe = model.wasLikedByMe
        updateUI()
    }
    func updateUI(){
        shareBtn.layer.cornerRadius = 16
        gradient.frame = gradientView.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor(red: 60.0 / 255.0, green: 60.0 / 255.0, blue: 71.0 / 255.0, alpha: 1.0).cgColor].compactMap { $0 }
        gradientView.layer.insertSublayer(gradient, at: 0)
            }

    
    
    @IBAction func shareBtn(_ sender: UIButton) {
        self.shareBtnHandler?(self.bulbshareRef)
    }
    
    @IBAction func likeBtn(_ sender: UIButton) {
        self.likeBtnHandler?(self.bulbshareRef,self.wasLikedByMe)
        if wasLikedByMe{
            self.likeBtn.setImage(UIImage(named: Briefs.like), for: .normal)
            likeCount -= 1
        } else{
            self.likeBtn.setImage(UIImage(named: Briefs.liked), for: .normal)
            likeCount += 1
        }
        self.likeBtn.setTitle("\(likeCount!)", for: .normal)
        wasLikedByMe = !wasLikedByMe
    }
    
    
    @IBAction func commentBtn(_ sender: UIButton) {
        self.commentBtnHandler?(self.bulbshareRef)
    }
    
}
