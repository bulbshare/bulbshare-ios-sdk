import Foundation
import UIKit

class BottomPagingCollectionView: UICollectionView {
    var numberOfSlides = 0
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        registerCell()
    }
    func registerCell(){
        self.register(UINib(nibName: "BottomPagingCollectionViewCell", bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: "BottomPagingCollectionViewCell")
    }
    func bindBottomPaging(count: Int){
        self.numberOfSlides = count
    }
}
extension BottomPagingCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.numberOfSlides
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: BottomPagingCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BottomPagingCollectionViewCell", for: indexPath) as? BottomPagingCollectionViewCell else {
            return UICollectionViewCell()
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width/CGFloat(self.numberOfSlides), height: self.frame.height)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
