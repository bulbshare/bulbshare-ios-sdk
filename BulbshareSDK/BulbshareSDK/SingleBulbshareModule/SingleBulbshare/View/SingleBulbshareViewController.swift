import UIKit

class SingleBulbshareViewController: BaseViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var bottomPagingCollectionView: BottomPagingCollectionView!
    @IBOutlet weak var photoVideoCollectionView: PhotoVideoCollectionView!
    @IBOutlet weak var singleBulbshareBottomView: SingleBulbshareBottomView!
    @IBOutlet weak var singleBulbshareHeaderView: SingleBulbshareHeaderView!
    fileprivate var presenter: SingleBulbsharePresenter!
    // MARK: - Local Variables
    var input: BSSingleBulbshareInput!
    var bulbshareRef: String?
    var model: [BulbshareViewModel]!
    var row: [Int]!
    var currentCell: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SingleBulbsharePresenter(delegate: self)
        presenter.getSingleBulbshare(input: self.input)
        self.photoVideoCollectionView.bindBulbshare(model: self.model, row: self.row, currentCell: self.currentCell, currentVC: .singleBulbshare)
        self.bottomPagingCollectionView.bindBottomPaging(count: row[currentCell])
        self.bottomPagingCollectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .left)
        self.photoVideoCollectionView.sliderHandler = { [weak self] indexPath in
            self?.bottomPagingCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
        }
        
        bottomViewHandler()
        headerViewHandler()
    }
    func headerViewHandler() {
        self.singleBulbshareHeaderView.moreBtnHandler = { [weak self] bulbshareRef,userRef in
            if userRef == KeychainAccessHandler().getUserRef(){
                self?.showActionSheetAlert(title: Alert.deleteBulbshare) { [weak self] in
                    self?.showAlertController(withTitle: Alert.AreYouSure, message: Alert.deleteMessage, acceptTitle: Alert.Ok, acceptCompletionBlock: {
                        [weak self] in
                        self?.presenter.deleteBulbshare(ref: bulbshareRef)
                        self?.navigationController?.popViewController(animated: true)
                    }, cancelTitle: Alert.Cancel, cancelCompletionBlock: nil)
                }
            } else {
                self?.showActionSheetAlert(title: Alert.reportTitle) { [weak self] in
                    let storyboard = UIStoryboard(name: "Report", bundle: SDKBundles.sdkBundle)
                    let vc = storyboard.instantiateViewController(withIdentifier: Report.reportViewController) as! ReportViewController
                    vc.bulbshareRef = bulbshareRef
                    self?.present(vc, animated: true, completion: nil)
                }
            }
        }
        self.singleBulbshareHeaderView.cancelBtnHandler = {  _ in
            self.navigationController?.popViewController(animated: true)
            self.photoVideoCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
        }
    }
    func bottomViewHandler(){
        self.singleBulbshareBottomView.commentBtnHandler = { bulbshareRef in
        }
        self.singleBulbshareBottomView.likeBtnHandler = {  bulbshareRef,wasLikedByMe in
            self.presenter.getLikeUnlike(ref: bulbshareRef, wasLikedByMe: wasLikedByMe)
        }
    }
}
extension SingleBulbshareViewController: SingleBulbshareDelegate{
    func getSingleBulbshare(data: SingleBulbshareViewModel) {
        DispatchQueue.main.async {
            self.singleBulbshareHeaderView.bindData(model: data)
            self.singleBulbshareBottomView.bindData(model: data)
        }
    }
    
    
}
