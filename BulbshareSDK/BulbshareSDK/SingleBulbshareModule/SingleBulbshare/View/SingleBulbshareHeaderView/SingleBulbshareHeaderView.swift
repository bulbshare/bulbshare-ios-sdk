import UIKit
import SDWebImage

class SingleBulbshareHeaderView: UIView {
    
    // MARK: - Local properties
    private var setView: UIView!
    // MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var briefNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var userProfileImage: UIImageView!
    var bulbshareRef: String!
    var userRef: String!
    var cancelBtnHandler: ((String) -> Void)?
    var moreBtnHandler: ((String,String) -> Void)?
    let gradient = CAGradientLayer()
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    func bindData(model: SingleBulbshareViewModel){
        self.userNameLabel.text = model.userName
        self.briefNameLabel.text = model.briefName
        self.timeLabel.text = model.timeAgo
        self.bulbshareRef = model.bulbshareRef
        self.userRef = model.userRef
        updateUI()
        if let profileImage = model.profilePhoto{
            DispatchQueue.main.async {
                self.userProfileImage.sd_setShowActivityIndicatorView(true)
                self.userProfileImage.sd_setIndicatorStyle(.gray)
                self.userProfileImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "profile_placeholder"))
            }
        }
    }
    func updateUI(){
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height/2
        gradient.frame = gradientView.bounds
        gradient.colors = [UIColor(red: 60.0 / 255.0, green: 60.0 / 255.0, blue: 71.0 / 255.0, alpha: 1.0).cgColor, UIColor.clear.cgColor].compactMap { $0 }
        gradientView.layer.insertSublayer(gradient, at: 0)
    }
    @IBAction func moreBtnTapped(_ sender: UIButton) {
        self.moreBtnHandler?(self.bulbshareRef,self.userRef)
    }
    
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.cancelBtnHandler?(self.bulbshareRef)
    }
    
   
    
    
}
