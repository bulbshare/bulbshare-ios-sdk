import UIKit

class BottomPagingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override var isSelected: Bool {
            didSet {
                if isSelected{
                    self.bottomView.backgroundColor = .white
                } else{
                    self.bottomView.backgroundColor = .clear
                }
            }
        }
}
