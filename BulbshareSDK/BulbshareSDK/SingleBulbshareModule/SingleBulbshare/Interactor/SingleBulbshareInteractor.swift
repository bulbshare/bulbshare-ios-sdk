import UIKit

class SingleBulbshareInteractor {
    // MARK: - Local Variables
    fileprivate weak var delegate: SingleBulbshareResponseDelegate!
    
    // MARK: - Initializers
    init(delegate: SingleBulbshareResponseDelegate){
        self.delegate = delegate
    }
    /**
     Show SingleBulbshare controller
     */
    func showSingleBulbshare(navigation: UINavigationController,input: BSSingleBulbshareInput)
    {
        let storyBoard: UIStoryboard = UIStoryboard(name: SingleBulbshare.SingleBulbshareStoryBoard, bundle: SDKBundles.sdkBundle)
        let viewController = storyBoard.instantiateViewController(withIdentifier: SingleBulbshare.SingleBulbshareViewController) as! SingleBulbshareViewController
        viewController.input = input
        viewController.callBackToInteractor = { [weak self] (value, error) in
            if value {
                self?.delegate.singleBulbshareDataGotSuccessfully()
            } else if let error = error {
                self?.delegate.singleBulbshareThrowError(error: error)
            }
        }
        
        navigation.pushViewController(viewController, animated: true)
    }
}
