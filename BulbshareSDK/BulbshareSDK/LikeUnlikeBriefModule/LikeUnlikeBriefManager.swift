import Foundation
class LikeUnlikeBriefManager{
    
    fileprivate var service: LikeUnlikeBriefService = LikeUnlikeBriefService()
    func likeBrief(ref: String,type: String, completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        let arg = LikeUnlikeBriefRequestModel(briefref: ref)
        let model = APIRequest<LikeUnlikeBriefRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.likeBrief(model: model, type: type) { response, error in
            completion(response,error)
        }
    }
}
