import Foundation
class LikeUnlikeBriefService {
    private let likeRouter = ServiceManager<LikeBriefAPI>()
    private let unlikeRouter = ServiceManager<UnlikeBriefAPI>()
    func likeBrief(
        model: APIRequest<LikeUnlikeBriefRequestModel>,type: String,
        completion: @escaping (_ response: Bool?,
                               _ error: Error?) -> Void) {
        if type == Feeds.like {
            likeRouter.request(.LikeBrief(model: model)) { (response, error) in
                guard  error == nil else {
                    completion(nil, error)
                    return
                }
                do {
                    guard (response as? Data) != nil else {
                        completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                        return
                    }
                    completion(true, nil)
                } catch {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                }
            }
        } else if type == Feeds.unlike{
            unlikeRouter.request(.UnlikeBrief(model: model)) { response, error in
                guard  error == nil else {
                    completion(nil, error)
                    return
                }
                do {
                    guard (response as? Data) != nil else {
                        completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                        return
                    }
                    completion(true, nil)
                } catch {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                }
            }
        }
        
    }
}
