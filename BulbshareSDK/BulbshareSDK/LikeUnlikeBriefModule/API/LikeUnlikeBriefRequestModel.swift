import Foundation
struct LikeUnlikeBriefRequestModel: Codable {
    let briefref: String
    
}

class LikeUnlikeBriefRequest {
    func params(model: APIRequest<LikeUnlikeBriefRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
