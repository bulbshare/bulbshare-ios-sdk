import Foundation

enum UnlikeBriefAPI{
    case UnlikeBrief(model: APIRequest<LikeUnlikeBriefRequestModel>)
}
extension UnlikeBriefAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .UnlikeBrief:
            return "unlikebrief"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .UnlikeBrief(let model):
            return LikeUnlikeBriefRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
