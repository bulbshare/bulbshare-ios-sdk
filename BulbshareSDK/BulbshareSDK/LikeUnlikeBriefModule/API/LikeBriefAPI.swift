import Foundation

enum LikeBriefAPI{
    case LikeBrief(model: APIRequest<LikeUnlikeBriefRequestModel>)
}
extension LikeBriefAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .LikeBrief:
            return "likebrief"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .LikeBrief(let model):
            return LikeUnlikeBriefRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
