import UIKit
struct BriefViewModel {
    var wasLikedByMe: Bool
    var timeRemaining: String
    var postImage: String
    var brandImage: String
    var brandDesc: String
    var likeCount: Int
    var likeCountString: String
    var bulbshareCount: String
    var briefRef: String
    var commentCount: String
    var imageOverlapText: String
    var brandName: String
    var likedImage: UIImage
    var postTimeColor: UIColor
}
class BriefMapper{
    class func briefMap(model: [BriefResponseData]) -> [BriefViewModel]{
        var briefArray: [BriefViewModel] = []
        for value in model{
            BriefRefList.sharedInstance.briefRefs.append(value.briefref)
            let timeRemain = Date().timeRemaining(endDate: value.endsOn, userResponse: value.userHasResponded)
            briefArray.append(BriefViewModel(wasLikedByMe: value.wasLikedByMe, timeRemaining: timeRemain.0, postImage: value.bannerImage, brandImage: value.brand.logo, brandDesc: value.name, likeCount: value.likesCount, likeCountString: self.setBtnCount(count: value.likesCount), bulbshareCount: self.setBtnCount(count: value.bulbsharesCount), briefRef: value.briefref, commentCount: self.setBtnCount(count: value.commentsCount), imageOverlapText: value.feedTitle, brandName: value.brand.name,likedImage: self.likeBtnImage(wasLikedByMe: value.wasLikedByMe),postTimeColor: timeRemain.1))
        }
        return briefArray
    }
    class func setBtnCount(count: Int) -> String{
        if count > 999{
            return "999+"
        } else{
            return "\(count)"
        }
    }
    class func likeBtnImage(wasLikedByMe: Bool) -> UIImage{
        if wasLikedByMe == true{
            return UIImage(named: "liked")!
        } else{
            return UIImage(named: "like")!
        }
    }
}
