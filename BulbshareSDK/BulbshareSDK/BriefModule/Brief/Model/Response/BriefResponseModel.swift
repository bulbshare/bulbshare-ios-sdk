import Foundation

// MARK: - Brief Response
struct BriefResponse: Codable {
    let briefs: [BriefResponseData]
    let channels: [Channel]?
    let userRole: Int

    enum CodingKeys: String, CodingKey {
        case briefs, channels
        case userRole = "user_role"
    }
}

// MARK: - Brief
struct BriefResponseData: Codable {
    let briefref: String
    let brand: Channel
    let name, internalName, briefDescription: String
    let coverPhoto, coverVideoMobile: String
    let coverVideoWeb, introText, feedTitle: String
    let bannerImage: String
    let bannerVideo, bannerText: String
    let ad1_Image: String
    let ad1_Href: String
    let ad2_Text: String?
    let ad2_Image: String
    let ad2_Href, rewardImage, rewardText: String
    let bsPictureRequired: Bool
    let bsPictureText: String
    let bsVideoRequired: Bool
    let bsVideoText: String
    let bsCommentRequired: Bool
    let bsCommentText: String
    let bsAllowLongVideo: Bool
    let startsOn, endsOn: String
    let commsSentOn: String?
    let introductionTitle, introductionSubtitle, introductionBody, introductionImage: String
    let audition: Bool
    let auditionTitle, auditionSubtitle, auditionSuccessImage, auditionSuccessText: String
    let trackingURL: String
    let trackingFailureURL: String?
    let type: Int
    let briefType: String?
    var likesCount, bulbsharesCount, responsesCount, commentsCount: Int
    let responseLimit, maxResponses: Int
    let status: String
    let isPrivate, isPublic, isActivated: Bool
    let isHidden, isEnrichment, organisationid: String
    let templateBriefID: String?
    let translateResponses: String?
    let translateResponsesEngine: TranslateResponsesEngine?
    let translateFrom, translateTo: Translate?
    let projectID, templateOrganisationID: String?
    let languageID: String?
    let bvProductID: String?
    let bvProductName: String?
    let bvIncentivised: String?
    let userResponsesCount: Int
    let targeting: String?
    let userHasResponded: Bool
    let isWelcome: String
    var canSubmitBulbshares, canAnswerPoll, wasLikedByMe: Bool
    let rewards: [JSONAny]?
    let isSyndicationBrief: Bool
    let features: [JSONAny]?  
    let brandname: String?
    let projectName: String?
    let languageName, code, typeDesc: String?
    
    enum CodingKeys: String, CodingKey {
        case briefref, brand, name
        case internalName = "internal_name"
        case briefDescription = "description"
        case coverPhoto = "cover_photo"
        case coverVideoMobile = "cover_video_mobile"
        case coverVideoWeb = "cover_video_web"
        case introText = "intro_text"
        case feedTitle = "feed_title"
        case bannerImage = "banner_image"
        case bannerVideo = "banner_video"
        case bannerText = "banner_text"
        case ad1_Image = "ad_1_image"
        case ad1_Href = "ad_1_href"
        case ad2_Text = "ad_2_text"
        case ad2_Image = "ad_2_image"
        case ad2_Href = "ad_2_href"
        case rewardImage = "reward_image"
        case rewardText = "reward_text"
        case bsPictureRequired = "bs_picture_required"
        case bsPictureText = "bs_picture_text"
        case bsVideoRequired = "bs_video_required"
        case bsVideoText = "bs_video_text"
        case bsCommentRequired = "bs_comment_required"
        case bsCommentText = "bs_comment_text"
        case bsAllowLongVideo = "bs_allow_long_video"
        case startsOn = "starts_on"
        case endsOn = "ends_on"
        case commsSentOn = "comms_sent_on"
        case introductionTitle = "introduction_title"
        case introductionSubtitle = "introduction_subtitle"
        case introductionBody = "introduction_body"
        case introductionImage = "introduction_image"
        case audition
        case auditionTitle = "audition_title"
        case auditionSubtitle = "audition_subtitle"
        case auditionSuccessImage = "audition_success_image"
        case auditionSuccessText = "audition_success_text"
        case trackingURL = "tracking_url"
        case trackingFailureURL = "tracking_failure_url"
        case type
        case briefType = "brief_type"
        case likesCount = "likes_count"
        case bulbsharesCount = "bulbshares_count"
        case responsesCount = "responses_count"
        case commentsCount = "comments_count"
        case responseLimit = "response_limit"
        case maxResponses = "max_responses"
        case status
        case isPrivate = "is_private"
        case isPublic = "is_public"
        case isActivated = "is_activated"
        case isHidden = "is_hidden"
        case isEnrichment = "is_enrichment"
        case organisationid
        case templateBriefID = "template_brief_id"
        case translateResponses = "translate_responses"
        case translateResponsesEngine = "translate_responses_engine"
        case translateFrom = "translate_from"
        case translateTo = "translate_to"
        case projectID = "project_id"
        case templateOrganisationID = "template_organisation_id"
        case languageID = "language_id"
        case bvProductID = "bv_product_id"
        case bvProductName = "bv_product_name"
        case bvIncentivised = "bv_incentivised"
        case userResponsesCount = "user_responses_count"
        case targeting
        case userHasResponded = "user_has_responded"
        case isWelcome = "is_welcome"
        case canSubmitBulbshares = "can_submit_bulbshares"
        case canAnswerPoll = "can_answer_poll"
        case wasLikedByMe = "was_liked_by_me"
        case rewards
        case isSyndicationBrief = "is_syndication_brief"
        case features
        case brandname
        case projectName = "project_name"
        case languageName = "language_name"
        case code
        case typeDesc = "type_desc"
        
    }
}

// MARK: - Channel
struct Channel: Codable {
    let brandid: Int
    let brandref, name: String
    let logo: String
    let likesCount, followersCount: Int
    let isDiscoverable: Bool
    let discoverTitle, discoverAbout: String
    let discoveryCountry: String?
    let requireScreener: Bool
    let screenerURL: String
    let screenerBriefid: String?
    let briefMaxResponsesLimit, status: String
    let isPrivate: Bool
    let organisationid: String
    let languageID: String?
    let languageName: LanguageName?
    let code: Code?
    let isFollowedByMe: Bool?
    let primaryColor, secondaryColor: Color?
    let canSubmitResponse, channelDescription, longDescription, briefCount: String?
    let userHasAccess: Bool?
    let theme: Theme?

    enum CodingKeys: String, CodingKey {
        case brandid, brandref, name, logo
        case likesCount = "likes_count"
        case followersCount = "followers_count"
        case isDiscoverable = "is_discoverable"
        case discoverTitle = "discover_title"
        case discoverAbout = "discover_about"
        case discoveryCountry = "discovery_country"
        case requireScreener = "require_screener"
        case screenerURL = "screener_url"
        case screenerBriefid = "screener_briefid"
        case briefMaxResponsesLimit = "brief_max_responses_limit"
        case status
        case isPrivate = "is_private"
        case organisationid
        case languageID = "language_id"
        case languageName = "language_name"
        case code
        case isFollowedByMe = "is_followed_by_me"
        case primaryColor = "primary_color"
        case secondaryColor = "secondary_color"
        case canSubmitResponse = "can_submit_response"
        case channelDescription = "description"
        case longDescription = "long_description"
        case briefCount = "brief_count"
        case userHasAccess = "user_has_access"
        case theme
    }
}

enum Code: String, Codable {
    case enGB = "en-gb"
}

enum LanguageName: String, Codable {
    case english = "English"
}

// MARK: - Theme
struct Theme: Codable {
    let primaryColor, secondaryColor: Color
    let banner: String

    enum CodingKeys: String, CodingKey {
        case primaryColor = "primary_color"
        case secondaryColor = "secondary_color"
        case banner
    }
}

enum Translate: String, Codable {
    case en = "en"
}

enum TranslateResponsesEngine: String, Codable {
    case google = "google"
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
