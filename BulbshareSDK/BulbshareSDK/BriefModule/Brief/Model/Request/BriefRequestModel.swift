import Foundation
struct BriefRequestModel: Codable {
    let privatebrandid, page: Int
    let appVersion: String
    
   private enum CodingKeys: String, CodingKey {
        case privatebrandid = "privatebrandid"
        case page = "page"
        case appVersion = "app_version"
    }
}

class BriefRequest {
    func params(model: APIRequest<BriefRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
