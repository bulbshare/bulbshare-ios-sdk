import Foundation

enum BriefAPI{
    case getBriefs(model: APIRequest<BriefRequestModel>)
}
extension BriefAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .getBriefs:
            return "myfeedcp"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .getBriefs(let model):
            return BriefRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
