import UIKit

class BriefInteractor {
    
    fileprivate weak var delegate: BriefResponseDelegate!
    
    init(delegate: BriefResponseDelegate) {
        self.delegate = delegate
    }
    
    func showBrief(navigation: UINavigationController,input: BSBriefInput) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Brief", bundle: SDKBundles.sdkBundle)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "BriefViewController") as! BriefViewController
        viewController.input = input
        viewController.callBackToInteractor  = { [weak self] (value, error) in
            if value {
                self?.delegate.briefDataGotSuccessfully()
            } else if let error = error {
                self?.delegate.briefThrowError(error: error)
            }
        }
        navigation.pushViewController(viewController, animated: false)
    }
}
