import Foundation
import UIKit
class BriefPresenter{
    // MARK: - Variables
    fileprivate weak var delegate: BriefDelegate?
    fileprivate var briefManager: BriefManager = BriefManager()
    fileprivate var briefIntroManager: BriefIntroManager = BriefIntroManager()
    var responseArray: [BriefViewModel] = []
    // MARK: - Initializers
    init(delegate: BriefDelegate){
        self.delegate = delegate
    }
    /**
     this method is used to get briefs
     */
    func getBrief(input: BSBriefInput,currentPage: Int){
        if currentPage == 1{
            self.delegate?.showLoader()
            self.responseArray.removeAll()
        }
        briefManager.getBreifs(input: input, page: currentPage) { [weak self] (response, error) in
            self?.delegate?.hideLoader()
            if let response = response{
                self?.delegate?.getBriefs(data: response)
                self?.responseArray += response
            } else {
                if let errorTemp = error as? SDKError {
                    self?.delegate?.showError(error: errorTemp)
                } else if let errorTemp = error as? SDKError {
                    self?.delegate?.showError(error: errorTemp)
                }
            }
        }
    }
    func getLikeStatus(ref: String,wasLikedByMe: Bool,index: Int){
        if wasLikedByMe{
            briefManager.likeUnlikeBrief(ref: ref, type: "unlike") { response, error in
                if let response = response {
                    Logger.debug(arg: "\(response)")
                } else if let errorTemp = error as? SDKError{
                    self.delegate?.showError(error: errorTemp)
                }
            }
            self.responseArray[index].likedImage = UIImage(named: "like")!
            self.responseArray[index].likeCount -= 1
            self.responseArray[index].likeCountString = "\(self.responseArray[index].likeCount)"
        } else{
            briefManager.likeUnlikeBrief(ref: ref, type: "like") { response, error in
                if let response = response {
                    Logger.debug(arg: "\(response)")
                } else if let errorTemp = error as? SDKError{
                    self.delegate?.showError(error: errorTemp)
                }
            }
            self.responseArray[index].likedImage = UIImage(named: "liked")!
            self.responseArray[index].likeCount += 1
            self.responseArray[index].likeCountString = "\(self.responseArray[index].likeCount)"
        }
        self.responseArray[index].wasLikedByMe = !wasLikedByMe
        self.delegate?.updateBriefs(data: responseArray, index: index)
    }
    /**
     this method is used to map to brief intro
     */
    func getBriefIntro(briefRefID: String){
        briefIntroManager.getBriefIntro(briefref: briefRefID) { [weak self] (response, error) in
            if let responseData = response {
                let responseModel = self?.validateBriefIntro(model: responseData,briefRef: briefRefID)
                self?.delegate?.updateBriefs(data: responseModel?.model,index: responseModel?.index)
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    func validateBriefIntro(model: BriefIntroResponse,briefRef: String) -> (model: [BriefViewModel]?,index: Int?){
        let index = responseArray.firstIndex{$0.briefRef == briefRef}
        responseArray[index!].likeCountString = "\(model.likesCount!)"
        responseArray[index!].bulbshareCount = "\(model.bulbsharesCount!)"
        responseArray[index!].commentCount = "\(model.commentsCount!)"
        responseArray[index!].wasLikedByMe = model.wasLikedByMe!
        responseArray[index!].likeCount = model.likesCount!
        if model.wasLikedByMe!{
            responseArray[index!].likedImage = UIImage(named: "liked")!
        } else{
            responseArray[index!].likedImage = UIImage(named: "like")!
        }
        return (model: responseArray,index: index)
    }
}
