import Foundation
class BriefManager{
    fileprivate var service: BriefService = BriefService()
    var likeUnlikeBriefManager: LikeUnlikeBriefManager = LikeUnlikeBriefManager()
    func getBreifs(
        input: BSBriefInput,page: Int,
        completion: @escaping (_ response: [BriefViewModel]?,
                               _ error: Error?) -> Void) {
        let arg = BriefRequestModel(privatebrandid: input.privateBrandId, page: page, appVersion: sdkConfig.appVersion)
        let model = APIRequest<BriefRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.getBreifs(model: model) { response, error in
            if let value = response {
                completion(BriefMapper.briefMap(model: (value.briefs)),error)
            } else {
                completion(nil, error)
            }
        }
    }
    func likeUnlikeBrief(ref: String,type: String,completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        likeUnlikeBriefManager.likeBrief(ref: ref, type: type, completion: completion)
    }
}
