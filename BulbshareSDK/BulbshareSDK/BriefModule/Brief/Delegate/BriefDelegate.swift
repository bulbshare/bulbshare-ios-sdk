import Foundation

protocol BriefDelegate: BaseDelegate {
    func getBriefs(data: [BriefViewModel])
    func getPaginationBrief(pageindex: Int)
    func updateBriefs(data: [BriefViewModel]?,index: Int?)
}
