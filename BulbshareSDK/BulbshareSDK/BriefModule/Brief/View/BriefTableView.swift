import UIKit

class BriefTableView: UITableView {
    
    // MARK: - Local variables
    var model: [BriefViewModel] = []
    fileprivate weak var briefdelegate: BriefDelegate!
    var postImageHandler: ((String) -> Void)?
    var likeBtnHandler: ((String,Int,Bool) -> Void)?
    var bulbshareHandler: ((String) -> Void)?
    var commentHandler: ((String) -> Void)?
    var spinner = UIActivityIndicatorView(style: .gray)
    var currentPage = 1
    var totalCount = 50
    // MARK: - Life cycle methods
    override func awakeFromNib() {
        backgroundColor = .clear
        self.delegate = self
        self.dataSource = self
        spinner.color = UIColor(red: 53.0/255.0, green: 220.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        registerCell()
    }
    func bindBrief(model: [BriefViewModel],briefdelegate: BriefDelegate,currentPage: Int){
        if currentPage == 1 {
            self.currentPage = currentPage
            self.model.removeAll()
        }
        self.model += model
        if model.count == 0 {
            self.totalCount = self.model.count
        }
        self.briefdelegate = briefdelegate
        DispatchQueue.main.async {
            self.reloadData()
        }
        spinnerSetup()
    }
    func updateBrief(model: [BriefViewModel],index: Int){
        self.model.removeAll()
        self.model = model
        DispatchQueue.main.async {
            self.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
        
    }
    func spinnerSetup() {
        DispatchQueue.main.async {
            self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.bounds.width, height: CGFloat(44))
        }
    }
    
    func registerCell(){
        self.register(UINib(nibName: Briefs.BriefTableViewCell, bundle: SDKBundles.sdkBundle), forCellReuseIdentifier: Briefs.BriefTableViewCell)
    }
}
extension BriefTableView: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Briefs.BriefTableViewCell, for: indexPath) as? BriefTableViewCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.bind(model: self.model[indexPath.row], index: indexPath.row)
        cell.postImageHandler = { [weak self] briefRef in
                self?.postImageHandler?(briefRef)
        }
        cell.likeBtnHandler = { [weak self] briefRef,index,wasLikedByMe in
            self?.likeBtnHandler?(briefRef,index!,wasLikedByMe)
        }
        cell.commentHandler = { [weak self] briefRef in
            self?.commentHandler?(briefRef)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height*0.62
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.model.count - 1 && indexPath.row+1 < self.totalCount {
            currentPage += 1
            self.briefdelegate?.getPaginationBrief(pageindex: currentPage)
            showSpinner()
        } else {
            hideSpinner()
        }
    }
    func showSpinner() {
        spinner.startAnimating()
        self.tableFooterView = spinner
        self.tableFooterView?.isHidden = false
    }
    
    func hideSpinner() {
        spinner.stopAnimating()
        self.tableFooterView?.isHidden = true
    }
}
