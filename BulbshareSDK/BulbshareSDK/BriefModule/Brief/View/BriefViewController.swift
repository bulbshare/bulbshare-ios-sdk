import UIKit

class BriefViewController: BaseViewController {
    
    @IBOutlet weak var briefTableView: BriefTableView!
    // MARK: - Local variables
    fileprivate var presenter: BriefPresenter!
    fileprivate  var pageIndex = 1
    var input: BSBriefInput!
    var briefRefID: String?
    var refreshControl = UIRefreshControl()
    var feedJoinButtonHandler: ((String) -> Void)?
    
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = BriefPresenter(delegate: self)
        self.getBrief(input: self.input, currentPage: self.pageIndex)
        refreshControl.addTarget(self, action: #selector(pullToRefresh(sender:)), for: .valueChanged)
        refreshControl.tintColor = UIColor(hexString: ColorConstants.lightGreenColor)
        briefTableView.refreshControl = refreshControl
        closureCalls()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let briefRef = self.briefRefID{
            self.getBriefIntro(briefRefId: briefRef)
        }
        
    }
    @objc func pullToRefresh(sender: UIRefreshControl){
        pageIndex = 1
        self.getBrief(input: self.input, currentPage: pageIndex)
        sender.endRefreshing()
    }
    private func closureCalls(){
        briefTableView.postImageHandler = { [weak self] breifRef in
            self?.briefRefID = breifRef
            self?.feedJoinButtonHandler?(breifRef)
        }
        briefTableView.commentHandler = { [weak self] briefRef in
            let storyBoard: UIStoryboard = UIStoryboard(name: Feeds.Feed, bundle: SDKBundles.sdkBundle)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: BriefIntro.CommentViewController) as! CommentViewController
            nextVC.briefRefID = briefRef
            self?.briefRefID = briefRef
            self?.navigationController?.pushViewController(nextVC, animated: false)
        }
        briefTableView.likeBtnHandler = { [weak self] breifRef,index,wasLikedByMe in
            self?.presenter.getLikeStatus(ref: breifRef, wasLikedByMe: wasLikedByMe, index: index)
        }
    }
    private func getBrief(input: BSBriefInput,currentPage: Int){
        self.presenter.getBrief(input: input, currentPage: currentPage)
    }
    private func getBriefIntro(briefRefId: String){
        self.presenter.getBriefIntro(briefRefID: briefRefId)
    }
}
extension BriefViewController: BriefDelegate{
    func updateBriefs(data: [BriefViewModel]?, index: Int?) {
        if let model = data,let index = index{
            self.briefTableView.updateBrief(model: model, index: index)
            DispatchQueue.main.async {
                self.briefTableView.reloadData()
            }
        }
    }
    func getBriefs(data: [BriefViewModel]) {
        briefTableView.bindBrief(model: data, briefdelegate: self, currentPage: pageIndex)
        callBackToInteractor?(true, nil)
    }
    func getPaginationBrief(pageindex: Int) {
        self.pageIndex = pageindex
        self.getBrief(input: self.input, currentPage: pageIndex)
    }
}
