import UIKit
@_implementationOnly import SDWebImage

class BriefTableViewCell: UITableViewCell {
    // MARK: - IBOutlets
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var postTimeRemaining: UILabel!
    @IBOutlet weak var brandDesc: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var enterBtn: UIButton!
    @IBOutlet weak var imageOverlapText: UILabel!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var bulbhshareBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    // MARK: - LocalVariables
    var briefRef: String!
    var indexClicked: Int!
    var wasLikedByMe: Bool!
    var postImageHandler: ((String) -> Void)?
    var commentHandler: ((String) -> Void)?
    var likeBtnHandler: ((String,Int?,Bool) -> Void)?
    override func awakeFromNib() {
        customizeOutlets()
        imageTapped()
        enterBtn.isUserInteractionEnabled = false
        
    }
    // MARK: - IBOutlets Actions
    @IBAction func bublshareBtnTapped(_ sender: UIButton) {
    }
    @IBAction func likeBtnTapped(_ sender: UIButton) {
        self.likeBtnHandler?(self.briefRef,indexClicked,self.wasLikedByMe)
    }
    @IBAction func commentBtnTapped(_ sender: UIButton) {
        self.commentHandler?(self.briefRef)
    }
    // MARK: - Helper Functions
    func bind(model: BriefViewModel,index: Int){
        indexClicked = index
        postTimeRemaining.text = model.timeRemaining
        postTimeRemaining.textColor = model.postTimeColor
        DispatchQueue.main.async {
            self.postImage.sd_setShowActivityIndicatorView(true)
            self.brandImage.sd_setShowActivityIndicatorView(true)
            self.postImage.sd_setIndicatorStyle(.gray)
            self.brandImage.sd_setIndicatorStyle(.gray)
            self.postImage.sd_setImage(with: URL(string: model.postImage), placeholderImage: nil)
            self.brandImage.sd_setImage(with: URL(string: model.brandImage), placeholderImage: nil)
        }
        imageOverlapText.text = model.imageOverlapText
        brandDesc.text = model.brandDesc
        brandName.text = model.brandName
        briefRef = model.briefRef
        likeBtn.setImage(model.likedImage, for: .normal)
        likeBtn.setTitle(model.likeCountString, for: .normal)
        commentBtn.setTitle(model.commentCount, for: .normal)
        bulbhshareBtn.setTitle(model.bulbshareCount, for: .normal)
        self.wasLikedByMe = model.wasLikedByMe
    }
    private func imageTapped(){
        postImage.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTap(tapGestureRecognizer:)))
        postImage?.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func imageTap(tapGestureRecognizer: UITapGestureRecognizer){   self.postImageHandler?(self.briefRef)
    }
    private func customizeOutlets(){
        brandImage.layer.cornerRadius = (brandImage.frame.height)/2
        enterBtn.layer.cornerRadius = 15
        enterBtn.layer.borderWidth = 2
        enterBtn.layer.borderColor = UIColor.white.cgColor
    }
}
