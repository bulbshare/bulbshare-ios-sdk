import Foundation

struct BSBreifs: Codable {

    let briefref: String
    let brand: BSBrand
    let name: String
    let internalName: String
    let description: String
    let coverPhoto: String
    let coverVideoMobile: String
    let coverVideoWeb: String
    let introText: String
    let feedTitle: String
    let bannerImage: String
    let bannerVideo: String
    let bannerText: String
    let ad1Image: String
    let ad1Href: String
    let ad2Image: String
    let ad2Href: String
    let rewardImage: String
    let rewardText: String
    let bsPictureRequired: Bool
    let bsPictureText: String
    let bsVideoRequired: Bool
    let bsVideoText: String
    let bsCommentRequired: Bool
    let bsCommentText: String
    let bsAllowLongVideo: Bool
    let startsOn: String
    let endsOn: String
    let introductionTitle: String
    let introductionSubtitle: String
    let introductionBody: String
    let introductionImage: String
    let audition: Bool
    let auditionTitle: String
    let auditionSubtitle: String
    let auditionSuccessImage: String
    let auditionSuccessText: String
    let trackingUrl: String
    let type: Int
    let likesCount: Int
    let bulbsharesCount: Int
    let responsesCount: Int
    let commentsCount: Int
    let responseLimit: Int
    let maxResponses: Int
    let status: String
    let isPrivate: Bool
    let isPublic: Bool
    let isActivated: Bool
    let isHidden: String
    let isEnrichment: String
    let organisationid: String
    let translateResponses: String
    let userHasResponded: Bool
    let userResponsesCount: Int
    let canSubmitBulbshares: Bool
    let canAnswerPoll: Bool
    let userRole: Int
    let wasLikedByMe: Bool
    let isSyndicationBrief: Bool

    private enum CodingKeys: String, CodingKey {
        case briefref = "briefref"
        case brand = "brand"
        case name = "name"
        case internalName = "internal_name"
        case description = "description"
        case coverPhoto = "cover_photo"
        case coverVideoMobile = "cover_video_mobile"
        case coverVideoWeb = "cover_video_web"
        case introText = "intro_text"
        case feedTitle = "feed_title"
        case bannerImage = "banner_image"
        case bannerVideo = "banner_video"
        case bannerText = "banner_text"
        case ad1Image = "ad_1_image"
        case ad1Href = "ad_1_href"
        case ad2Image = "ad_2_image"
        case ad2Href = "ad_2_href"
        case rewardImage = "reward_image"
        case rewardText = "reward_text"
        case bsPictureRequired = "bs_picture_required"
        case bsPictureText = "bs_picture_text"
        case bsVideoRequired = "bs_video_required"
        case bsVideoText = "bs_video_text"
        case bsCommentRequired = "bs_comment_required"
        case bsCommentText = "bs_comment_text"
        case bsAllowLongVideo = "bs_allow_long_video"
        case startsOn = "starts_on"
        case endsOn = "ends_on"
        case introductionTitle = "introduction_title"
        case introductionSubtitle = "introduction_subtitle"
        case introductionBody = "introduction_body"
        case introductionImage = "introduction_image"
        case audition = "audition"
        case auditionTitle = "audition_title"
        case auditionSubtitle = "audition_subtitle"
        case auditionSuccessImage = "audition_success_image"
        case auditionSuccessText = "audition_success_text"
        case trackingUrl = "tracking_url"
        case type = "type"
        case likesCount = "likes_count"
        case bulbsharesCount = "bulbshares_count"
        case responsesCount = "responses_count"
        case commentsCount = "comments_count"
        case responseLimit = "response_limit"
        case maxResponses = "max_responses"
        case status = "status"
        case isPrivate = "is_private"
        case isPublic = "is_public"
        case isActivated = "is_activated"
        case isHidden = "is_hidden"
        case isEnrichment = "is_enrichment"
        case organisationid = "organisationid"
        case translateResponses = "translate_responses"
        case userHasResponded = "user_has_responded"
        case userResponsesCount = "user_responses_count"
        case canSubmitBulbshares = "can_submit_bulbshares"
        case canAnswerPoll = "can_answer_poll"
        case userRole = "user_role"
        case wasLikedByMe = "was_liked_by_me"
        case isSyndicationBrief = "is_syndication_brief"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        briefref = try values.decode(String.self, forKey: .briefref)
        brand = try values.decode(BSBrand.self, forKey: .brand)
        name = try values.decode(String.self, forKey: .name)
        internalName = try values.decode(String.self, forKey: .internalName)
        description = try values.decode(String.self, forKey: .description)
        coverPhoto = try values.decode(String.self, forKey: .coverPhoto)
        coverVideoMobile = try values.decode(String.self, forKey: .coverVideoMobile)
        coverVideoWeb = try values.decode(String.self, forKey: .coverVideoWeb)
        introText = try values.decode(String.self, forKey: .introText)
        feedTitle = try values.decode(String.self, forKey: .feedTitle)
        bannerImage = try values.decode(String.self, forKey: .bannerImage)
        bannerVideo = try values.decode(String.self, forKey: .bannerVideo)
        bannerText = try values.decode(String.self, forKey: .bannerText)
        ad1Image = try values.decode(String.self, forKey: .ad1Image)
        ad1Href = try values.decode(String.self, forKey: .ad1Href)
        ad2Image = try values.decode(String.self, forKey: .ad2Image)
        ad2Href = try values.decode(String.self, forKey: .ad2Href)
        rewardImage = try values.decode(String.self, forKey: .rewardImage)
        rewardText = try values.decode(String.self, forKey: .rewardText)
        bsPictureRequired = try values.decode(Bool.self, forKey: .bsPictureRequired)
        bsPictureText = try values.decode(String.self, forKey: .bsPictureText)
        bsVideoRequired = try values.decode(Bool.self, forKey: .bsVideoRequired)
        bsVideoText = try values.decode(String.self, forKey: .bsVideoText)
        bsCommentRequired = try values.decode(Bool.self, forKey: .bsCommentRequired)
        bsCommentText = try values.decode(String.self, forKey: .bsCommentText)
        bsAllowLongVideo = try values.decode(Bool.self, forKey: .bsAllowLongVideo)
        startsOn = try values.decode(String.self, forKey: .startsOn)
        endsOn = try values.decode(String.self, forKey: .endsOn)
        introductionTitle = try values.decode(String.self, forKey: .introductionTitle)
        introductionSubtitle = try values.decode(String.self, forKey: .introductionSubtitle)
        introductionBody = try values.decode(String.self, forKey: .introductionBody)
        introductionImage = try values.decode(String.self, forKey: .introductionImage)
        audition = try values.decode(Bool.self, forKey: .audition)
        auditionTitle = try values.decode(String.self, forKey: .auditionTitle)
        auditionSubtitle = try values.decode(String.self, forKey: .auditionSubtitle)
        auditionSuccessImage = try values.decode(String.self, forKey: .auditionSuccessImage)
        auditionSuccessText = try values.decode(String.self, forKey: .auditionSuccessText)
        trackingUrl = try values.decode(String.self, forKey: .trackingUrl)
        likesCount = try values.decode(Int.self, forKey: .likesCount)
        bulbsharesCount = try values.decode(Int.self, forKey: .bulbsharesCount)
        responsesCount = try values.decode(Int.self, forKey: .responsesCount)
        commentsCount = try values.decode(Int.self, forKey: .commentsCount)
        responseLimit = try values.decode(Int.self, forKey: .responseLimit)
        maxResponses = try values.decode(Int.self, forKey: .maxResponses)
        status = try values.decode(String.self, forKey: .status)
        isPrivate = try values.decode(Bool.self, forKey: .isPrivate)
        isPublic = try values.decode(Bool.self, forKey: .isPublic)
        isActivated = try values.decode(Bool.self, forKey: .isActivated)
        isHidden = try values.decode(String.self, forKey: .isHidden)
        isEnrichment = try values.decode(String.self, forKey: .isEnrichment)
        organisationid = try values.decode(String.self, forKey: .organisationid)
        translateResponses = try values.decode(String.self, forKey: .translateResponses)
        userHasResponded = try values.decode(Bool.self, forKey: .userHasResponded)
        userResponsesCount = try values.decode(Int.self, forKey: .userResponsesCount)
        canSubmitBulbshares = try values.decode(Bool.self, forKey: .canSubmitBulbshares)
        canAnswerPoll = try values.decode(Bool.self, forKey: .canAnswerPoll)
        userRole = try values.decode(Int.self, forKey: .userRole)
        wasLikedByMe = try values.decode(Bool.self, forKey: .wasLikedByMe)
        isSyndicationBrief = try values.decode(Bool.self, forKey: .isSyndicationBrief)
        type = try values.decode(Int.self, forKey: .type)

    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(briefref, forKey: .briefref)
        try container.encode(brand, forKey: .brand)
        try container.encode(name, forKey: .name)
        try container.encode(internalName, forKey: .internalName)
        try container.encode(description, forKey: .description)
        try container.encode(coverPhoto, forKey: .coverPhoto)
        try container.encode(coverVideoMobile, forKey: .coverVideoMobile)
        try container.encode(coverVideoWeb, forKey: .coverVideoWeb)
        try container.encode(introText, forKey: .introText)
        try container.encode(feedTitle, forKey: .feedTitle)
        try container.encode(bannerImage, forKey: .bannerImage)
        try container.encode(bannerVideo, forKey: .bannerVideo)
        try container.encode(bannerText, forKey: .bannerText)
        try container.encode(ad1Image, forKey: .ad1Image)
        try container.encode(ad1Href, forKey: .ad1Href)
        try container.encode(ad2Image, forKey: .ad2Image)
        try container.encode(ad2Href, forKey: .ad2Href)
        try container.encode(rewardImage, forKey: .rewardImage)
        try container.encode(rewardText, forKey: .rewardText)
        try container.encode(bsPictureRequired, forKey: .bsPictureRequired)
        try container.encode(bsPictureText, forKey: .bsPictureText)
        try container.encode(bsVideoRequired, forKey: .bsVideoRequired)
        try container.encode(bsVideoText, forKey: .bsVideoText)
        try container.encode(bsCommentRequired, forKey: .bsCommentRequired)
        try container.encode(bsCommentText, forKey: .bsCommentText)
        try container.encode(bsAllowLongVideo, forKey: .bsAllowLongVideo)
        try container.encode(startsOn, forKey: .startsOn)
        try container.encode(endsOn, forKey: .endsOn)
        try container.encode(introductionTitle, forKey: .introductionTitle)
        try container.encode(introductionSubtitle, forKey: .introductionSubtitle)
        try container.encode(introductionBody, forKey: .introductionBody)
        try container.encode(introductionImage, forKey: .introductionImage)
        try container.encode(audition, forKey: .audition)
        try container.encode(auditionTitle, forKey: .auditionTitle)
        try container.encode(auditionSubtitle, forKey: .auditionSubtitle)
        try container.encode(auditionSuccessImage, forKey: .auditionSuccessImage)
        try container.encode(auditionSuccessText, forKey: .auditionSuccessText)
        try container.encode(trackingUrl, forKey: .trackingUrl)
        try container.encode(type, forKey: .type)
        try container.encode(likesCount, forKey: .likesCount)
        try container.encode(bulbsharesCount, forKey: .bulbsharesCount)
        try container.encode(responsesCount, forKey: .responsesCount)
        try container.encode(commentsCount, forKey: .commentsCount)
        try container.encode(responseLimit, forKey: .responseLimit)
        try container.encode(maxResponses, forKey: .maxResponses)
        try container.encode(status, forKey: .status)
        try container.encode(isPrivate, forKey: .isPrivate)
        try container.encode(isPublic, forKey: .isPublic)
        try container.encode(isActivated, forKey: .isActivated)
        try container.encode(isHidden, forKey: .isHidden)
        try container.encode(isEnrichment, forKey: .isEnrichment)
        try container.encode(organisationid, forKey: .organisationid)
        try container.encode(translateResponses, forKey: .translateResponses)
        try container.encode(userHasResponded, forKey: .userHasResponded)
        try container.encode(userResponsesCount, forKey: .userResponsesCount)
        try container.encode(canSubmitBulbshares, forKey: .canSubmitBulbshares)
        try container.encode(canAnswerPoll, forKey: .canAnswerPoll)
        try container.encode(userRole, forKey: .userRole)
        try container.encode(wasLikedByMe, forKey: .wasLikedByMe)
        try container.encode(isSyndicationBrief, forKey: .isSyndicationBrief)
    }

}
