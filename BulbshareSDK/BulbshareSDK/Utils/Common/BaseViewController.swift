/**
 Base class for all view controllers
 */

import UIKit
@_implementationOnly import IQKeyboardManagerSwift

class BaseViewController: UIViewController {
    
    var callBackToInteractor: ((Bool,SDKError?) -> Void)?
    
    override func viewDidLoad() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    /// Helper function for showing alert view controller
    func showAlertController(withTitle title: String?, message: String, acceptTitle: String, acceptCompletionBlock: (() -> Void)? = nil, cancelTitle: String? = nil, cancelCompletionBlock: (() -> Void)? = nil ) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: (title != nil) ? title: "", message: message, preferredStyle: UIAlertController.Style.alert)
            
            if let title = cancelTitle {
                alert.addAction(UIAlertAction(title: title, style: UIAlertAction.Style.default, handler: { _ in
                    //Cancel Action
                    if let completionBlock = cancelCompletionBlock {
                        completionBlock()
                    }
                }))
            }
            alert.addAction(UIAlertAction(title: acceptTitle,
                                          style: UIAlertAction.Style.default,
                                          handler: { _ in
                if let completionBlock = acceptCompletionBlock {
                    completionBlock()
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func showSingleAlert(message: String, actionTitle: String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func showSingleAlertWithTitle(title: String,message: String, actionTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSingleAlertWithCompletion(message: String, actionTitle: String, completion: (() -> Void)? = nil){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: { _ in
            if let completionBlock = completion {
                completionBlock()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    func showActionSheetAlert(title: String, completion: @escaping (() -> Void)){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: title, style: .destructive, handler: {
          _ in
            completion()
        }))
            alert.addAction(UIAlertAction(title: Alert.Cancel, style: .cancel, handler: nil))
        self.present(alert,animated: true, completion: nil)
    }
}

extension BaseViewController: BaseDelegate {
    func showLoader() {
        DispatchQueue.main.async {
            LoaderUtility.shared.showLoader(onView: self.view)
        }
    }
    
    func hideLoader() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            LoaderUtility.shared.hideLoader(fromView: self.view)
        })
    }
    
    func showError(error: SDKError) {
        callBackToInteractor?(false, error)
        self.showAlertController(withTitle: "error".localised, message: error.failureReason!, acceptTitle: "Ok".localised.description)
    }
}
