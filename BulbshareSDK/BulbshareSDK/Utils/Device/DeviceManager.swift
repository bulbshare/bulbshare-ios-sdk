import Foundation
import  UIKit
class DeviceManager {
    fileprivate let _d: UIDevice = UIDevice.current
     func  device() -> Device {
        let os = self._d.systemName
        let version = self._d.systemVersion
        let name = self._d.name
        let deviceId = self._d.identifierForVendor!.debugDescription
        return Device(device_os: os, device_os_version: version, device_name: name, device_unique_id: deviceId, deviceType: 1)
    }
}
