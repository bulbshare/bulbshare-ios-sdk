import Foundation
struct Device {
    var device_os: String!
    var device_os_version: String!
    var device_name: String!
    var device_unique_id: String!
    var deviceType: Int
    init(device_os: String, device_os_version: String, device_name: String, device_unique_id: String,deviceType: Int) {
        self.device_os = device_os
        self.device_os_version = device_os_version
        self.device_name = device_name
        self.device_unique_id = device_unique_id
        self.deviceType = deviceType
    }
}
