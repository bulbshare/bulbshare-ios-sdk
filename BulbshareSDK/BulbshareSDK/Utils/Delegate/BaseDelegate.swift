/**
 protocol will be used as ErrorAlert view delegate
 Responsible to send notification from presenter to view to show error alert
 */

import UIKit

protocol BaseDelegate: AnyObject {
    func showLoader()
    func hideLoader()
    func showError(error: SDKError)
}
