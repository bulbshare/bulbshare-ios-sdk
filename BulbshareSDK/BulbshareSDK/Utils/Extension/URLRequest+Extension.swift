import Foundation
extension URLRequest {
     mutating func addHTTPHeader(_ headers: HTTPHeaders?) {
        guard let headers = headers else {
            return
        }
        for (key, value) in headers {
            self.setValue(value, forHTTPHeaderField: key)
        }
    }
}
