import Foundation

extension String {
    func convertJsonToDictionary() throws -> [String: AnyObject] {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                guard  let dict =  try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                    return [String: AnyObject]()
                }
                return dict
            } catch {
             throw(error)
            }
        }
        return [String: AnyObject]()
    }
    var localised: String {
        let lang = "en"
        if let bundle = SDKBundles.sdkBundle, let path = bundle.path(forResource: lang, ofType: "lproj"), let langBundle = Bundle(path: path) {
            return NSLocalizedString(self, tableName: nil, bundle: langBundle, value: "", comment: "")
        } else {
            return self
        }
    }
    
    
    // Get file from path
    func getSavedFile() -> URL? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(self)
            return imageUrl
        }
        return nil
    }
    
}

extension Dictionary {
    func convertDictionaryToJson() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: [])
            let jsonString = String(data: jsonData, encoding: .utf8)!
        return jsonString
        } catch {
            return nil
        }
    }
}
