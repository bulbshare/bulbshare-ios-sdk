import UIKit
extension Date {
    func timeAgo(postedTime: String) -> String {
        let dateFormatter = DateFormatter()
        var timeString = ""
        dateFormatter.dateFormat = Briefs.dateFormat
        let postedTimeDate = dateFormatter.date(from: postedTime)
        let currentTimeToString = dateFormatter.string(from: Date())
        let currentDate = dateFormatter.date(from: currentTimeToString)!
        let timeLeft = Calendar.current.dateComponents([.day,.hour,.minute,.month], from: postedTimeDate ?? currentDate, to: currentDate)
        if timeLeft.month! >= 1{
            if let monthTime = timeLeft.month{
                timeString =  "\(monthTime)m ago"
            }
        } else if timeLeft.day! >= 1{
            if let dayTime = timeLeft.day{
                timeString =  "\(dayTime)d ago"
            }
        } else if timeLeft.hour! >= 1{
            if let hourTime = timeLeft.hour{
                timeString =  "\(hourTime)h ago"
            }
        } else if timeLeft.minute! >= 1 {
            timeString =  "just now"
        }
        return timeString
    }
    func timeRemaining(endDate: String,userResponse: Bool) -> (String,UIColor) {
        if userResponse == true{
            return ("completed",UIColor(hexString: ColorConstants.postCompleted))
        } else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = Briefs.dateFormat
            let endsDate = dateFormatter.date(from: endDate)
            let currentTimeToString = dateFormatter.string(from: Date())
            let currentDate = dateFormatter.date(from: currentTimeToString)!
            let timeLeft = Calendar.current.dateComponents([.day,.hour,.minute], from: currentDate, to: endsDate ?? currentDate)
            if timeLeft.day! >= 1{
                if let dayTime = timeLeft.day{
                    return ("\(dayTime)d left",UIColor(hexString: ColorConstants.postUncompleted))
                }
            } else if timeLeft.hour! >= 1 {
                if let hourTime = timeLeft.hour{
                    return ("\(hourTime)h left",UIColor(hexString: ColorConstants.postUncompleted))
                }
            } else if timeLeft.minute! >= 1{
                if let minTime = timeLeft.minute{
                    return ("\(minTime)m left",UIColor(hexString: ColorConstants.postUncompleted))
                }
            } else{
                return ("completed",UIColor(hexString: ColorConstants.postCompleted))
            }
        }
        return ("completed",UIColor(hexString: ColorConstants.postCompleted))
    }
}
