import AVFoundation
import UIKit

extension URL {
    
func getThumbnailImage() -> UIImage? {
    let asset: AVAsset = AVAsset(url: self)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.appliesPreferredTrackTransform=true
    do {
        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60), actualTime: nil)
        return UIImage(cgImage: thumbnailImage)
    } catch let error {
        print(error)
    }
    return nil
}
}
