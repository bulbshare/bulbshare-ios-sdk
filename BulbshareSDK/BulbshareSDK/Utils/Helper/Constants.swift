import Foundation

enum Keys {
    static let sdkVersion = "sdkVersion"
    static let apiVersion = "ApiVersion"
    static let authorization = "Authorization"
    static let secret = "auth_secret"
    static let userRef = "userref"
    static let deviceRef = "deviceref"
    static let timeStamp = "timeStamp"
    
    static func keysForUserDefaults() -> [String] {
        return []
    }
}


enum Constants {
    static let frameworkName = "BulbshareSDK"
    static var secretKey: String {
        let device = DeviceManager().device()
        return "Bulbshare"
        + device.device_os
        + device.device_os_version
        + device.device_name
        + device.device_unique_id
    }
}

enum CurrentVC: Int {
    case singleBulbshare
    case bulbshareList
}

enum BriefIntro {
    static let BriefHeaderVideoViewCell = "BriefHeaderVideoViewCell"
    static let BriefHeaderImageViewCell = "BriefHeaderImageViewCell"
    static let BriefDetailCollectionViewCell = "BriefDetailCollectionViewCell"
    static let BriefDetailProgressViewCell = "BriefDetailProgressViewCell"
    static let BriefDetailTextLabelCell = "BriefDetailTextLabelCell"
    static let BriefDetailImageCell = "BriefDetailImageCell"
    static let BriefDetailGridCollectionViewCell = "BriefDetailGridCollectionViewCell"
    static let CommentViewController = "CommentViewController"
    static let GridCollectionViewCell = "GridCollectionViewCell"
    static let CommentsTableViewCell = "CommentsTableViewCell"
    static let LikeTableViewCell = "LikeTableViewCell"
    static let BriefIntroVerticalCell = "BriefIntroVerticalCell"
    static let LoadingCollectionViewCell = "LoadingCollectionViewCell"
    static let swipeUp = "swipeUp"
    static let liked = "liked"
    static let likeBordered = "likeBordered"
    static let like = "like"
    static let completed = "Completed"
    static let takePart = "Take Part"
    
    
}
enum Polls{
    static let OptionsTableViewCell = "OptionsTableViewCell"
    static let OptionOthersTableViewCell = "OptionOthersTableViewCell"
    static let atleast = "atleast"
    static let exactly = "exactly"
    static let atmost = "atmost"
    static let none = "none"
    
}
enum Briefs{
    static let BriefTableViewCell = "BriefTableViewCell"
    static let liked = "liked"
    static let like = "like"
    static let limit = "999+"
    static let completed = "Completed"
    static let dateFormat = "yyyy-MM-dd HH:mm:ss"
    static let OpenTextQuestionTableViewCell = "OpenTextQuestionTableViewCell"
    static let OpenTextAnswerTableViewCell = "OpenTextAnswerTableViewCell"
}
enum Feeds{
    static let FeedViewController = "FeedViewController"
    static let Feed = "Feed"
    static let FeedsHeadingCollectionViewCell = "FeedsHeadingCollectionViewCell"
    static let BriefIntroViewController = "BriefIntroViewController"
    static let like = "like"
    static let unlike = "unlike"
}

enum PollBrief {
    static let sureTitle = "sureTitle"
    static let pollBriefPermissionDesc = "pollBriefPermissionDesc"
    static let exit = "exit"
    static let cancel = "cancel"
    static let leftRightinfo = "leftRightinfo"
    static let happyWithMedia = "happyWithMedia"
    static let surveyMediaImage = "surveyMediaImage"
    static let surveyMediaVideo = "surveyMediaVideo"
    static let surveyMediaAudio = "surveyMediaAudio"
    static let cameraNotAvailable = "cameraNotAvailable"
    static let alertCommentToShort = "Your Comment is Too Short"
    static let alertCommentToLong = "Your Comment is Too Long"
    static let ok = "ok"
    static let surveyActionMediaPhoto = "surveyActionMediaPhoto"
    static let surveyActionMediaVideo = "surveyActionMediaVideo"
    static let surveyActionMediaAudio = "surveyActionMediaAudio"
    static let surveyActionMediaPhotoUpload = "surveyActionMediaPhotoUpload"
    static let surveyActionMediaVideoUpload = "surveyActionMediaVideoUpload"
    static let errorVideoTooShort = "errorVideoTooShort"
    static let stringConstant = "%02d:%02d"
    static let RecordAudio = "RecordAudio"
}

enum ColorConstants {
    static let greenColor = "#087307"
    static let postCompleted = "#8cc63f"
    static let postUncompleted = "#dfdee4"
    static let redColor = "#EC0000"
    static let orangeColor = "#E78C01"
    static let blueColor = "#0f4BF0"
    static let lightGreenColor = "#4adfe0"
}

enum HighlightType {
    case like
    case dislike
    case question
}

enum CommentType {
    case add
    case edit
}

enum Alert {
    static let PleaseEnterYourComment = "PleaseEnterYourComment".localised
    static let Oops = "Oops".localised
    static let YourCommentIsTooLong = "YourCommentIsTooLong".localised
    static let AreYouSure = "AreYouSure".localised
    static let WantToDeleteComment = "WantToDeleteComment".localised
    static let Ok = "Ok".localised
    static let Cancel = "Cancel".localised
    static let MinimumCharacter = "MinimumCharacter".localised
    static let reportTitle = "Report Bulbshare"
    static let deleteBulbshare = "Delete BulbShare"
    static let reportMessage = "We will remove this Bulbshare if it violates our Comunity Guidelines."
    static let deleteMessage = "Are you sure you want to delete this bulbshare? This cannot be undone."
    static let reportAccept = "Report"
    static let RecorderMinimumTime = "RecorderMinimumTime".localised
    static let DeleteRecording = "DeleteRecording".localised
    static let WantToDeleteRecording = "WantToDeleteRecording".localised
    static let Delete = "Delete".localised
    static let TryAgain = "TryAgain".localised
}

enum MediaType: String {
    case image
    case video
    case audio
    case photo
}

enum ExtensionType: String {
    case jpg
    case png
    case mp4
    case mp3
    case m4a
}
enum Report {
    static let reportTableCell = "ReportTableViewCell"
    static let reportViewController = "ReportViewController"
    static let reportReasonOne = "reportReasonOne".localised
    static let reportReasonTwo = "reportReasonTwo".localised
    static let reportReasonThree = "reportReasonThree".localised
    static let reportReasonFour = "reportReasonFour".localised
    static let reportReasonFive = "reportReasonFive".localised
    static let reportReasonSix = "reportReasonSix".localised
    static let reportReasonSeven = "reportReasonSeven".localised
    static let reportReasonEight = "reportReasonEight".localised
    static let reportReasonNine = "reportReasonNine".localised
    static let reportReasonTen = "reportReasonTen".localised
}

enum SingleBulbshare {
    static let SingleBulbshareStoryBoard = "SingleBulbshare"
    static let SingleBulbshareCollectionViewCell = "SingleBulbshareCollectionViewCell"
    static let SingleBulbshareViewController = "SingleBulbshareViewController"
}

enum SurveyQuestionType: String {
    case one = "1"
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
    case six = "6"
    case seven = "7"
    case eight = "8"
    case nine = "9"
}
