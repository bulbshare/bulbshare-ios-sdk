import UIKit

extension UIView {
    
    func animateClickOnImage(completion: @escaping(() -> Void)) {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 1.0, animations: {() -> Void in
                self.transform = .identity
                completion()
            })
        })
   }
    func animateBorderColor(toColor: UIColor,duration: Double){
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "borderColor")
        animation.fromValue = layer.borderColor
        animation.toValue = toColor.cgColor
        animation.duration = duration
        layer.add(animation,forKey: "borderColor")
        layer.borderColor = toColor.cgColor
    }
    func fadeIn(duration: TimeInterval = 0.2,
                delay: TimeInterval = 0.0,
                completion: @escaping ((Bool) -> Void) = {(_: Bool) -> Void in }) {
       UIView.animate(withDuration: duration,
                      delay: delay,
                      options: UIView.AnimationOptions.curveEaseIn,
                      animations: {
         self.alpha = 1.0
       }, completion: completion)
     }

     func fadeOut(duration: TimeInterval = 0.2,
                  delay: TimeInterval = 0.0,
                  completion: @escaping (Bool) -> Void = {(_: Bool) -> Void in }) {
       UIView.animate(withDuration: duration,
                      delay: delay,
                      options: UIView.AnimationOptions.curveEaseIn,
                      animations: {
         self.alpha = 0.0
       }, completion: completion)
     }
}
