import Foundation

protocol DataEncryptorProtocol {
    static func encryptData(value: String, forKey key: String) -> Data?
    static func decryptData(value: Data, forKey key: String) -> String?

}

class DataEncryptor: DataEncryptorProtocol {

    static func encryptData(value: String, forKey key: String) -> Data? {
        do {
            if let encryptedData = try value.data(using: .utf8)?.aes256Encrypt(withKey: key) {
                return encryptedData
            }
        } catch {
            return nil
        }
        return nil
    }

    static  func decryptData(value: Data, forKey key: String) -> String? {
        do {
            let decryptedData = String(bytes: try value.aes256Decrypt(withKey: key), encoding: .utf8)
            return decryptedData
        } catch {
            return nil
        }
    }
}
