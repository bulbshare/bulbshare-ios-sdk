import Foundation

class SDKBundles {
   static private(set) var appBundle: Bundle! = Bundle.main
   static private(set) var sdkBundle: Bundle? = {
    let bundle = Bundle.allFrameworks.filter({ $0.bundlePath.contains( Constants.frameworkName ) })
    return bundle.first
  }()
  //for testing only
  static func setupApp(bundle: Bundle) {
    appBundle = bundle
  }
}
