import Foundation
internal protocol UserDefaultProtocol {
    static func getUserDefaultForKey(key: String!) -> Any?
    static func saveToUserDefault(value: Any!, key: String!)
    static func removeUserDefaultForKey(key: String)
    static func resetDefaults()
    static func saveEncryptedData(value: Any!, key: String!)
    static func getDecryptedData(key: String!) -> Any?
}

class UserDefault: UserDefaultProtocol {
  //user default
  class var userDefault: UserDefaults {
    return UserDefaults.standard
  }
    /**
     method to get data from user default
     - parameter key: String at which data is saved
     - parameter value: Any data to be fetched
     
     */
    class func getUserDefaultForKey(key: String!) -> Any? {
        return userDefault.object(forKey: key)
    }

    /**
     method to save data to the user default
     - parameter value: Any data to be saved
     - parameter key: String at which data is saved
     */
    class func saveToUserDefault(value: Any!, key: String!) {
        userDefault.set(value, forKey: key)
        userDefault.synchronize()
    }

    /**
     method to remove data from user defaults
     - parameter key: String at which data is saved
     */
    class func removeUserDefaultForKey(key: String) {
        userDefault.removeObject(forKey: key)
        userDefault.synchronize()
    }

    /**
     method to reset user default
     */
    static func resetDefaults() {
        let keys = Keys.keysForUserDefaults()
        keys.forEach { key in
            userDefault.removeObject(forKey: key)
        }
    }

    /**
     method to save encrypted data to the user default
     - parameter value: Any data to be saved
     - parameter key: String at which data is saved
     */
    class func saveEncryptedData(value: Any!, key: String!) {
        guard value != nil else {
            return
        }
        if let value = value as? String {
            guard let  encryptedData = DataEncryptor.encryptData(value: value, forKey: Constants.secretKey) else {
                return
            }
            UserDefault.saveToUserDefault(value: encryptedData, key: key)
        }
    }
    /**
     method to get decrypted data from the user default
     - parameter key: String at which data is saved
     */
    class func getDecryptedData(key: String!) -> Any? {
        guard let data = UserDefault.getUserDefaultForKey(key: key) as? Data else {
            return nil
        }
        if UserDefault.getUserDefaultForKey(key: Keys.sdkVersion) == nil {
                UserDefault.saveEncryptedData(value: data, key: key)
                UserDefault.saveToUserDefault(value: SDKSettings.sdkVersion(), key: Keys.sdkVersion)
        }
        guard let decryptedData = DataEncryptor.decryptData(value: data, forKey: Constants.secretKey) else {
            return nil
        }
        return decryptedData
    }
}
