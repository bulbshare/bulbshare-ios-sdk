import Foundation
import  KeychainAccess

class KeychainAccessHandler {
    let keychain = Keychain(service: SDKBundles.sdkBundle?.bundleIdentifier ?? "com.bulbshare.sdk")
    
    func clearAll() {
        do {
            try keychain.removeAll()
        } catch {
            Logger.debug(arg: "error in keychain clear all")
        }
    }
    func saveUser(model: User) {
        keychain[Keys.secret] = model.authSecret
        keychain[Keys.deviceRef] = model.deviceref
        keychain[Keys.userRef] = model.userref
        Logger.debug(arg: "user info saved successfully")
    }
    func getUserSecret() -> String? {
        return keychain[Keys.secret] ?? nil
    }
    func getUserRef() -> String? {
        return keychain[Keys.userRef] ?? nil
    }
    func getDeviceRef() -> String? {
        return keychain[Keys.deviceRef] ?? nil
    }
    func isUserAuthorized() -> Bool {
        do {
            guard try keychain.get(Keys.secret) != nil, try keychain.get(Keys.userRef) != nil else {
                return false
            }
            return true
        } catch {
            Logger.debug(arg: "keychain data not found")
        }
        return false
    }
    
}
