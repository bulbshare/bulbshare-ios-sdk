import Foundation

enum URLs {
  static var baseURL: String {
    return "https://dev.bulbshare.com"
    guard let value = SDKBundles.appBundle.infoDictionary?["BACKEND_URL"] as? String else {
      return ""
    }
    return value
  }
}
