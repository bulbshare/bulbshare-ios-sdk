/**
 This class will be used as Loade Utility
 Implements singleton pattern
 */

import Foundation
import MBProgressHUD

class LoaderUtility {

    // MARK: - Singleton instance
    static var shared: LoaderUtility = LoaderUtility()

    private init() {}

    // MARK: - Helper methods
    /**
     This method is used to show loader on the provided view
     - parameter view: UIView provided view
     - returns: Void
     */
    func showLoader(onView view: UIView) {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: view, animated: true)
        }
    }

    /**
     This method is used to hide loader from the provided view
     - parameter view: UIView provided view
     - returns: Void
     */
    func hideLoader(fromView view: UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
