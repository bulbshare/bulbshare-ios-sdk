import Foundation
/// this enum meathod is used to print the statements into the console window
enum Logger {
    static let fileName: String = "Logger-\(Date()).txt"
    static func debug(arg: String...) {
        #if DEBUG
        let string = "\(Date()), debug: \(arg.joined(separator: ", "))"
        print(string)
        write(string)
        #endif
    }
    static func error(arg: String...) {
        #if DEBUG
        let string = "\(Date()),  error: \( arg.joined(separator: ", "))"
        print(string)
        write(string)
        #endif
    }
    static func write(_ string: String) {
        let fm = FileManager.default
        let log = fm.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(fileName)
        if let handle = try? FileHandle(forWritingTo: log) {
            handle.seekToEndOfFile()
            handle.write(string.data(using: .utf8)!)
            handle.closeFile()
        } else {
            try? string.data(using: .utf8)?.write(to: log)
        }
    }
}
