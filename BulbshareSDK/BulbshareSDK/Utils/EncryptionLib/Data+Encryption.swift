import Foundation

extension Data {

    func aes256Encrypt(withKey: String) throws -> Data {
        return try (self as NSData).aes256Encrypt(withKey: withKey)
    }

    func aes256Decrypt(withKey: String) throws -> Data {

        return try (self as NSData).aes256Decrypt(withKey: withKey)
    }
}
