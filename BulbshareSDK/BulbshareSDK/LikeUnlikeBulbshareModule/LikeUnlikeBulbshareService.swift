import Foundation
class LikeUnlikeBulbshareService {
    private let likeRouter = ServiceManager<LikeBulbshareAPI>()
    private let unlikeRouter = ServiceManager<UnlikeBulbshareAPI>()
    func likeBulbshare(
        model: APIRequest<CommonBulbshareReqModel>,type: String,
        completion: @escaping (_ response: Bool?,
                               _ error: Error?) -> Void) {
        if type == Feeds.like{
            
            likeRouter.request(.LikeBulbshare(model: model)) { (response, error) in
                guard  error == nil else {
                    completion(nil, error)
                    return
                }
                do {
                    guard (response as? Data) != nil else {
                        completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                        return
                    }
                    completion(true, nil)
                }
            }
        } else if type == Feeds.unlike{
            unlikeRouter.request(.UnlikeBulbshare(model: model)) { response, error in
                guard  error == nil else {
                    completion(nil, error)
                    return
                }
                do {
                    guard (response as? Data) != nil else {
                        completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                        return
                    }
                    completion(true, nil)
                }
            }
        }
        
    }
}
