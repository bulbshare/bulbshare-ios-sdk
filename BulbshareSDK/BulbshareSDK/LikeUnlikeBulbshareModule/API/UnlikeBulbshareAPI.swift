import Foundation

enum UnlikeBulbshareAPI{
    case UnlikeBulbshare(model: APIRequest<CommonBulbshareReqModel>)
}
extension UnlikeBulbshareAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .UnlikeBulbshare:
            return "unlikebulbshare"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .UnlikeBulbshare(let model):
            return CommonBulbshareRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
