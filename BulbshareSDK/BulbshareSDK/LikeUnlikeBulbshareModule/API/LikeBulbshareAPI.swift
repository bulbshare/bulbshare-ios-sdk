import Foundation

enum LikeBulbshareAPI{
    case LikeBulbshare(model: APIRequest<CommonBulbshareReqModel>)
}
extension LikeBulbshareAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .LikeBulbshare:
            return "likebulbshare"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .LikeBulbshare(let model):
            return CommonBulbshareRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
