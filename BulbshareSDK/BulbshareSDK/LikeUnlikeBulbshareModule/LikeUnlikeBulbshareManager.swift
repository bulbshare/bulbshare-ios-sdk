import Foundation
class LikeUnlikeBulbshareManager{
    
    fileprivate var service: LikeUnlikeBulbshareService = LikeUnlikeBulbshareService()
    func likeBulbshare(ref: String,type: String, completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        let arg = CommonBulbshareReqModel(bulbshareref: ref)
        let model = APIRequest<CommonBulbshareReqModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.likeBulbshare(model: model, type: type) { response, error in
            completion(response,error)
        }
    }}
