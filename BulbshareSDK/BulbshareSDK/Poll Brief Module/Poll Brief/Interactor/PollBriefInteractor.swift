import UIKit

class PollBriefInteractor {

    fileprivate weak var delegate: PollBriefResponseDelegate!
    
    init(delegate: PollBriefResponseDelegate) {
        self.delegate = delegate
    }
    
    func showPollBrief(navigation: UINavigationController, briefID: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "PollBrief", bundle: SDKBundles.sdkBundle)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PollBriefVC") as! PollBriefVC
        vc.modalPresentationStyle = .overFullScreen
        vc.briefID = briefID
        vc.callBackToInteractor = { [weak self] (value, error) in
            if value {
                self?.delegate.gotPollBriefDetailsSuccessfully()
            } else if let error = error {
                self?.delegate.throwError(error: error)
            }
        }
        navigation.present(vc, animated: false, completion: nil)
    }
}
