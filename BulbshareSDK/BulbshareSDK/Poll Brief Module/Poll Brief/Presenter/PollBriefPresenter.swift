import Foundation

class PollBriefPresenter {
    
    // MARK: Variables
    fileprivate weak var delegate: PollBriefDelegate!
    fileprivate var manager: PollBriefManager = PollBriefManager()
    
    // MARK: - initializers
    init(delegate: PollBriefDelegate) {
        self.delegate = delegate
    }
    /**
     This method used to map the poll brief views
     */
    func getBrief(briefID: String) {
        self.manager.getPollBriefData(input: PollBriefRequestModel(ref: briefID), completion: { [weak self] (response,error) in
            self?.delegate?.hideLoader()
            if let data = response {
                self?.mapPollBriefView(model: data)
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        })
    }
    
    /**
     This method used to get media upload URL
     */
    func getMediaUploadURL(model: MediaUploadURLReqModel, mediaType: String) {
        self.manager.getMediaUploadURL(input: model, completion: { [weak self] (response,error) in
            if let data = response {
                guard let pngData = self?.getMediaData(mediaType: mediaType, model: model) else {
                    return
                }
                self?.uploadImageOnS3(model: UploadMediaOnS3ReqModel(url: (mediaType == MediaType.photo.rawValue ? data.url: data.videoThumbnailURL)!, data: pngData),pollItemID: model.pollitemid, key: mediaType == MediaType.photo.rawValue ? data.key ?? "": data.videoThumbnailKey ?? "", md5CheckSum: pngData.md5Hash())
            } else if let errorTemp = error as? SDKError {
//                self?.delegate?.showError(error: errorTemp)
            }
        })
    }
    
    func validateConfirmMediaQuesUploadModel(pollItemID: Int, key: String, md5Checksum: String) -> ConfirmMediaQuesUploadReqModel{
        return ConfirmMediaQuesUploadReqModel(pollitemid: pollItemID, key: key, checksum: md5Checksum)
    }
    
    func conformMediaQuesUpload(model: ConfirmMediaQuesUploadReqModel) {
        self.manager.conformMediaQuesUpload(input: model, completion: { [weak self] (response, error) in
            if response != nil {
                Logger.debug(arg: "Media uploaded Successfully")
            } else if let errorTemp = error as? SDKError {
//                self?.delegate?.showError(error: errorTemp)
            }
        })
    }
    /**
     This method used to map the poll brief views
     */
    func uploadImageOnS3(model: UploadMediaOnS3ReqModel, pollItemID: Int, key: String, md5CheckSum: String) {
        self.manager.uploadImageOnS3(input: model, completion: { [weak self] (response,error) in
            if response != nil {
                guard let reqModel = self?.validateConfirmMediaQuesUploadModel(pollItemID: pollItemID, key: key, md5Checksum: md5CheckSum) else {
                    return
                }
                self?.conformMediaQuesUpload(model: reqModel)
            } else if let errorTemp = error as? SDKError {
//                self?.delegate?.showError(error: errorTemp)
            }
        })
    }
    
    /**
     This method used to map the poll brief views
     */
    fileprivate func mapPollBriefView(model: [PollBriefResponse]) {
        let profileDetails = PollBriefMapper.map(model: model)
        self.delegate?.getPollbriefdata(view: profileDetails)
    }
    
    
    func isNextButtonShow(model: PollBriefResponse) -> Bool {
        switch model.type {
        case SurveyQuestionType.one.rawValue, SurveyQuestionType.two.rawValue, SurveyQuestionType.seven.rawValue, SurveyQuestionType.eight.rawValue:
            return false
        case SurveyQuestionType.three.rawValue, SurveyQuestionType.four.rawValue, SurveyQuestionType.nine.rawValue, SurveyQuestionType.five.rawValue :
            return true
        default:
            break
        }
        return false
    }
    
    func showBGImageNoBrief(model: PollBriefResponse) -> Bool {
        switch model.type {
        case SurveyQuestionType.one.rawValue, SurveyQuestionType.two.rawValue, SurveyQuestionType.seven.rawValue, SurveyQuestionType.three.rawValue, SurveyQuestionType.four.rawValue:
            return false
        case SurveyQuestionType.eight.rawValue :
            return true
        default:
            break
        }
        return false
    }
    func isBottomViewHide(model: PollBriefResponse) -> Bool {
        switch model.type {
        case SurveyQuestionType.five.rawValue:
            return true
        default:
            break
        }
        return false
    }
    
    func titleText(model: PollBriefResponse) -> String {
        switch model.type {
        case SurveyQuestionType.one.rawValue, SurveyQuestionType.two.rawValue, SurveyQuestionType.seven.rawValue:
            return model.title
        case SurveyQuestionType.three.rawValue, SurveyQuestionType.four.rawValue, SurveyQuestionType.eight.rawValue, SurveyQuestionType.nine.rawValue :
            return ""
        default:
            break
        }
        return ""
    }
    
    func description( model: PollBriefResponse) -> String {
        switch model.type {
        case SurveyQuestionType.one.rawValue, SurveyQuestionType.seven.rawValue:
            return model.description
        case SurveyQuestionType.two.rawValue:
            return model.description + "\n" + PollBrief.leftRightinfo.localised.description
        case SurveyQuestionType.three.rawValue, SurveyQuestionType.four.rawValue, SurveyQuestionType.eight.rawValue, SurveyQuestionType.nine.rawValue :
            return ""
        default:
            break
        }
        return ""
    }
    
    
    func validateMediaType(model: PollBriefResponse) ->(cameraText: String, galleryText: String, mediaType: MediaType)? {
        switch model.mediatype {
        case MediaType.photo.rawValue:
            return (PollBrief.surveyActionMediaPhoto.localised, PollBrief.surveyActionMediaPhotoUpload.localised, .photo)
        case MediaType.video.rawValue:
            return (PollBrief.surveyActionMediaVideo.localised, PollBrief.surveyActionMediaVideoUpload.localised, .video)
        case MediaType.audio.rawValue:
            return ("", "", .audio)
        case .none:
            break
        case .some(_):
            break
        }
        return nil
    }
    
    func getMediaData(mediaType: String, model: MediaUploadURLReqModel) -> Data? {
        switch mediaType {
        case MediaType.photo.rawValue:
            guard let mediaURL: URL = "image.jpeg".getSavedFile() else {
                return nil
            }
            do {
                guard let pngData = try? Data(contentsOf: mediaURL) else{
                    return nil
                }
                return pngData
            }
        case MediaType.video.rawValue:
            guard let mediaURL: URL = "video.mov".getSavedFile() else {
                return nil
            }
            do {
                guard let pngData = try? Data(contentsOf: mediaURL) else{
                    return nil
                }
                return pngData
            }
        case MediaType.audio.rawValue:
            Logger.debug(arg: "validateMediaType audio: \(mediaType)")
        default:
            break
        }
        return nil
    }
    
}
