import UIKit
import Foundation
import CommonCrypto
import AVFoundation

class PollBriefVC: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var collectionView: PollBriefCollectionView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var labelPercentage: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var buttonCross: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var imageNoBrief: UIImageView!
    
    // MARK: - Local variables
    fileprivate var presenter: PollBriefPresenter!
    fileprivate var showTempMediaView: UIView!
    fileprivate var pollViewModel: [PollBriefViewModel]!
    fileprivate var selectedIndexRow: Int = 0
    fileprivate var totalViews: Int!
    fileprivate var perIndexPercentage: Float!
    fileprivate var player: AVPlayer!
    var imagePicker: ImagePicker!
    var videoPicker: VideoPicker!
    var briefID: String!
    var ischaracterValidate: Bool!
    
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextButton.isHidden = true
        presenter = PollBriefPresenter(delegate: self)
        self.presenter.getBrief(briefID: briefID)
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.videoPicker = VideoPicker(presentationController: self, delegate: self)
    }
    
    func setupBottomView(model: PollBriefResponse) {
        DispatchQueue.main.async {
            let titleText = self.presenter.titleText(model: model)
            let descText = self.presenter.description(model: model)
            self.labelDesc.text = descText
            self.labelTitle.text = titleText
            self.titleView.isHidden = titleText == "" ? true: false
            self.descView.isHidden = descText == "" ? true: false
            self.progressView.isHidden = false
            
            self.imageNoBrief.isHidden = self.presenter.showBGImageNoBrief(model: model)
            if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.itemSize = CGSize(width: self.collectionView.frame.width, height: self.collectionView.frame.size.height)
                layout.invalidateLayout()
            }
            let percentageIntCount = Int(self.perIndexPercentage * Float(self.selectedIndexRow))
            self.labelPercentage.text = String(percentageIntCount) + "%"
            let percentageCount = (self.perIndexPercentage * Float(self.selectedIndexRow)) / 100
            self.progressBar.progress = percentageCount
            self.isNextButtonShow(model: model)
        }
    }
    
    func hideBottomView(model: PollBriefResponse) {
        DispatchQueue.main.async {
            self.nextButton.isUserInteractionEnabled = false
            self.nextButton.alpha = 1
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        if selectedIndexRow == 0 {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.showAlertController(withTitle: PollBrief.sureTitle.localised.description, message: PollBrief.pollBriefPermissionDesc.localised.description, acceptTitle: PollBrief.exit.localised.description, acceptCompletionBlock: { [weak self] in
                self?.dismiss(animated: true, completion: nil)
            }, cancelTitle: PollBrief.cancel.localised.description, cancelCompletionBlock: nil)
        }
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        if ischaracterValidate{
            self.showSingleAlertWithTitle(title: Alert.Oops, message: Alert.MinimumCharacter, actionTitle: Alert.Ok)
        } else {
            self.presenter.getMediaUploadURL(model: self.createMediaUploadURLReqModel(model: self.pollViewModel[selectedIndexRow].model!), mediaType: self.pollViewModel[selectedIndexRow].model?.mediatype ?? "")
            if selectedIndexRow < self.totalViews-1 {
                selectedIndexRow += 1
                self.collectionView.validateNext(index: selectedIndexRow)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
     
    }
    
    func isNextButtonShow(model: PollBriefResponse) {
        nextButton.isHidden = !self.presenter.isNextButtonShow(model: model)
    }
    
    func validateNextButton(value: Bool) {
        nextButton.isUserInteractionEnabled = value
        nextButton.alpha = value ? 1.0: 0.5
    }
}
// MARK: - Poll Brief Delegate
extension PollBriefVC: PollBriefDelegate {
    func showMedia(mediatype: MediaType) {
        showTempMediaView = UIView(frame: self.view.bounds)
        showTempMediaView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        switch mediatype {
        case .photo, .image:
            guard let mediaURL: URL = "image.jpeg".getSavedFile() else {
                return
            }
            showTempMediaView.backgroundColor = .black
            let imageView  = UIImageView(frame: self.showTempMediaView.frame)
            imageView.image = UIImage(contentsOfFile: mediaURL.path)
            imageView.contentMode = .scaleAspectFit
            self.showTempMediaView.addSubview(imageView)
        case .video:
            guard let mediaURL: URL = "video.mov".getSavedFile() else {
                return
            }
            player = AVPlayer(url: mediaURL)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = CGRect(x: 0, y: 0, width: showTempMediaView.frame.width, height: showTempMediaView.frame.height)
            playerLayer.videoGravity = .resizeAspectFill
            player.play()
            showTempMediaView.layer.insertSublayer(playerLayer, at: 0)
        case .audio:
            break
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(mediaTapped(tapGestureRecognizer:)))
        showTempMediaView.isUserInteractionEnabled = true
        showTempMediaView.addGestureRecognizer(tapGestureRecognizer)
        self.view.addSubview(showTempMediaView)
    }
    
    @objc func mediaTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if player != nil {
            self.player.pause()
        }
        self.showTempMediaView.removeFromSuperview()
    }
    func openImage(usingCamera: Bool, model: PollBriefResponse) {
        guard UIImagePickerController.isSourceTypeAvailable(usingCamera ? .camera: .photoLibrary) else {
            self.showAlertController(withTitle: "", message: PollBrief.cameraNotAvailable.localised, acceptTitle: "Ok".localised.description)
            return
        }
        self.imagePicker.presentImagePicker(from: self.view, sourcetype: usingCamera ? .camera: .photoLibrary)
    }
    func createMediaUploadURLReqModel(model: PollBriefResponse) -> MediaUploadURLReqModel{
        return MediaUploadURLReqModel(briefref: self.briefID ?? "", pollitemid: Int(model.pollitemid) ?? 0, media_type: model.mediatype == MediaType.photo.rawValue ? MediaType.image.rawValue: MediaType.video.rawValue, media_ext: model.mediatype == MediaType.photo.rawValue ? ExtensionType.jpg.rawValue: ExtensionType.mp4.rawValue)
    }
    
    func openVideoCamera(usingCamera: Bool, model: PollBriefResponse) {
        self.videoPicker = VideoPicker(presentationController: self, delegate: self)
        guard UIImagePickerController.isSourceTypeAvailable(usingCamera ? .camera: .photoLibrary) else {
            self.showAlertController(withTitle: "", message: PollBrief.cameraNotAvailable.localised, acceptTitle: "Ok".localised.description)
            return
        }
        self.videoPicker.present(from: self.view, sourceType: usingCamera ? .camera: .photoLibrary)
    }
    
    func showSingleAlertmessage(message: String, actionTitle: String) {
        self.showSingleAlert(message: message, actionTitle: actionTitle)
    }
    
    func showAlert(withTitle: String, message: String, acceptTitle: String, cancelTitle: String, acceptCompletionBlock: (() -> Void)? = nil, cancelCompletionBlock: (() -> Void)? = nil) {
        self.showAlertController(withTitle: withTitle, message: message, acceptTitle: acceptTitle, acceptCompletionBlock: acceptCompletionBlock, cancelTitle: cancelTitle, cancelCompletionBlock: cancelCompletionBlock)
    }
    
    func showAlertWithCompletion(message: String, actionTitle: String, completion: (() -> Void)? = nil ) {
        self.showSingleAlertWithCompletion(message: message, actionTitle: actionTitle, completion: completion)
    }
    
    func isBottomViewHidden(value: Bool) {
        self.titleView.isHidden = value
        self.descView.isHidden = value
        self.progressView.isHidden = value
    }
    
    func isNextBtnHidden(value: Bool) {
        self.nextButton.isHidden = value
        if value == false {
            self.nextButton.isUserInteractionEnabled = true
            self.nextButton.alpha = 1.0
        }
    }
    
    // MARK: - Helper methods
    /**
     This method set the Poll brief data
     - parameters PollBriefViewModel: A/B Setup type bind data having cell type
     */
    func getPollbriefdata(view: [PollBriefViewModel]) {
        self.selectedIndexRow = 0
        self.totalViews = view.count
        self.pollViewModel = view
        self.perIndexPercentage = Float(100 / self.totalViews)
        self.collectionView.showPollFor(views: view, delegate: self)
        if let model = view[selectedIndexRow].model {
            self.setupBottomView(model: model)
            self.callBackToInteractor?(true, nil)
        }
    }
    
    /**
     This method call when model validate
     - parameters PollBriefResponseData: A/B Setup type data
     */
    func isModelValidated(value: Bool, model: PollBriefResponse?, characterValidation: Bool) {
        if let modelData = model {
            self.setupBottomView(model: modelData)
            self.hideBottomView(model: modelData)
        } else {
            validateNextButton(value: value)
            self.ischaracterValidate = characterValidation
        }
    }
    /**
     This method call to move next type
     - parameters value: Bool type
     */
    func enableNext(value: Bool) {
        if selectedIndexRow < self.totalViews-1 {
            selectedIndexRow += 1
            self.collectionView.validateNext(index: selectedIndexRow)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func validateCrossButton(value: Bool) {
        self.buttonCross.isHidden = value
    }
}

extension PollBriefVC: VideoPickerDelegate {
    func didSelect(url: URL?) {
        if let mediaURL = url {
            do {
                let pngData = try Data(contentsOf: mediaURL)
                if let imageMedia = mediaURL.getThumbnailImage() {
                    let videoFile = getDocumentsDirectory().appendingPathComponent("video.mov")
                    try? pngData.write(to: videoFile)
                    let videoDataDict: [String: UIImage] = [MediaType.image.rawValue: imageMedia]
                    NotificationCenter.default.post(name: Notification.Name("imagePicker"), object: nil, userInfo: videoDataDict)
                }
            } catch {
                print(error)
            }
        } else {
            self.showSingleAlertmessage(message: PollBrief.errorVideoTooShort.localised, actionTitle: "Ok")
        }
    }
}
extension PollBriefVC: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let image = image else {
            return
        }
        if let pngData: Data = image.jpegData(compressionQuality: 0.9) {
            let imageFile = getDocumentsDirectory().appendingPathComponent("image.jpeg")
            try? pngData.write(to: imageFile)
            let imageDataDict: [String: UIImage] = [MediaType.image.rawValue: image]
            NotificationCenter.default.post(name: Notification.Name("imagePicker"), object: nil, userInfo: imageDataDict)
        }
    }
}
