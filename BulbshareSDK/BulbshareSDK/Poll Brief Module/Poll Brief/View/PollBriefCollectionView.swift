import UIKit

class PollBriefCollectionView: UICollectionView {

    // MARK: - Local variables
    fileprivate var pollViews: [PollBriefViewModel]!
    fileprivate weak var pollDelegate: PollBriefDelegate!
    fileprivate var selectedRow = 0

    // MARK: - Life cycle methods
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        self.allowsSelection = false
    }

    // MARK: - Helper methods
   
    func showPollFor(views: [PollBriefViewModel], delegate: PollBriefDelegate) {
        self.pollDelegate = delegate
        self.pollViews = views
        selectedRow = 0
        DispatchQueue.main.async {
            for poll in self.pollViews {
                self.register(UINib(nibName: String(describing: poll.cellType), bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: String(describing: poll.cellType))
            }
                self.reloadData()
                self.contentInsetAdjustmentBehavior = .never
        }
    }

    /**
    This method validate the current view's datamodel and allow to move next
    - parameters index: Int
    */
    func validateNext(index: Int) {
        self.pollDelegate?.isModelValidated(value: true, model: self.pollViews[index].model, characterValidation: false)
        self.isPagingEnabled = false
        self.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: false)
    }
}

// MARK: - Collection view datasource and delegate
extension PollBriefCollectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.pollViews != nil else {
            return 0
        }
        return self.pollViews.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: PollBaseCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: self.pollViews[indexPath.row].cellType), for: indexPath) as? PollBaseCollectionCell else {
            return UICollectionViewCell()
        }
            cell.bind(model: self.pollViews[indexPath.row].model!, delegate: pollDelegate)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = self.frame.size.width
        let height = self.frame.size.height
        return CGSize(width: width, height: height)
    }
}
