import Foundation
class PollBriefManager {
    // MARK: Variables
    fileprivate var service: PollBriefService = PollBriefService()
    /**
     method for get poll brief data
     - parameter PollBriefRequestModel: having deviceRef
     */
    func getPollBriefData(input: PollBriefRequestModel, completion: @escaping (_ response: [PollBriefResponse]?, _ error: Error?) -> Void) {
        let args = PollBriefRequestModel(ref: input.ref)
        let model = APIRequest<PollBriefRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: args)
        self.service.getPollBreifs(model: model) { (data, error) in
            completion(data, error)
        }
    }
    
    /**
     method for get poll brief data
     - parameter PollBriefRequestModel: having deviceRef
     */
    func getMediaUploadURL(input: MediaUploadURLReqModel, completion: @escaping (_ response: MediaUploadResponse?, _ error: Error?) -> Void) {
        let model = APIRequest<MediaUploadURLReqModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: input)
        self.service.getUploadMediaURL(model: model) { (data, error) in
            completion(data, error)
        }
    }
    
    func conformMediaQuesUpload(input: ConfirmMediaQuesUploadReqModel, completion: @escaping (_ response: MediaUploadResponse?, _ error: Error?) -> Void) {
        let model = APIRequest<ConfirmMediaQuesUploadReqModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: input)
        self.service.conformMediaQuesUpload(model: model) { (data, error) in
            completion(data, error)
        }
    }
    func uploadImageOnS3(input: UploadMediaOnS3ReqModel, completion: @escaping (_ response: Bool?, _ error: Error?) -> Void) {
        self.service.uploadImageOnS3(model: input) { (data, error) in
            completion(data, error)
        }
    }
}
