import Foundation

class PollBriefMapper {
    class func map(model: [PollBriefResponse]) -> [PollBriefViewModel] {
        var array: [PollBriefViewModel] = []
        
        for data in model {
            switch data.type {
            case "1":
                let type1 = PollBriefViewModel(model: data, cellType: ABTypeCollectionCell.self)
                array.append(type1)
            case "2":
                let type2 = PollBriefViewModel(model: data, cellType: SwipeImageCell.self)
                array.append(type2)
            case "3":
                let type3 = PollBriefViewModel(model: data, cellType: MCQTypeCell.self)
                array.append(type3)
            case "4":
                let type4 = PollBriefViewModel(model: data, cellType: OpenTextCollectionCell.self)
                array.append(type4)
            case "5":
                let type5 = PollBriefViewModel(model: data, cellType: InfoImageCollectionCell.self)
                array.append(type5)
            case "7":
                let type7 = PollBriefViewModel(model: data, cellType: HighlightImageCell.self)
                array.append(type7)
            case "8":
                let type8 = PollBriefViewModel(model: data, cellType: MediaUploadCell.self)
                array.append(type8)
            case "9":
                let type9 = PollBriefViewModel(model: data, cellType: ImageGridCollectionCell.self)
                array.append(type9)
            default:
                break
            }
        }
        return array
    }
    
    
    class func mapMCQ(model: [Collection]) -> ([PollBriefMCQViewModel]) {
        var array: [PollBriefMCQViewModel] = []
        
        for value in model{
            if value.value == "other" {
                let type1 = PollBriefMCQViewModel(model: value, cellType: OptionOthersTableViewCell.self)
                array.append(type1)
            } else {
                let type2 = PollBriefMCQViewModel(model: value, cellType: OptionsTableViewCell.self)
                array.append(type2)
            }
        }
        return array
    }
}
