
import Foundation
class PollBriefService {
    // MARK: Variables
    private let router = ServiceManager<PollBriefAPI>()
    
    func getPollBreifs(
        model: APIRequest<PollBriefRequestModel>,
        completion: @escaping (_ response: [PollBriefResponse]?,
                               _ error: Error?) -> Void) {
        
        router.request(.getPollBriefs(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<[PollBriefResponse]?>.self, from: response)
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
    
    func getUploadMediaURL(
        model: APIRequest<MediaUploadURLReqModel>,
        completion: @escaping (_ response: MediaUploadResponse?,
                               _ error: Error?) -> Void) {
        router.request(.requestMediaUploadURL(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<MediaUploadResponse?>.self, from: response)
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
    
    func conformMediaQuesUpload(
        model: APIRequest<ConfirmMediaQuesUploadReqModel>,
        completion: @escaping (_ response: MediaUploadResponse?,
                               _ error: Error?) -> Void) {
        router.request(.requestConfirmMediaQuesUpload(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<MediaUploadResponse>.self, from: response)
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
    
    
    func uploadImageOnS3(
        model: UploadMediaOnS3ReqModel,
        completion: @escaping (_ response: Bool?,
                               _ error: Error?) -> Void) {
        router.multipartRequest(.uploadMediaOnS3(model: model), url: model.url, data: model.data, completion: { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Bool else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                completion(response, error)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        })
    }
}
