import Foundation

protocol PollBriefDelegate: BaseDelegate {
    func getPollbriefdata(view: [PollBriefViewModel])
    func isModelValidated(value: Bool, model: PollBriefResponse?, characterValidation: Bool)
    func enableNext(value: Bool)
    func validateCrossButton(value: Bool)
    func openImage(usingCamera: Bool, model: PollBriefResponse)
    func isNextBtnHidden(value: Bool)
    func isBottomViewHidden(value: Bool)
    func showSingleAlertmessage(message: String,actionTitle: String)
    func openVideoCamera(usingCamera: Bool, model: PollBriefResponse)
    func showMedia(mediatype: MediaType)
    func showAlert(withTitle: String, message: String, acceptTitle: String, cancelTitle: String, acceptCompletionBlock: (() -> Void)?, cancelCompletionBlock: (() -> Void)?)
    func showAlertWithCompletion(message: String, actionTitle: String, completion: (() -> Void)?)
}
