import Foundation

class PollBriefViewModel {
    var model: PollBriefResponse?
    var cellType: PollBaseCollectionCell.Type = PollBaseCollectionCell.self

    // MARK: - initializer
    init(model: PollBriefResponse? = nil, cellType: PollBaseCollectionCell.Type) {
        self.model = model
        self.cellType = cellType
    }
}

class PollBriefMCQViewModel {
    var model: Collection?
    var cellType: PollBaseTableCell.Type = PollBaseTableCell.self

    // MARK: - initializer
    init(model: Collection? = nil, cellType: PollBaseTableCell.Type) {
        self.model = model
        self.cellType = cellType
    }
}
