import Foundation

// MARK: - MediaUploadResData
struct MediaUploadResponse: Codable {
    let item, key, videoThumbnailKey, videoThumbnailURL: String?
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case item, key, url
        case videoThumbnailKey = "videotn_key"
        case videoThumbnailURL = "videotn_url"
    }
}
