
import Foundation
// MARK: - get Media Upload URL Request
struct MediaUploadURLReqModel: Codable {
    let briefref: String
    let pollitemid: Int
    let media_type: String
    let media_ext: String
}

class MediaUploadURLRequest {
    func params(model: APIRequest<MediaUploadURLReqModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
