
import Foundation
// MARK: - Confirm Media Question Upload Request Model
struct ConfirmMediaQuesUploadReqModel: Codable {
    let pollitemid: Int
    let key: String
    let checksum: String
}

class ConfirmMediaQuesUploadRequest {
    func params(model: APIRequest<ConfirmMediaQuesUploadReqModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
