import Foundation

// MARK: - PollBriefRequestModel
struct PollBriefRequestModel: Codable {
    let ref: String
}

class PollBriefRequest {
    func params(model: APIRequest<PollBriefRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
