import Foundation

// MARK: - Upload Media On S3 Request
struct UploadMediaOnS3ReqModel: Codable {
    let url: String
    let data: Data
}

class UploadMediaOnS3Request {
    func params(model: UploadMediaOnS3ReqModel) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
