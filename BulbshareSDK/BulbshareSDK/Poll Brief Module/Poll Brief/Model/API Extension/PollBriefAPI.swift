import Foundation

enum PollBriefAPI{
    case getPollBriefs(model: APIRequest<PollBriefRequestModel>)
    case requestMediaUploadURL(model: APIRequest<MediaUploadURLReqModel>)
    case requestConfirmMediaQuesUpload(model: APIRequest<ConfirmMediaQuesUploadReqModel>)
    case uploadMediaOnS3(model: UploadMediaOnS3ReqModel)
    }
extension PollBriefAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .getPollBriefs:
            return "getbriefpoll"
        case .requestMediaUploadURL:
            return "requestmediaquestionuploadurl"
        case .uploadMediaOnS3:
            return ""
        case .requestConfirmMediaQuesUpload:
            return "confirmmediaquestionupload"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getPollBriefs, .requestMediaUploadURL:
            return .post
        case .uploadMediaOnS3:
            return .put
        case .requestConfirmMediaQuesUpload:
            return .post
        }
        
    }
    
    var parameters: Parameters? {
        switch self {
        case .getPollBriefs(let model):
            return PollBriefRequest().params(model: model)
        case .requestMediaUploadURL(let model):
            return MediaUploadURLRequest().params(model: model)
        case .uploadMediaOnS3(let model):
            return UploadMediaOnS3Request().params(model: model)
        case .requestConfirmMediaQuesUpload(let model):
            return ConfirmMediaQuesUploadRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
}
