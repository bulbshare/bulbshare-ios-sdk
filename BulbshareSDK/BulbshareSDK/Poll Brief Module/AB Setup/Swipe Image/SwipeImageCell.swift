
import UIKit

class SwipeImageCell: PollBaseCollectionCell {

     // MARK: - IBOutlets
    @IBOutlet weak var swipeImageInfoView: SwipeImageView!

    // MARK: - Helper methods
    
    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        swipeImageInfoView.bind(model: model, delegate: delegate)
    }
}
