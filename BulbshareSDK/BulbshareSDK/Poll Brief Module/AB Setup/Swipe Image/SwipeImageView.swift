import UIKit
@_implementationOnly import SDWebImage
@_implementationOnly import Koloda

class SwipeImageView: UIView {
    
    @IBOutlet weak var kolodaView: KolodaView!
    
    // MARK: - Local properties
    fileprivate var model: PollBriefResponse?
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Helper methods
    func bind(model: PollBriefResponse?, delegate: PollBriefDelegate) {
        if let introModel = model {
            self.delegate = delegate
            self.model = introModel
            kolodaView.dataSource = self
            kolodaView.delegate = self
        }
    }
}
extension SwipeImageView: KolodaViewDelegate {
    func koloda(_ koloda: KolodaView, draggedCardWithPercentage finishPercentage: CGFloat, in direction: SwipeResultDirection) {
        if finishPercentage == 100 {
            self.delegate?.enableNext(value: true)
            self.removeFromSuperview()
        }
    }
}

extension SwipeImageView: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return 1
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let customView = CustomOverlayView()
        if let modeldata = self.model, let data = modeldata.collection?[0] {
            customView.bind(model: data)
        }
        return customView
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        let customView = CustomOverlayView()
        return customView
    }
}
