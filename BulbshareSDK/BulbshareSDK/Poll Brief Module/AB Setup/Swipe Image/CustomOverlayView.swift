import UIKit
@_implementationOnly import Koloda
@_implementationOnly import SDWebImage

class CustomOverlayView: OverlayView {
    
    private var setView: UIView!
    @IBOutlet weak var labelOverlayText: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Helper methods
    func bind(model: Collection) {
        if let imageURL = model.image, let overlayText = model.overlayText {
            self.imgView.sd_setShowActivityIndicatorView(true)
            self.imgView.sd_setIndicatorStyle(.white)
            self.imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: nil)
            labelOverlayText.text = overlayText
        }
    }
}
