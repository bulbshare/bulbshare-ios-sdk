
import UIKit

class OpenTextCollectionCell: PollBaseCollectionCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var OpenTextInfoView: OpenTextView!

    // MARK: - Helper methods

    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        OpenTextInfoView.bind(model: model, delegate: delegate)
    }
}
