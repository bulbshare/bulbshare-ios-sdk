import UIKit

class OpenTextView: UIView {
    
    // MARK: - Local properties
    fileprivate var model: PollBriefResponse!
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    // MARK: - IBOutlets
    @IBOutlet weak var answerTextView: CustomTextView!
    @IBOutlet weak var questionLabel: UILabel!
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)

    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.setView.endEditing(true)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    // MARK: - Helper methods
    func bind(model: PollBriefResponse?, delegate: PollBriefDelegate) {
        self.setUpUI()
        if let introModel = model {
            self.delegate = delegate
            self.answerTextView.delegate = self
            self.model = introModel
            self.delegate.isModelValidated(value: false, model: nil, characterValidation: false)
            self.questionLabel.text = model?.title
            self.answerTextView.placeholder = model?.description ?? ""
        }
    }
    func setUpUI() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.setView.addGestureRecognizer(tap)
    }
}
extension OpenTextView: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        validateNextBtn(text: newString as String)
        return true
    }
    func validateNextBtn(text: String){
        if text.count > 0 {
            if text.count < model.minLength ?? 10{
                self.delegate.isModelValidated(value: true, model: nil, characterValidation: true)
            } else{
                self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
            }
        } else {
            self.delegate.isModelValidated(value: false, model: nil, characterValidation: false)
        }
    }
}
