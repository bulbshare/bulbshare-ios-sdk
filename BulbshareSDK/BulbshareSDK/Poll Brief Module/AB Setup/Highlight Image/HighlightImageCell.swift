import UIKit

class HighlightImageCell: PollBaseCollectionCell {

    // MARK: - IBOutlets
    @IBOutlet weak var highlightImageInfoView: HighlightImageView!
    // MARK: - Helper methods
    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        highlightImageInfoView.bind(model: model, delegate: delegate)
    }
}
