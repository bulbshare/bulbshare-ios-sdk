import UIKit
struct MarkupModel {
    var comment: String
    let view: UIView
    let point: CGPoint
    let tag: Int
    let fieldType: HighlightType
    
    enum CodingKeys: String, CodingKey {
        case comment, view, point, tag, fieldType
    }
}
