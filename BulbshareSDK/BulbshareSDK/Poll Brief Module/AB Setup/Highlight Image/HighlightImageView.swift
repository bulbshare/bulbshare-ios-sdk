import UIKit
import Foundation

class HighlightImageView: UIView {
    
    // MARK: - Local properties
    fileprivate var model: PollBriefResponse!
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    var markupView: ABMarkupTopView!
    var collectionOfViews: UIView!
    var tagCount = 99
    var arrOfHighlightModel = [MarkupModel]()
    var selectedType: HighlightType!
    var likeColor = UIColor.init(hexString: ColorConstants.greenColor, alpha: 1.0)
    var dislikeColor = UIColor.init(hexString: ColorConstants.redColor, alpha: 1.0)
    var quesColor = UIColor.init(hexString: ColorConstants.orangeColor, alpha: 1.0)
    let markupViewWidth = 335
    let markupViewHeight = 94
    
    // MARK: - IBOutlet
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var disLikeButton: UIButton!
    @IBOutlet weak var quesButton: UIButton!
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if collectionOfViews != nil {
            collectionOfViews.frame = CGRect(x: 0, y: 0, width: self.imgView.frame.width, height: self.imgView.frame.height)
        }
        self.bringSubviewToFront(bottomView)
    }
    // MARK: - Helper methods
    func bind(model: PollBriefResponse?, delegate: PollBriefDelegate) {
        
        if let introModel = model, let imageURL = introModel.image_url {
            collectionOfViews = UIView()
            
            collectionOfViews.backgroundColor = .clear
            collectionOfViews.frame = CGRect(x: 0, y: 0, width: self.imgView.frame.width, height: self.imgView.frame.height)
            self.collectionOfViews.layoutIfNeeded()
            self.addSubview(collectionOfViews)
            self.delegate = delegate
            self.model = introModel
            imgView.sd_setShowActivityIndicatorView(true)
            imgView.sd_setIndicatorStyle(.white)
            self.imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: nil)
            let screenWidth = UIScreen.main.bounds.size.width
            markupView = ABMarkupTopView(frame: CGRect(x: (Int(screenWidth) - markupViewWidth) / 2, y: ((Int(imgView.frame.size.height) - markupViewHeight) / 2) - 10, width: markupViewWidth, height: markupViewHeight))
            self.addSubview(markupView)
            markupvalidate(fieldType: .like)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            collectionOfViews.isUserInteractionEnabled = true
            collectionOfViews.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        guard markupView == nil else {
            self.removeMarkupView()
            return
        }
        var selectedModel: MarkupModel!
        let touchPoint: CGPoint = tapGestureRecognizer.location(in: self.imgView)
        let filteredSubview = collectionOfViews.subviews.filter { subView -> Bool in
            return subView.frame.contains(touchPoint)
        }.first
        if let filtered = filteredSubview {
            let filterData = arrOfHighlightModel.filter{$0.tag == filtered.tag}.first
            selectedModel = filterData
        }
        if selectedModel != nil {
            self.delegate.isNextBtnHidden(value: true)
            let tempView = CommentView(frame: CGRect(x: self.imgView.frame.origin.x, y: self.imgView.frame.origin.y, width: self.imgView.frame.width, height: self.imgView.frame.height + self.bottomView.frame.height))
            tempView.backgroundImage.isHidden = true
            tempView.backgroundView.isHidden = true
            tempView.bindData(text: selectedModel.comment, actionType: .edit, model: self.model)
            self.delegate?.validateCrossButton(value: true)
            tempView.deleteCallBack = { [weak self] in
                tempView.removeFromSuperview()
                if let subView = filteredSubview {
                    let index = self?.arrOfHighlightModel.firstIndex{$0.tag == subView.tag}
                    if let indxdat = index {
                        self?.delegate?.validateCrossButton(value: false)
                        self?.arrOfHighlightModel.remove(at: indxdat)
                        self?.validateNextButton()
                    }
                    subView.removeFromSuperview()
                }
            }
            tempView.doneCallback = { [weak self] text in
                tempView.removeFromSuperview()
                if let filtered1 = filteredSubview {
                    let index = self?.arrOfHighlightModel.firstIndex{$0.tag == filtered1.tag}
                    if let indxdat = index {
                        selectedModel.comment = text
                        self?.arrOfHighlightModel[indxdat] = selectedModel
                        self?.delegate?.validateCrossButton(value: false)
                        self?.validateNextButton()
                    }
                }
            }
            self.addSubview(tempView)
        } else {
            let viewHighlight = MarkupView()
            viewHighlight.frame.size = CGSize(width: 50, height: 50)
            viewHighlight.center = CGPoint(x: CGFloat(touchPoint.x), y: CGFloat(touchPoint.y))
            viewHighlight.binddata(type: selectedType)
            viewHighlight.tag = tagCount
            selectedModel = MarkupModel(comment: "", view: viewHighlight, point: touchPoint, tag: tagCount, fieldType: selectedType)
            tagCount += 1
            self.delegate.isNextBtnHidden(value: true)
            let tempView = CommentView(frame: CGRect(x: self.imgView.frame.origin.x, y: self.imgView.frame.origin.y, width: self.imgView.frame.width, height: self.imgView.frame.height+self.bottomView.frame.height))
            tempView.bindData(text: selectedModel.comment, actionType: .add, model: self.model)
            
            tempView.deleteCallBack = { [weak self] in
                tempView.removeFromSuperview()
                let index = self?.arrOfHighlightModel.firstIndex{$0.tag == viewHighlight.tag}
                if let indxdat = index {
                    self?.arrOfHighlightModel.remove(at: indxdat)
                    self?.validateNextButton()
                }
                self?.delegate?.validateCrossButton(value: false)
                viewHighlight.removeFromSuperview()
            }
            tempView.doneCallback = { [weak self] text in
                tempView.removeFromSuperview()
                let index = self?.arrOfHighlightModel.firstIndex{$0.tag == viewHighlight.tag}
                if let indxdat = index {
                    selectedModel.comment = text
                    self?.arrOfHighlightModel[indxdat] = selectedModel
                    self?.validateNextButton()
                    self?.delegate?.validateCrossButton(value: false)
                }
            }
            
            viewHighlight.animateClickOnImage(completion: { [weak self] in
                self?.delegate?.validateCrossButton(value: true)
                self?.addSubview(tempView)
            })
            arrOfHighlightModel.append(selectedModel)
            collectionOfViews.addSubview(viewHighlight)
        }
    }
    
    fileprivate func removeMarkupView() {
        self.markupView.removeFromSuperview()
        self.markupView = nil
    }
    @IBAction func likeButtonAction(_ sender: Any) {
        guard markupView == nil else {
            self.removeMarkupView()
            return
        }
        markupvalidate(fieldType: .like)
    }
    
    @IBAction func dislikeButtonAction(_ sender: Any) {
        guard markupView == nil else {
            self.removeMarkupView()
            return
        }
        markupvalidate(fieldType: .dislike)
    }
    
    @IBAction func quesButtonAction(_ sender: Any) {
        guard markupView == nil else {
            self.removeMarkupView()
            return
        }
        markupvalidate(fieldType: .question)
    }
        fileprivate func validateNextButton() {
            if self.arrOfHighlightModel.count == 0 {
                self.delegate.isNextBtnHidden(value: true)
            } else {
                self.delegate.isNextBtnHidden(value: false)
            }
        }
    func markupvalidate(fieldType: HighlightType) {
        switch fieldType {
        case .like:
            selectedType = .like
            buttonsValidate(selected: true, button: likeButton, selectedType: .like)
            buttonsValidate(selected: false, button: disLikeButton, selectedType: .dislike)
            buttonsValidate(selected: false, button: quesButton, selectedType: .question)
        case .dislike :
            selectedType = .dislike
            buttonsValidate(selected: false, button: likeButton, selectedType: .like)
            buttonsValidate(selected: true, button: disLikeButton, selectedType: .dislike)
            buttonsValidate(selected: false, button: quesButton, selectedType: .question)
        case .question:
            selectedType = .question
            buttonsValidate(selected: false, button: likeButton, selectedType: .like)
            buttonsValidate(selected: false, button: disLikeButton, selectedType: .dislike)
            buttonsValidate(selected: true, button: quesButton, selectedType: .question)
        }
    }
    
    func buttonsValidate(selected: Bool, button: UIButton, selectedType: HighlightType) {
        switch selectedType {
        case .like:
            button.tintColor = selected ? likeColor: .gray
            button.layer.borderWidth = selected ? 2.0: 0.0
            button.layer.borderColor = selected ? likeColor.cgColor: UIColor.clear.cgColor
        case .dislike :
            button.tintColor = selected ? dislikeColor: .gray
            button.layer.borderWidth = selected ? 2.0: 0.0
            button.layer.borderColor = selected ? dislikeColor.cgColor: UIColor.clear.cgColor
        case .question:
            button.tintColor = selected ? quesColor: .gray
            button.layer.borderWidth = selected ? 2.0: 0.0
            button.layer.borderColor = selected ? quesColor.cgColor: UIColor.clear.cgColor
        }
        button.layer.cornerRadius = button.frame.size.height/2
        button.layer.masksToBounds = true
    }
}
