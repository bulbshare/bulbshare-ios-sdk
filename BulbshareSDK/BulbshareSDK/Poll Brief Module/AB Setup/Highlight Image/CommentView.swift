import UIKit
class CommentView: UIView {
    
    // MARK: - Local properties
    fileprivate var model: PollBriefResponse!
    private var setView: UIView!
    
    // MARK: - IBOutlets
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var textView: CustomTextView!
    @IBOutlet weak var titleLabel: UILabel!
    var deleteCallBack: (() -> Void)?
    var doneCallback: ((String) -> Void)?
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    func bindData(text: String, actionType: CommentType, model: PollBriefResponse) {
        self.deleteButton.setTitle(actionType == .add ? "Cancel": "Delete", for: .normal)
        self.textView.text = text
        self.textView.placeholder = model.hint ?? ""
        self.titleLabel.text = model.title
        updateUI()
    }
    func bindDataImageGrid(actionType: CommentType,model: PollBriefResponse,index: Int){
        self.deleteButton.setTitle(actionType == .add ? "Cancel": "Delete", for: .normal)
        self.backgroundImage.sd_setImage(with: URL(string: model.collection![index].image!), placeholderImage: nil)
        self.titleLabel.text = model.commentTitle
        self.textView.placeholder = model.commentDescription ?? ""
        updateUI()
    }
    private func updateUI(){
        self.textView.tintColor = .white
        self.textView.delegate = self
        self.deleteButton.layer.cornerRadius = self.deleteButton.frame.height/2
        self.deleteButton.layer.masksToBounds = true
        self.doneButton.layer.cornerRadius = self.doneButton.frame.height/2
        self.doneButton.layer.masksToBounds = true
    }
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        deleteCallBack?()
        self.textView.resignFirstResponder()
        
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        doneCallback?(self.textView.text)
        self.textView.resignFirstResponder()
        
    }
}
extension CommentView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
       return textView.text.count + (text.count - range.length) <= 200
   }
}
