import UIKit
class MarkupView: UIView {
    
    // MARK: - Local properties
    fileprivate var model: PollBriefResponse!
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    fileprivate var likeColor = UIColor.init(hexString: ColorConstants.greenColor, alpha: 1.0)
    fileprivate var dislikeColor = UIColor.init(hexString: ColorConstants.redColor, alpha: 1.0)
    fileprivate var quesColor = UIColor.init(hexString: ColorConstants.orangeColor, alpha: 1.0)
    
    // MARK: - IBOutlet
    @IBOutlet weak var imgView: UIImageView!
    
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    func binddata(type: HighlightType) {
        switch type {
        case .like:
            self.imgView.image = UIImage(named: "ic_thumb_up")
            imgView.backgroundColor = likeColor
        case .dislike :
            self.imgView.image = UIImage(named: "ic_thumb_down")
            imgView.backgroundColor = dislikeColor
        case .question:
            self.imgView.image = UIImage(named: "ic_question_mark")
            imgView.backgroundColor = quesColor
        }
        imgView.image = imgView.image?.withRenderingMode(.alwaysTemplate)
        imgView.tintColor = .white
        imgView.layer.borderWidth =  2.0
        imgView.layer.borderColor =  UIColor.white.cgColor
        self.imgView.layer.cornerRadius = self.imgView.frame.size.height/2
        self.imgView.layer.masksToBounds = true
        
}
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }
}
