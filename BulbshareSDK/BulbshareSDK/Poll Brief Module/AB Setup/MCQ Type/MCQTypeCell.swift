import UIKit

class MCQTypeCell: PollBaseCollectionCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var pollTypeThreeInfoView: MCQTypeView!

    // MARK: - Helper methods
    
    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        pollTypeThreeInfoView.bind(model: model, delegate: delegate)
    }
    
}
