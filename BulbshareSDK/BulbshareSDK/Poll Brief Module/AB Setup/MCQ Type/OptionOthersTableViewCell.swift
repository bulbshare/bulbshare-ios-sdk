import UIKit
protocol otherCellProtocol: AnyObject{
    func updateHeightOfRow(cell: OptionOthersTableViewCell,textView: UITextView)
    
}
class OptionOthersTableViewCell: PollBaseTableCell {
    // MARK: - IBOutlets
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var otherLbl: UILabel!
    @IBOutlet weak var optionsTextView: CustomTextView!
    @IBOutlet weak var selectionImage: UIImageView!
    // MARK: - LocalVariables
    weak var cellDelegate: otherCellProtocol?
    var selectCount: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        optionsTextView.delegate = self
        optionsTextView.isUserInteractionEnabled = false
        optionsTextView.placeholder = "✏️"
        borderView.layer.cornerRadius = 10
        borderView.layer.borderColor = UIColor.white.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            borderView.layer.borderWidth = 3
            optionsTextView.isUserInteractionEnabled = true
            optionsTextView.becomeFirstResponder()
            if selectCount == 1{
                selectionImage.image = UIImage(named: "ic_single_selection_selected")
            } else if selectCount! > 1{
                selectionImage.image = UIImage(named: "ic_multi_selection_selected")
            }
            
        } else {
            borderView.layer.borderWidth = 1
            optionsTextView.resignFirstResponder()
            optionsTextView.isUserInteractionEnabled = false
            optionsTextView.text = ""
            if let deletate = cellDelegate {
                deletate.updateHeightOfRow(cell: self, textView: optionsTextView)
            }
            if selectCount == 1{
                selectionImage.image = UIImage(named: "ic_single_selection_deselected")
            } else if selectCount! > 1{
                selectionImage.image = UIImage(named: "ic_multi_selection_deselected")
            }
            
        }
    }
    
    override func setDelegate(delegate: otherCellProtocol) {
        self.cellDelegate = delegate
    }
    override func bind(model: Collection,selectCount: Int) {
        self.selectCount = selectCount
        self.otherLbl.text = model.text
    }
    
}
extension OptionOthersTableViewCell: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        if let deletate = cellDelegate {
            deletate.updateHeightOfRow(cell: self, textView: optionsTextView)
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return textView.text.count + (text.count - range.length) <= 200
    }    
}
