import UIKit
class MCQTypePresenter {
    var model: PollBriefResponse?
    weak var delegate: PollBriefDelegate!
    var indexSelected =  [String]()
    var optionsData: [Collection]!
    var selectCount: Int
    var noneIndexPath = IndexPath()
    var isNoneSelected = false
    var array = [IndexPath]()
    var pollTypeView: MCQTypeView!
    
    init(model: PollBriefResponse?, delegate: PollBriefDelegate!) {
        self.model = model
        self.delegate = delegate
        self.optionsData = (model?.collection)!
        self.selectCount = (model?.selectCount)!
    }
    
    func singleOptionSelected(tableView: UITableView,indexPath: IndexPath){
        self.indexSelected.removeAll()
        self.indexSelected.append(self.optionsData[self.pollTypeView.optionsTableView.indexPathForSelectedRow!.row].value!)
        self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
    }
    func multipleOptionsSelected(tableView: UITableView,indexPath: IndexPath){
        if self.selectCount >= 1 && isNoneSelected == false{
            self.indexSelected.removeAll()
            self.array.removeAll()
            if let arr = tableView.indexPathsForSelectedRows{
                for index in arr {
                    indexSelected.append(self.optionsData[index.row].value!)
                    self.array.append(index)
                }
            }
            if indexSelected.contains(Polls.none){
                for index in array{
                    if optionsData[index.row].value == "none"{
                        noneIndexPath = index
                    }
                    tableView.deselectRow(at: index, animated: true)
                }
                array.removeAll()
                indexSelected.removeAll()
                array.append(noneIndexPath)
                indexSelected.append(optionsData[noneIndexPath.row].value!)
                tableView.selectRow(at: noneIndexPath, animated: true, scrollPosition: .none)
                isNoneSelected = true
            } else{
                isNoneSelected = false
            }
        } else{
            tableView.deselectRow(at: noneIndexPath, animated: true)
            array.removeAll()
            indexSelected.removeAll()
            isNoneSelected = false
        }
        nextBtnConditions(isNoneSelected: self.isNoneSelected)
    }
    func nextBtnConditions(isNoneSelected: Bool){
        if isNoneSelected == false{
            if self.model?.kind == Polls.atleast && self.indexSelected.count >= self.selectCount{
                self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
            } else if self.model?.kind == Polls.exactly && self.indexSelected.count == self.selectCount{
                self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
            } else if self.model?.kind == Polls.atmost && self.indexSelected.count <= self.selectCount && self.indexSelected.count != 0{
                self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
            } else{
                self.delegate.isModelValidated(value: false, model: nil, characterValidation: false)
            }
        } else{
            self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
        }
    }
    
}
