import UIKit
class MCQTypeView: UIView {
    
    // MARK: - IBoutlets
    @IBOutlet weak var questionsLabel: UILabel!
    @IBOutlet weak var optionsTableView: UITableView!
    // MARK: - Local Variables
    fileprivate weak var delegate: PollBriefDelegate!
    var pollBriefMCQViewModel: [PollBriefMCQViewModel]!
    private var setView: UIView!
    var presenter: MCQTypePresenter!
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
        
    }
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Helper methods
    func bind(model: PollBriefResponse?, delegate: PollBriefDelegate) {
        if let introModel = model {
            self.setupUI()
            self.presenter = MCQTypePresenter(model: introModel, delegate: delegate)
            self.delegate = delegate
            self.questionsLabel.text = introModel.title
            self.optionsTableView.reloadData()
            self.delegate.isModelValidated(value: false, model: nil, characterValidation: false)
            self.pollBriefMCQViewModel = PollBriefMapper.mapMCQ(model: (model?.collection)!)
        }
        
    }
    func setupUI(){
        optionsTableView.delegate = self
        optionsTableView.dataSource = self
        optionsTableView.allowsMultipleSelectionDuringEditing = true
        optionsTableView.allowsMultipleSelection = true
        self.optionsTableView.register(UINib(nibName: Polls.OptionsTableViewCell, bundle: SDKBundles.sdkBundle), forCellReuseIdentifier: Polls.OptionsTableViewCell)
        self.optionsTableView.register(UINib(nibName: Polls.OptionOthersTableViewCell, bundle: SDKBundles.sdkBundle), forCellReuseIdentifier: Polls.OptionOthersTableViewCell)
    }
    
}
extension MCQTypeView: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.presenter.optionsData != nil else {
            return 0
        }
        return self.presenter.optionsData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell: PollBaseTableCell = tableView.dequeueReusableCell(withIdentifier: String(describing: self.pollBriefMCQViewModel[indexPath.row].cellType), for: indexPath) as? PollBaseTableCell
        else {
            return UITableViewCell()
        }
        cell.bind(model: pollBriefMCQViewModel[indexPath.row].model!, selectCount: pollBriefMCQViewModel.count)
        cell.setDelegate(delegate: self)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.multipleOptionsSelected(tableView: tableView, indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.presenter.multipleOptionsSelected(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension MCQTypeView: otherCellProtocol{
    func updateHeightOfRow(cell: OptionOthersTableViewCell, textView: UITextView) {
        let size = textView.bounds.size
        let newSize = optionsTableView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        if size.height != newSize.height{
            UIView.setAnimationsEnabled(false)
            optionsTableView?.beginUpdates()
            optionsTableView?.endUpdates()
            UIView.setAnimationsEnabled(true)
            if let thisIndexPath = optionsTableView.indexPath(for: cell){
                optionsTableView.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
        }
    }
}
