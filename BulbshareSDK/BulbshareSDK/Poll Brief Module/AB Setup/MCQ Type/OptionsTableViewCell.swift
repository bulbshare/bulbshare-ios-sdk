import UIKit

class OptionsTableViewCell: PollBaseTableCell {
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var optionsText: UILabel!
    @IBOutlet weak var selectionImage: UIImageView!
    var selectCount: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        borderView.layer.cornerRadius = 10
        borderView.layer.borderColor = UIColor.white.cgColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        borderView.layer.borderWidth = selected ? 3 : 1
        if self.selectCount == 1{
            self.selectionImage.image = selected ? UIImage(named: "ic_single_selection_selected") : UIImage(named: "ic_single_selection_deselected")
        } else if self.selectCount! > 1{
            self.selectionImage.image = selected ? UIImage(named: "ic_multi_selection_selected") : UIImage(named: "ic_multi_selection_deselected")
        }
        
    }
    
    override func bind(model: Collection,selectCount: Int) {
        self.optionsText.text = model.text
        self.selectCount = selectCount
    }
    
}
