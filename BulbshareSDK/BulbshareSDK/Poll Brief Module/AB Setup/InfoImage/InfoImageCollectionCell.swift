import UIKit

class InfoImageCollectionCell: PollBaseCollectionCell {

     // MARK: - IBOutlets
    @IBOutlet weak var InfoImageView: InfoImageView!
    
    // MARK: - Helper methods
    
    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        InfoImageView.bind(model: model, delegate: delegate)
    }
}
