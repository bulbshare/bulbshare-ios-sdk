import UIKit
class InfoImageView: UIView {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var overlayText: UILabel!
    @IBOutlet weak var linkText: UILabel!
    @IBOutlet weak var linkView: UIView!
    
    // MARK: - Local properties
    fileprivate var model: PollBriefResponse!
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    var collectionIndex: Int = 0
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
        self.linkView.layer.cornerRadius = self.linkView.frame.height/2
        self.linkView.layer.borderWidth = 1
        self.linkView.layer.borderColor = UIColor.systemBlue.cgColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(changeView))
        self.addGestureRecognizer(tap)
    }
    
    @objc func changeView(){
        collectionIndex += 1
        if let collectionCount = self.model.collection?.count{
            if collectionIndex < collectionCount{
                if model.collection![collectionIndex].link == "" {
                    self.linkView.isHidden = true
                } else {
                    self.linkView.isHidden = false
                }
                self.image.sd_setImage(with: URL(string: self.model.collection![collectionIndex].image!), placeholderImage: nil)
                self.overlayText.text = model.collection![collectionIndex].overlayText
                self.linkText.text = model?.collection![collectionIndex].linkText
            } else if collectionIndex == collectionCount {
                self.collectionIndex = 0
                self.delegate.enableNext(value: true)
            }
        }
    }
    
    @IBAction func linkButtonTapped() {
        let chromeURL = model.collection![0].link
        UIApplication.shared.open(URL(string: chromeURL!)!)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Helper methods
    func bind(model: PollBriefResponse?, delegate: PollBriefDelegate) {
        if let introModel = model {
            self.delegate = delegate
            self.model = introModel
            self.delegate.isBottomViewHidden(value: true)
            self.delegate.isModelValidated(value: false, model: model, characterValidation: false)
            self.image.sd_setShowActivityIndicatorView(true)
            self.image.sd_setIndicatorStyle(.gray)
            self.image.sd_setImage(with: URL(string: self.model.collection![0].image!), placeholderImage: nil)
            self.overlayText.text = model?.collection![0].overlayText
            self.linkText.text = model?.collection![0].linkText
            DispatchQueue.main.async {
                if model?.collection![0].link == "" {
                    self.linkView.isHidden = true
                } else {
                    self.linkView.isHidden = false
                }
            }
        }
    }
}
