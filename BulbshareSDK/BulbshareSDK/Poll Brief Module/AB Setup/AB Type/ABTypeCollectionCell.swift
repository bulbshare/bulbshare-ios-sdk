import UIKit

class ABTypeCollectionCell: PollBaseCollectionCell {

    // MARK: - IBOutlets
    @IBOutlet weak var abTypeInfoView: ABTypeView!

    // MARK: - Helper methods
    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        abTypeInfoView.bind(model: model, delegate: delegate)
    }
}
