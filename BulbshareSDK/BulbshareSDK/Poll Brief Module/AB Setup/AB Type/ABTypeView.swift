import UIKit
class ABTypeView: UIView {

    // MARK: - Local properties
    fileprivate var model: PollBriefResponse!
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    fileprivate var imageColectionData: [Item]!
    fileprivate var itemCount: Int = 2
    
    // MARK: - IBOutlet
    @IBOutlet weak var imgCollectionView: UICollectionView!
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
        self.imgCollectionView.register(UINib(nibName: String(describing: ImageCollectionCell.self), bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: String(describing: ImageCollectionCell.self))
        self.imgCollectionView.delegate = self
        self.imgCollectionView.dataSource = self
    }

    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }

    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    // MARK: - Helper methods
    func bind(model: PollBriefResponse?, delegate: PollBriefDelegate) {
        if let introModel = model {
            self.delegate = delegate
            self.model = introModel
            if let imagedata = introModel.collection?[0].items {
                self.imageColectionData = imagedata
                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                layout.minimumInteritemSpacing = 0
                layout.minimumLineSpacing = 0
                imgCollectionView.collectionViewLayout = layout
                self.imgCollectionView.reloadData()
            }
        }
    }
}

// MARK: - Collection view datasource and delegate
extension ABTypeView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.imageColectionData != nil else {
            return 0
        }
        return self.imageColectionData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            guard let cell: ImageCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ImageCollectionCell.self), for: indexPath) as? ImageCollectionCell else {
            return UICollectionViewCell()
        }
        cell.bind(model: self.imageColectionData[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = self.frame.size.width
        let height = self.frame.size.height
        if self.imageColectionData.count == itemCount {
            return CGSize(width: width, height: height/CGFloat(itemCount))
        }
        return CGSize(width: width/CGFloat(itemCount), height: height/CGFloat(itemCount))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.animateClickOnImage {
                self.delegate?.enableNext(value: true)
            }
        }
    }
}
