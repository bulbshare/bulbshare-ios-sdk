import UIKit
@_implementationOnly import SDWebImage
import Foundation

class ImageCollectionCell: UICollectionViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelOverlayText: UILabel!

    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Helper methods
    func bind(model: Item) {
        DispatchQueue.main.async {
            self.imageView.sd_setShowActivityIndicatorView(true)
            self.imageView.sd_setIndicatorStyle(.white)
            self.imageView.sd_setImage(with: URL(string: model.image), placeholderImage: nil)
        }
        labelOverlayText.text = model.overlayText
    }
}
