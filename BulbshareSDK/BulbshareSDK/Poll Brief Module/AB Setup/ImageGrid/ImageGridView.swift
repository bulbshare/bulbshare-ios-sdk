import UIKit
class ImageGridView: UIView {
    
    // MARK: - IBoutlets
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    // MARK: - Local Variables
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    fileprivate var OptionsColectionData: [Collection]!
    var presenter: ImageGridPresenter!
    var zoomInProgress = false
    var selectionCell: ImageGridCollectionViewCell!
    var selectedImage: UIImageView!
    var selectionPoint: CGPoint!
    var itemCount: Int!
    var characterMinLen = 0
    var characterMaxLen = 5000
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Helper methods
    func bind(model: PollBriefResponse?, delegate: PollBriefDelegate) {
        if let introModel = model{
            self.setupUI()
            self.presenter = ImageGridPresenter(model: introModel, delegate: delegate)
            self.delegate = delegate
            self.questionLabel.text = introModel.title
            self.delegate.isModelValidated(value: false, model: nil, characterValidation: false)
            if let optionsData = introModel.collection{
                self.OptionsColectionData = optionsData
                self.itemCount = optionsData.count
                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                optionsCollectionView.collectionViewLayout = layout
                self.optionsCollectionView.reloadData()
            }
        }
    }
    private func setupUI(){
        self.optionsCollectionView.delegate = self
        self.optionsCollectionView.dataSource = self
        self.optionsCollectionView.allowsMultipleSelection = true
        self.optionsCollectionView.register(UINib(nibName: "ImageGridCollectionViewCell", bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: "ImageGridCollectionViewCell")
    }
    @objc func handleLongPress(_ sender: UILongPressGestureRecognizer?) {
        if sender?.state == .began && zoomInProgress == false {
            let point = sender?.location(in: self.optionsCollectionView)
            guard let indexPath = self.optionsCollectionView.indexPathForItem(at: point!) else { return }
            selectionCell = optionsCollectionView.cellForItem(at: indexPath) as? ImageGridCollectionViewCell
            selectionPoint = optionsCollectionView.convert(selectionCell.center, to: self)
            self.zoomInAnimationWithCell()
        } else if sender?.state == .ended && zoomInProgress == true{
            self.zoomOutAnimationWithCell()
        }
    }
    func zoomInAnimationWithCell() {
        zoomInProgress = true
        let imageView = selectionCell.optionsImageView
        selectedImage = UIImageView(frame: imageView?.bounds ?? CGRect.zero)
        selectedImage.center = selectionPoint
        selectedImage.image = imageView?.image
        selectedImage.contentMode = .scaleAspectFill
        selectedImage.clipsToBounds = true
        selectedImage.layer.cornerRadius = 3.0
        selectedImage.layer.borderColor = UIColor.clear.cgColor
        selectedImage.layer.borderWidth = 4.0
        self.addSubview(selectedImage)
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            self?.selectionCell.contentView.isHidden = true
            self?.selectedImage.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
            self?.selectedImage.center = self!.center
        })
    }
    func zoomOutAnimationWithCell() {
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            self?.selectedImage?.transform = .identity
            self?.selectedImage?.center = (self?.selectionPoint)!
        }) { [weak self] _ in
            self?.selectionPoint = CGPoint(x: 0, y: 0)
            DispatchQueue.main.async {
                self?.selectedImage.removeFromSuperview()
                self?.selectionCell.contentView.isHidden = false
                self?.zoomInProgress = false
            }
        }
    }
}
extension ImageGridView: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.OptionsColectionData != nil else {
            return 0
        }
        return self.OptionsColectionData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: ImageGridCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGridCollectionViewCell", for: indexPath) as? ImageGridCollectionViewCell else {
            return UICollectionViewCell()
        }
        self.OptionsColectionData[indexPath.row].comment = ""
        var zoomLPGRecognizer = UILongPressGestureRecognizer()
        zoomLPGRecognizer = UILongPressGestureRecognizer(target: self,action: #selector(handleLongPress(_:)))
        zoomLPGRecognizer.minimumPressDuration = 0.5
        zoomLPGRecognizer.allowableMovement = 1
        cell.addGestureRecognizer(zoomLPGRecognizer)
        cell.bind(model: self.OptionsColectionData[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = self.frame.size.width-70
        return CGSize(width: width/CGFloat(3), height: (width/CGFloat(3)*1.3))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter.multipleOptionsSelected(collectionView: collectionView, indexPath: indexPath)
        selectedChange(indexPath: indexPath)
        self.presenter.nextBtnConditions()
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        self.presenter.multipleOptionsSelected(collectionView: collectionView, indexPath: indexPath)
        selectedChange(indexPath: indexPath)
        self.presenter.nextBtnConditions()
    }
    func loadCommentView(indexpath: IndexPath,modeToOpen: CommentType){
        let tempView = CommentView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: self.frame.height))
        tempView.bindDataImageGrid(actionType: modeToOpen, model: self.presenter.model!, index: indexpath.row)
        tempView.textView.becomeFirstResponder()
        self.delegate.isNextBtnHidden(value: true)
        self.delegate.validateCrossButton(value: true)
        if modeToOpen == .edit{
            tempView.textView.text = self.OptionsColectionData[indexpath.row].comment
        }
        tempView.deleteCallBack = { [weak self] in
            tempView.removeFromSuperview()
            self?.OptionsColectionData[indexpath.row].comment = ""
            self?.optionsCollectionView.deselectItem(at: indexpath, animated: true)
            self?.delegate.isNextBtnHidden(value: false)
            self?.delegate.validateCrossButton(value: false)
            if modeToOpen == .add {
                let optionToBeRemoved = self?.OptionsColectionData[indexpath.row].optionid
                let indexToBeRemoved = self?.presenter.indexSelected.firstIndex(of: optionToBeRemoved!)
                self?.presenter.indexSelected.remove(at: indexToBeRemoved!)
            }
            self?.presenter.nextBtnConditions()
        }
        tempView.doneCallback = { [weak self] commentText in
            if commentText.count == self?.characterMinLen {
                self?.delegate.showSingleAlertmessage(message: PollBrief.alertCommentToShort, actionTitle: PollBrief.ok)
            } else if commentText.count > self!.characterMaxLen {
                self?.delegate.showSingleAlertmessage(message: PollBrief.alertCommentToLong, actionTitle: PollBrief.ok)
            } else {
                self?.OptionsColectionData[indexpath.row].comment = commentText
                tempView.removeFromSuperview()
                self?.delegate.isNextBtnHidden(value: false)
                self?.delegate.validateCrossButton(value: false)
                if modeToOpen == .edit {
                    self?.optionsCollectionView.selectItem(at: indexpath, animated: true, scrollPosition: .bottom)
                    self?.presenter.indexSelected.append(self!.OptionsColectionData[indexpath.row].optionid)
                    self?.presenter.nextBtnConditions()
                }
                self?.presenter.nextBtnConditions()
            }
        }
        self.addSubview(tempView)
    }
    func selectedChange(indexPath: IndexPath){
        if self.presenter.isCommentMandatory == true{
            if self.OptionsColectionData[indexPath.row].comment! == "" {
                self.loadCommentView(indexpath: indexPath, modeToOpen: .add)
            } else {
                self.loadCommentView(indexpath: indexPath, modeToOpen: .edit)
            }
        }
    }
}
