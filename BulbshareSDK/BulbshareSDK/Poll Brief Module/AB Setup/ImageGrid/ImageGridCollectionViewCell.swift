import UIKit
import SDWebImage

class ImageGridCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var optionsImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        borderView.layer.cornerRadius = 3
        optionsImageView.layer.cornerRadius = 3
        borderView.layer.borderWidth = 4
        borderView.layer.borderColor = UIColor.clear.cgColor
        selectedImageView.isHidden = true
    }
    override var isSelected: Bool{
        didSet{
            if isSelected{
                borderView.animateBorderColor(toColor: .white, duration: 0.2)
                selectedImageView.fadeIn()
                selectedImageView.isHidden = false
            } else {
                borderView.animateBorderColor(toColor: .clear, duration: 0.2)
                selectedImageView.fadeOut()
            }
        }
    }
    func bind(model: Collection) {
        DispatchQueue.main.async {
            self.optionsImageView.sd_setShowActivityIndicatorView(true)
            self.optionsImageView.sd_setIndicatorStyle(.white)
            self.optionsImageView.sd_setImage(with: URL(string: model.image!), placeholderImage: nil)
        }
    }


}
