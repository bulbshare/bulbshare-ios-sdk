import UIKit

class ImageGridCollectionCell: PollBaseCollectionCell {
    // MARK: - IBOutlets
    @IBOutlet weak var ImageGridInfoView: ImageGridView!
    // MARK: - Helper methods
    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        ImageGridInfoView.bind(model: model, delegate: delegate)
    }

}
