import Foundation
import UIKit
class ImageGridPresenter{
    var model: PollBriefResponse?
    weak var delegate: PollBriefDelegate!
    var indexSelected = [Int]()
    var isCommentMandatory:Bool!
    var optionsData: [Collection]!
    var selectCount: Int!
    
    init(model: PollBriefResponse?, delegate: PollBriefDelegate!){
        self.model = model
        self.delegate = delegate
        self.optionsData = (model?.collection)!
        self.selectCount = (model?.selectCount)!
        self.isCommentMandatory = (model?.commentMandatory)!
    }
    func multipleOptionsSelected(collectionView: UICollectionView,indexPath: IndexPath){
        self.indexSelected.removeAll()
        if let arr = collectionView.indexPathsForSelectedItems{
            for index in arr{
                indexSelected.append(self.optionsData[index.row].optionid)
            }
        }
    }
    func nextBtnConditions(){
        if self.model?.kind == Polls.exactly && self.indexSelected.count == self.selectCount{
            self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
        } else if self.model?.kind == Polls.atleast && self.indexSelected.count >= self.selectCount{
            self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
        } else if self.model?.kind == Polls.atmost && self.indexSelected.count <= self.selectCount && self.indexSelected.count != 0{
            self.delegate.isModelValidated(value: true, model: nil, characterValidation: false)
        } else{
            self.delegate.isModelValidated(value: false, model: nil, characterValidation: false)
        }
    }
}
