import UIKit
import AVFoundation
import CoreData

class MediaUploadView: UIView, AVAudioRecorderDelegate, AVAudioPlayerDelegate{
    
    // MARK: - Local properties
    fileprivate weak var delegate: PollBriefDelegate!
    private var setView: UIView!
    fileprivate var presenter: PollBriefPresenter!
    fileprivate var mediaType: MediaType!
    fileprivate var model: PollBriefResponse!
    fileprivate var tapGestureRecognizer: UITapGestureRecognizer!
    
    // MARK: - Upload Audio
    var fileName = "audioFile.m4a"
    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?
    var recordingSession: AVAudioSession!
    var recorderTimer: Timer!
    var totalRecorderTime = (0,0)
    
    // MARK: - IBOutlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var cameraVideoView: UIView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var recorderView: UIView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var stopbutton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var timerButton: UIButton!
    
    // MARK: - Life cycle methods
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - Initial setup methods
    /**
     Set up the XIB
     */
    private func xibSetup() {
        backgroundColor = .clear
        setView = loadViewFromNib()
        setView.frame = bounds
        setView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(setView)
    }
    
    /**
     This method load the view from XIB
     - returns: UIView
     */
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view!
    }
    
    override func layoutSubviews() {
        setView.frame = bounds
        setView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Helper methods
    func bind(modelDetails: PollBriefResponse?, delegate: PollBriefDelegate) {
        
        presenter = PollBriefPresenter(delegate: delegate)
        if let introModel = modelDetails, let image = introModel.image_url {
            self.model = introModel
            setupUI()
            NotificationCenter.default.addObserver(self, selector: #selector(self.showImage(_:)), name: NSNotification.Name(rawValue: "imagePicker"), object: nil)
            self.delegate = delegate
            crossButton.isHidden = true
            self.imageView.sd_setImage(with: URL(string: image), placeholderImage: nil)
            titleLabel.text = introModel.title
            descLabel.text = ""
            guard let values = self.presenter.validateMediaType(model: introModel) else {
                return
            }
            self.cameraButton.setTitle(values.cameraText, for: .normal)
            self.galleryButton.setTitle(values.galleryText, for: .normal)
            self.cameraButton.setImage(UIImage(named: values.mediaType == .video ? "ic_media_video": "ic_media_photo"), for: .normal)
            self.mediaType = values.mediaType
            if mediaType == .audio{
                self.startAudioSession()
                //checkMicrophoneAccess()
                cameraVideoView.isHidden = true
                playerView.isHidden = true
                recorderView.isHidden = false
                self.validateBottomButton(isShow: true)
                self.crossButton.isHidden = true
            } else if mediaType == .video || mediaType == .photo {
                recorderView.isHidden = true
                playerView.isHidden = true
                cameraVideoView.isHidden = false
                self.validateBottomButton(isShow: false)
            }
        }
    }
    
    func setupUI() {
        cameraButton.layer.cornerRadius = self.cameraButton.frame.height/2
        cameraButton.layer.masksToBounds = true
        galleryButton.layer.cornerRadius = self.galleryButton.frame.height/2
        galleryButton.layer.masksToBounds = true
        recordButton.layer.cornerRadius = self.recordButton.frame.height/2
        playPauseButton.layer.cornerRadius = self.playPauseButton.frame.height/2
        stopbutton.layer.cornerRadius = self.stopbutton.frame.height/2
        deleteButton.layer.cornerRadius = self.deleteButton.frame.height/2
        self.imageView.layer.cornerRadius = 10.0
        self.imageView.layer.masksToBounds = true
        self.imageView.sd_setShowActivityIndicatorView(true)
        self.imageView.sd_setIndicatorStyle(.white)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(mediaTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func mediaTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.delegate.showMedia(mediatype: self.mediaType)
    }
    func validateBottomButton(isShow: Bool) {
        self.cameraButton.isHidden = isShow
        self.galleryButton.isHidden = isShow
        crossButton.isHidden = !isShow
        self.cameraVideoView.isHidden = isShow
        tapGestureRecognizer.isEnabled = isShow
    }
    
    // MARK: Button Action
    @IBAction func galleryAction(_ sender: Any) {
        switch self.mediaType {
        case .photo:
            self.delegate.openImage(usingCamera: false, model: model)
        case .video:
            self.delegate.openVideoCamera(usingCamera: false, model: model)
        case .audio:
            break
        case .none:
            break
        case .some(.image):
            break
        }
    }
    
    @IBAction func cameraAction(_ sender: Any) {
        switch self.mediaType {
        case .photo:
            self.delegate.openImage(usingCamera: true, model: model)
        case .video:
            self.delegate.openVideoCamera(usingCamera: true, model: model)
        case .audio:
            break
        case .some(.image):
            break
        case .none:
            break
        }
    }
    /**
     Set audio session
     */
    func startAudioSession(){
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSession.Category.playAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.setupRecorder()
                    }
                }
            }
        } catch let error {
            Logger.debug(arg: "recording failed: \(error)")
        }
    }
    /**
     Setup up recorder method
     */
    func setupRecorder() {
        // Define the recorder setting
        let recorderSetting = [AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC as UInt32),
                               AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue,
                               AVSampleRateKey: 44100.0,
                               AVNumberOfChannelsKey: 2 ] as [String: Any]
        
        audioRecorder = try? AVAudioRecorder(url: getFileURL() as URL, settings: recorderSetting)
        audioRecorder?.delegate = self
        audioRecorder?.isMeteringEnabled = true
        audioRecorder?.prepareToRecord()
    }
    /**
     get directory method
     */
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    /**
     get file path method
     */
    func getFileURL() -> NSURL {
        let audioFilename = getDocumentsDirectory().appendingPathComponent(fileName)
        return audioFilename as NSURL
    }
    
    /**
     IBAction for record audio button
     */
    @IBAction func recordButtonAction(_ sender: UIButton) {
        if let recorder = audioRecorder {
            if !recorder.isRecording {
                let audioSession = AVAudioSession.sharedInstance()
                do {
                    try audioSession.setActive(true)
                } catch _ {
                }
                audioRecorder?.record()
                recordButton.setTitle("Recording   \(String(format: PollBrief.stringConstant, 0,0) as String)", for: .normal)
                recorderTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateRecorderTime), userInfo: nil, repeats: true)
                recordButton.setImage(UIImage(named: "ic_media_recording"), for: .normal)
            } else {
                recorderTimer.invalidate()
                if recorder.currentTime < 5.0 {
                    self.delegate.showAlertWithCompletion(message: Alert.RecorderMinimumTime, actionTitle: Alert.TryAgain) {
                        do {
                            try FileManager.default.removeItem(at: self.getFileURL() as URL)
                        } catch {
                        }
                        self.resetRecorder()
                    }
                } else {
                    totalRecorderTime = setTimer(time: Int(audioRecorder?.currentTime ?? 0))
                    resetRecorder()
                    recorderView.isHidden = true
                    playerView.isHidden = false
                    showPreviewScreen()
                    timerButton.setTitle("00:00 - \(String(format: PollBrief.stringConstant, totalRecorderTime.0,totalRecorderTime.1) as String)", for: .normal)
                    if !recorder.isRecording {
                        if !(audioPlayer?.isPlaying ?? false) {
                            audioPlayer = try? AVAudioPlayer(contentsOf: recorder.url)
                            audioPlayer?.delegate = self
                            audioPlayer?.prepareToPlay()
                        }
                    }
                }
            }
        }
    }
    
    /**
     Reset recorder
     */
    func resetRecorder() {
        audioRecorder?.stop()
        recordButton.setTitle(PollBrief.RecordAudio, for: .normal)
        self.recordButton.setImage(UIImage(named: "ic_media_audio"), for: .normal)
    }
    
    /**
     Set timer methods
     */
    @objc func updateRecorderTime() {
        if (audioRecorder?.isRecording) != nil {
            let time = setTimer(time: Int(audioRecorder?.currentTime ?? 0))
            recordButton.setTitle("Recording   \(String(format: PollBrief.stringConstant, time.0,time.1) as String)", for: .normal)
        }
    }
    
    func setTimer(time: Int) -> (Int,Int) {
        let currentTime = time
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
        return (minutes,seconds)
    }
    
    @objc func updatePlayerTime() {
        if (audioPlayer?.isPlaying) != nil {
            let time = setTimer(time: Int(audioPlayer?.currentTime ?? 0))
            timerButton.setTitle("\(String(format: PollBrief.stringConstant, time.0,time.1) as String) - \(String(format: PollBrief.stringConstant, totalRecorderTime.0,totalRecorderTime.1) as String)", for: .normal)
        }
    }
    
    /**
     IBAction for play/pause audio
     */
    @IBAction func playPauseButtonAction(_ sender: UIButton) {
        if !(audioPlayer?.isPlaying ?? false) {
            audioPlayer?.delegate = self
            audioPlayer?.play()
            recorderTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatePlayerTime), userInfo: nil, repeats: true)
            playPauseButton.setImage(UIImage(named: "ic_player_pause"), for: .normal)
            
        } else {
            audioPlayer?.pause()
            playPauseButton.setImage(UIImage(named: "ic_player_start"), for: .normal)
        }
    }
    /**
     IBAction for stop audio button
     */
    @IBAction func stopButtonAction(_ sender: UIButton) {
        if (audioPlayer?.isPlaying) != nil {
            audioPlayer?.stop()
            audioPlayer?.currentTime = 0.0
            audioPlayer?.delegate = nil
            playPauseButton.setImage(UIImage(named: "ic_player_start"), for: .normal)
        }
    }
    /**
     IBAction for delete audio button
     */
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        audioPlayer?.stop()
        self.playPauseButton.setImage(UIImage(named: "ic_player_start"), for: .normal)
        self.delegate.showAlert(withTitle: Alert.DeleteRecording, message: Alert.WantToDeleteRecording, acceptTitle: Alert.Delete, cancelTitle: Alert.Cancel) { [self] in
            do {
                try FileManager.default.removeItem(at: self.getFileURL() as URL)
            } catch {
            }
            self.recorderView.isHidden = false
            self.playerView.isHidden = true
            self.titleLabel.text = self.model.title
            self.descLabel.text = ""
            self.recordButton.setTitle(PollBrief.RecordAudio, for: .normal)
            self.recordButton.setImage(UIImage(named: "ic_media_audio"), for: .normal)
            self.playPauseButton.setImage(UIImage(named: "ic_player_start"), for: .normal)
        } cancelCompletionBlock: {
        }
    }
    /**
     Audio player delegate method
     */
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if (audioPlayer?.isPlaying) != nil {
            audioPlayer?.stop()
            audioPlayer?.currentTime = 0.0
            audioPlayer?.delegate = nil
            playPauseButton.setImage(UIImage(named: "ic_player_start"), for: .normal)
        }
    }
    /**
     Audio recorder delegate method
     */
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        audioRecorder?.delegate = nil
        self.recordButton.setTitle(PollBrief.RecordAudio, for: .normal)
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.delegate.isNextBtnHidden(value: true)
        if let introModel = self.model, let image = introModel.image_url {
            self.imageView.sd_setImage(with: URL(string: image), placeholderImage: nil)
            titleLabel.text = introModel.title
            descLabel.text =  ""
            self.validateBottomButton(isShow: false)
        }
    }
    // Register to receive notification in your class
    // handle notification
    @objc func showImage(_ notification: NSNotification) {
        self.showPreviewScreen(notification)
    }
    func showPreviewScreen(_ notification: NSNotification? = nil) {
        switch self.mediaType {
        case .photo:
            if let image = notification?.userInfo?[MediaType.image.rawValue] as? UIImage {
                self.imageView.image = image
            }
            titleLabel.text = PollBrief.surveyMediaImage.localised
            self.validateBottomButton(isShow: true)
        case .video:
            if let image = notification?.userInfo?[MediaType.image.rawValue] as? UIImage {
                self.imageView.image = image
            }
            titleLabel.text = PollBrief.surveyMediaVideo.localised
            self.validateBottomButton(isShow: true)
        case .audio:
            titleLabel.text = PollBrief.surveyMediaAudio.localised
            self.validateBottomButton(isShow: true)
            self.crossButton.isHidden = true
        case .none:
            break
        case .some(.image):
            break
        }
        descLabel.text =  PollBrief.happyWithMedia.localised
        self.delegate.isNextBtnHidden(value: false)
    }
}
