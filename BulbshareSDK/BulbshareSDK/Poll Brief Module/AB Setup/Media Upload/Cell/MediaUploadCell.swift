import UIKit

class MediaUploadCell: PollBaseCollectionCell {

    // MARK: - IBOutlets
    @IBOutlet weak var mediaUploadInfoView: MediaUploadView!

    // MARK: - Helper methods
    override func bind(model: PollBriefResponse, delegate: PollBriefDelegate) {
        mediaUploadInfoView.bind(modelDetails: model, delegate: delegate)
    }
}
