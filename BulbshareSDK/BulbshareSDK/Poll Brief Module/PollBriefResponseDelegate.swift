import Foundation

/**
 Login callbacks from the SDK
 */
protocol PollBriefResponseDelegate: AnyObject {
    /**
     Callback delegate if data load successfully
     */
   func gotPollBriefDetailsSuccessfully()

    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func throwError(error: SDKError)
}
