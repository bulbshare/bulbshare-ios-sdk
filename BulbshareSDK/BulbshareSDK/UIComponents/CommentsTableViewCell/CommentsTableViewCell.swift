import UIKit

class CommentsTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var commentTime: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    // MARK: - Local Variables
    var commentBriefDataModel: CommentResponse?
    override func awakeFromNib() {
        super.awakeFromNib()
        makeRound()
        // Initialization code
    }
    
    func makeRound(){
            
            userImage.layer.borderWidth = 0
            userImage.layer.masksToBounds = false
            userImage.layer.cornerRadius = userImage.frame.height/2
            userImage.clipsToBounds = true
        }
    
    func setData(model: CommentResponse?){
        if model != nil{
            self.commentBriefDataModel = model
            userName.text = commentBriefDataModel?.user.display_name
            comment.text = commentBriefDataModel?.comment
            if #available(iOS 13.0, *) {
                userImage.sd_setImage(with: URL(string: (commentBriefDataModel?.user.avatar)!), placeholderImage: UIImage(named: "profile_placeholder"))
            } else {
                userImage.sd_setImage(with: URL(string: (commentBriefDataModel?.user.avatar)!), placeholderImage: nil)
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = Briefs.dateFormat
            let submittedOn = dateFormatter.date(from: commentBriefDataModel!.submitted_on)
            commentTime.text = submittedOn!.timeAgoDisplay()
        }
    }

}
extension Date {
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        let month = 4 * week
        
        if secondsAgo < minute {
            return "\(secondsAgo) sec ago"
        } else if secondsAgo < hour {
            return "\(secondsAgo / minute) min ago"
        } else if secondsAgo < day {
            return "\(secondsAgo / hour) hour ago"
        } else if secondsAgo < week {
            return "\(secondsAgo / day) day ago"
        } else if secondsAgo < month {
            return "\(secondsAgo / week) W ago"
        } else {
            return "\(secondsAgo / month) M ago"
        }
    }
}
