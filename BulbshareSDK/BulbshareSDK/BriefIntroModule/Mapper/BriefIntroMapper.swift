import UIKit

class BriefIntroMapper {
    class func map(model: BriefIntroResponse, gridModel: [BulbShareByBriefResponse]?) -> ([BriefIntroViewModel],Int) {
        var array: [BriefIntroViewModel] = []
        var numberOfCell = 1
        var hasNotch = true
        DispatchQueue.main.async {
            hasNotch = UIDevice.current.hasNotch
        }
        if model.coverVideoMobile != "" {
            let type = BriefIntroViewModel(model: model, cellType: BriefHeaderVideoViewCell.self, cellHeight: 0)
            array.append(type)
        } else {
            let type = BriefIntroViewModel(model: model, cellType: BriefHeaderImageViewCell.self, cellHeight: 0)
            array.append(type)
        }
        
        let type1 = BriefIntroViewModel(model: model, cellType: BriefDetailCollectionViewCell.self, cellHeight: 0)
        array.append(type1)
        
        if hasNotch{
            let type2 = BriefIntroViewModel(model: model, cellType: BriefDetailProgressViewCell.self, cellHeight: 320 + (model.feedTitle?.height())!)
            array.append(type2)
        } else {
            let type2 = BriefIntroViewModel(model: model, cellType: BriefDetailProgressViewCell.self, cellHeight: 290 + (model.feedTitle?.height())!)
            array.append(type2)
        }
        if model.bannerText != "" {
            let type3 = BriefIntroViewModel(model: model, cellType: BriefDetailTextLabelCell.self, cellHeight: (model.bannerText?.height())! + 10)
            array.append(type3)
            numberOfCell += 1
        }
        if model.ad1_Image != "" {
            let type3 = BriefIntroViewModel(model: model, cellType: BriefDetailImageCell.self, cellHeight: 300)
            array.append(type3)
            numberOfCell += 1
        }
        if model.dataDescription != "" {
            let type3 = BriefIntroViewModel(model: model, cellType: BriefDetailTextLabelCell.self, cellHeight: (model.dataDescription?.height())! + 10)
            array.append(type3)
            numberOfCell += 1
        }
        if model.ad2_Image != "" {
            let type3 = BriefIntroViewModel(model: model, cellType: BriefDetailImageCell.self, cellHeight: 300)
            array.append(type3)
            numberOfCell += 1
        }
        if (model.bulbsharesCount ?? 0) > 0 {
            var cellHeight = 0
            let maxGridSize = 12
            let gridCellHeight = Int((UIScreen.main.bounds.width-3)/3)
            let gridConstraint = 137
            if (gridModel?.count ?? 0) < maxGridSize {
                if (gridModel?.count ?? 0 ) % 3 == 0{
                    cellHeight = (((gridModel?.count ?? 0) / 3) * gridCellHeight) + gridConstraint
                } else{
                    cellHeight = ((1+(gridModel?.count ?? 0) / 3) * gridCellHeight) + gridConstraint
                }
            } else {
                cellHeight = ((4) * gridCellHeight) + gridConstraint
            }
            let type3 = BriefIntroViewModel(gridModel: gridModel, cellType: BriefDetailGridCollectionViewCell.self, cellHeight: CGFloat(cellHeight))
            array.append(type3)
            numberOfCell += 1
        }
        return (array,numberOfCell)
    }
}
