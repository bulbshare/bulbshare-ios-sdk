import Foundation

enum BriefIntroAPI{
    case getBriefIntro(model: APIRequest<BriefIntroRequestModel>)
}
extension BriefIntroAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .getBriefIntro:
            return "getbrief"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .getBriefIntro(let model):
            return BriefIntroRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
