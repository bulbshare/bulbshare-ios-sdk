import Foundation
import UIKit

class BriefIntroViewModel {
    var model: BriefIntroResponse?
    var gridModel: [BulbShareByBriefResponse]?
    var cellType: BriefIntroBaseCollectionCell.Type = BriefIntroBaseCollectionCell.self
    var cellHeight: CGFloat

    // MARK: - initializer
    init(model: BriefIntroResponse? = nil,gridModel: [BulbShareByBriefResponse]? = nil ,  cellType: BriefIntroBaseCollectionCell.Type, cellHeight: CGFloat) {
        self.model = model
        self.cellType = cellType
        self.cellHeight = cellHeight
        self.gridModel = gridModel
    }
}

struct HorizontalData{
    var model: [BriefIntroViewModel]?
    var numberOfrow: Int?
}
