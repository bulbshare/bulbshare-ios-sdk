struct BriefRefList {
    static var sharedInstance = BriefRefList()
    private init() {}
    var briefRefs: [String] = [String]()
    var briefIndex: Int = 0
}
