import Foundation

// MARK: - BulbShareByBriefResponse
struct BulbShareByBriefResponse: Codable {
    let bulbshareref: String
    let brief: BSBrief
    let user: BSUser
    let picture: String
    let commentStyle: CommentStyle
    let comment: String

    enum CodingKeys: String, CodingKey {
        case bulbshareref, picture
        case user
        case brief
        case commentStyle = "comment_style"
        case comment
    }
}

// MARK: - User
struct BSUser: Codable {
    let userref: String
    enum CodingKeys: String, CodingKey {
        case userref
    }
}

// MARK: - BSBrief Class
struct BSBrief: Codable {
    let briefref: String
    enum CodingKeys: String, CodingKey {
        case briefref
    }
}
