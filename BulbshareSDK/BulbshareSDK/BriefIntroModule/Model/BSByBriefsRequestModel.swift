import Foundation

struct BSByBriefsRequestModel: Codable {
    let briefref: String
}
class BSByBriefsRequest {
    func params(model: APIRequest<BSByBriefsRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
        }
        return [String: AnyObject]()
    }
}
