import Foundation

enum BSByBriefsAPI{
    case getBSByBriefs(model: APIRequest<BSByBriefsRequestModel>)
}
extension BSByBriefsAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .getBSByBriefs:
            return "getbulbsharesbybrief"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .getBSByBriefs(let model):
            return BSByBriefsRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
