import Foundation

struct BriefIntroRequestModel: Codable {
    let briefref: String
}
class BriefIntroRequest {
    func params(model: APIRequest<BriefIntroRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
        }
        return [String: AnyObject]()
    }
}
