import Foundation

// MARK: - Brief Intro Response
struct BriefIntroResponse: Codable {
    let briefref: String?
    let brand: BriefIntroBrand
    let name, internalName, dataDescription: String?
    let coverPhoto: String?
    let coverVideoMobile, coverVideoWeb, introText, feedTitle: String?
    let bannerImage: String?
    let bannerVideo, bannerText: String?
    let ad1_Image: String?
    let ad1_Href, ad2_Text: String?
    let ad2_Image: String?
    let ad2_Href, rewardImage, rewardText: String?
    let bsPictureRequired: Bool?
    let bsPictureText: String?
    let bsVideoRequired: Bool?
    let bsVideoText: String?
    let bsCommentRequired: Bool?
    let bsCommentText: String?
    let bsAllowLongVideo: Bool?
    let startsOn, endsOn: String?
    let commsSentOn: JSONNull?
    let introductionTitle, introductionSubtitle, introductionBody, introductionImage: String?
    let audition: Bool?
    let auditionTitle, auditionSubtitle, auditionSuccessImage, auditionSuccessText: String?
    let trackingURL, trackingFailureURL: String?
    let type: Int?
    let briefType: String?
    var likesCount, bulbsharesCount, responsesCount, commentsCount: Int?
    let responseLimit, maxResponses: Int?
    let status: String?
    let isPrivate, isPublic, isActivated: Bool?
    let isHidden, isEnrichment, organisationid, templateBriefID: String?
    let translateResponses, translateResponsesEngine, translateFrom, translateTo: String?
    let projectID, templateOrganisationID: JSONNull?
    let languageID: String?
    let bvProductID: JSONNull?
    let bvProductName: String?
    let bvIncentivised: JSONNull?
    let userHasResponded: Bool?
    let userResponsesCount: Int?
    let canSubmitBulbshares, canAnswerPoll: Bool?
    let averageDuration: String?
    let userRole: Int?
    var wasLikedByMe, isSyndicationBrief: Bool?

    enum CodingKeys: String, CodingKey {
        case briefref, brand, name
        case internalName = "internal_name"
        case dataDescription = "description"
        case coverPhoto = "cover_photo"
        case coverVideoMobile = "cover_video_mobile"
        case coverVideoWeb = "cover_video_web"
        case introText = "intro_text"
        case feedTitle = "feed_title"
        case bannerImage = "banner_image"
        case bannerVideo = "banner_video"
        case bannerText = "banner_text"
        case ad1_Image = "ad_1_image"
        case ad1_Href = "ad_1_href"
        case ad2_Text = "ad_2_text"
        case ad2_Image = "ad_2_image"
        case ad2_Href = "ad_2_href"
        case rewardImage = "reward_image"
        case rewardText = "reward_text"
        case bsPictureRequired = "bs_picture_required"
        case bsPictureText = "bs_picture_text"
        case bsVideoRequired = "bs_video_required"
        case bsVideoText = "bs_video_text"
        case bsCommentRequired = "bs_comment_required"
        case bsCommentText = "bs_comment_text"
        case bsAllowLongVideo = "bs_allow_long_video"
        case startsOn = "starts_on"
        case endsOn = "ends_on"
        case commsSentOn = "comms_sent_on"
        case introductionTitle = "introduction_title"
        case introductionSubtitle = "introduction_subtitle"
        case introductionBody = "introduction_body"
        case introductionImage = "introduction_image"
        case audition
        case auditionTitle = "audition_title"
        case auditionSubtitle = "audition_subtitle"
        case auditionSuccessImage = "audition_success_image"
        case auditionSuccessText = "audition_success_text"
        case trackingURL = "tracking_url"
        case trackingFailureURL = "tracking_failure_url"
        case type
        case briefType = "brief_type"
        case likesCount = "likes_count"
        case bulbsharesCount = "bulbshares_count"
        case responsesCount = "responses_count"
        case commentsCount = "comments_count"
        case responseLimit = "response_limit"
        case maxResponses = "max_responses"
        case status
        case isPrivate = "is_private"
        case isPublic = "is_public"
        case isActivated = "is_activated"
        case isHidden = "is_hidden"
        case isEnrichment = "is_enrichment"
        case organisationid
        case templateBriefID = "template_brief_id"
        case translateResponses = "translate_responses"
        case translateResponsesEngine = "translate_responses_engine"
        case translateFrom = "translate_from"
        case translateTo = "translate_to"
        case projectID = "project_id"
        case templateOrganisationID = "template_organisation_id"
        case languageID = "language_id"
        case bvProductID = "bv_product_id"
        case bvProductName = "bv_product_name"
        case bvIncentivised = "bv_incentivised"
        case userHasResponded = "user_has_responded"
        case userResponsesCount = "user_responses_count"
        case canSubmitBulbshares = "can_submit_bulbshares"
        case canAnswerPoll = "can_answer_poll"
        case averageDuration = "average_duration"
        case userRole = "user_role"
        case wasLikedByMe = "was_liked_by_me"
        case isSyndicationBrief = "is_syndication_brief"
    }
}

// MARK: - Brand
struct BriefIntroBrand: Codable {
    let brandid: Int?
    let brandref, name: String?
    let logo: String?
    let likesCount, followersCount: Int?
    let isDiscoverable: Bool?
    let discoverTitle, discoverAbout, discoveryCountry: String?
    let requireScreener: Bool?
    let screenerURL: String?
    let screenerBriefid: JSONNull?
    let briefMaxResponsesLimit, status: String?
    let isPrivate: Bool?
    let organisationid, languageID, languageName, code: String?
    let primaryColor, secondaryColor: Color

    enum CodingKeys: String, CodingKey {
        case brandid, brandref, name, logo
        case likesCount = "likes_count"
        case followersCount = "followers_count"
        case isDiscoverable = "is_discoverable"
        case discoverTitle = "discover_title"
        case discoverAbout = "discover_about"
        case discoveryCountry = "discovery_country"
        case requireScreener = "require_screener"
        case screenerURL = "screener_url"
        case screenerBriefid = "screener_briefid"
        case briefMaxResponsesLimit = "brief_max_responses_limit"
        case status
        case isPrivate = "is_private"
        case organisationid
        case languageID = "language_id"
        case languageName = "language_name"
        case code
        case primaryColor = "primary_color"
        case secondaryColor = "secondary_color"
    }
}
