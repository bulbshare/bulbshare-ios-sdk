import UIKit

/**
 Login callbacks from the SDK
 */
protocol BriefIntroResponseDelegate: AnyObject {
    /**
     Callback delegate if data load successfully
     */
   func briefIntroDataGotSuccessfully()

    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func briefIntroThrowError(error: SDKError)
    
    
    /**
     Callback delegateto show poll brief
     
     - parameter id: Brief ID, navigation: UINavigationController
     */
    func showPollBrief(id: String, navigation: UINavigationController)
}
