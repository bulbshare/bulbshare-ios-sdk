import Foundation
protocol BriefIntroDelegate: BaseDelegate {
    /**
     callback delegate
     */
    func getBriefIntroData(data: BriefIntroResponse)
    func setupView(data: [BriefIntroViewModel], numberOfRows: Int)
    func getBSByBriefsData(data: [BulbShareByBriefResponse])
}

protocol BriefIntroHeaderDelegate: BaseDelegate{
    func closeBriefIntroView()
    func showCommentController()
    func likeButtonTapped(likeOrUnlike: String)
    func showPollBriefController()
}
