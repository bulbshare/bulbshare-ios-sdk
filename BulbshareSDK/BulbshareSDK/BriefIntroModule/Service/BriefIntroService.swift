import Foundation
class BriefIntroService {
    // MARK: - Variables
    private let router = ServiceManager<BriefIntroAPI>()
    func getBriefIntro(
        model: APIRequest<BriefIntroRequestModel>,
        completion: @escaping (_ response: BriefIntroResponse?,
                               _ error: Error?) -> Void) {
                router.request(.getBriefIntro(model: model)) { (response, error) in
                    guard  error == nil else {
                        completion(nil, error)
                        return
                    }
                    do {
                        guard let response = response as? Data else {
                            completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                            return
                        }
                        let jsonDecoder = JSONDecoder()
                        let responseData = try jsonDecoder.decode(APIResponse<BriefIntroResponse>.self, from: response)
                        completion(responseData.data, nil)
                    } catch {
                        completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    }
                }
        
    }
}
