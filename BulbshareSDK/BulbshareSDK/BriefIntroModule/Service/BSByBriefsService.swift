import Foundation
class BSByBriefsService {
    // MARK: - Variables
    private let router = ServiceManager<BSByBriefsAPI>()
    func getBSByBriefs(
        model: APIRequest<BSByBriefsRequestModel>,
        completion: @escaping (_ response: [BulbShareByBriefResponse]?,
                               _ error: Error?) -> Void) {
                router.request(.getBSByBriefs(model: model)) { (response, error) in
                    guard  error == nil else {
                        completion(nil, error)
                        return
                    }
                    do {
                        guard let response = response as? Data else {
                            completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                            return
                        }
                        let jsonDecoder = JSONDecoder()
                        let responseData = try jsonDecoder.decode(APIResponse<[BulbShareByBriefResponse]>.self, from: response)
                        completion(responseData.data, nil)
                    } catch {
                        completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    }
                }
        
    }
}
