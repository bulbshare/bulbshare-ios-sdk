import Foundation

class BriefIntroManager{
    // MARK: - Variables
    fileprivate var service: BriefIntroService = BriefIntroService()
    var likeUnlikeBriefManager: LikeUnlikeBriefManager = LikeUnlikeBriefManager()
    /**
     method to getBreifIntro data
     */
    func getBriefIntro(briefref: String , completion: @escaping (_ response: BriefIntroResponse?, _ error: Error?) -> Void)
    {
        let arg = BriefIntroRequestModel(briefref: briefref)
        let model = APIRequest<BriefIntroRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.getBriefIntro(model: model) { response, error in
            completion(response,error)
        }
    }
    /**
     method to get LikeUnlikeBrief data
     */
    func likeUnlikeBrief(ref: String, type: String, completion: @escaping (_ response: Bool?, _ error: Error?) -> Void){
        likeUnlikeBriefManager.likeBrief(ref: ref, type: type, completion: completion)
    }
    
}
