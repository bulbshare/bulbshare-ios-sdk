import Foundation

class BSByBriefsManager{
    // MARK: - Variables
    fileprivate var service: BSByBriefsService = BSByBriefsService()
    /**
     method to getBreifIntro data
     */
    func getBSByBriefs(briefref: String , completion: @escaping (_ response: [BulbShareByBriefResponse]?, _ error: Error?) -> Void)
    {
        let arg = BSByBriefsRequestModel(briefref: briefref)
        let model = APIRequest<BSByBriefsRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.getBSByBriefs(model: model) { response, error in
            completion(response,error)
        }
    }
}
