import Foundation
import UIKit

class BriefIntroInteractor {
    // MARK: - Local Variables
    fileprivate weak var delegate: BriefIntroResponseDelegate!

    // MARK: - Initializers
    init(delegate: BriefIntroResponseDelegate) {
        self.delegate = delegate
    }
    /**
     Show Brief intro controller
     */
    func showBriefIntro(navigation: UINavigationController, briefID: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Feed", bundle: SDKBundles.sdkBundle)
        let vc = storyBoard.instantiateViewController(withIdentifier: "BriefIntroViewController") as! BriefIntroViewController
        vc.briefID = briefID
        vc.callBackToInteractor = { [weak self] (value, error) in
            if value {
                self?.delegate.briefIntroDataGotSuccessfully()
            } else if let error = error {
                self?.delegate.briefIntroThrowError(error: error)
            }
        }
        vc.callBackToBriefIntroInteractor = {  [weak self] id in
            self?.delegate.showPollBrief(id: id, navigation: navigation)
        }
        navigation.pushViewController(vc, animated: false)
    }
}
