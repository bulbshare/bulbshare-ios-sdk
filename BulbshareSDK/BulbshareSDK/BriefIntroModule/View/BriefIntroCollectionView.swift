import Foundation
import UIKit

class BriefIntroCollectionView: UICollectionView{
    // MARK: - Local Variables
    weak var headerDelegate: BriefIntroHeaderDelegate?
    var briefIntroResponse: BriefIntroResponse?
    var bsByBriefsResponse: [BulbShareByBriefResponse]?
    var briefDetailViewModel: [BriefIntroViewModel]!
    
    var numberOfRow = 0
    // MARK: - Life Cycle Methods
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        registerCell()
    }
    
    // MARK: - Helper Methods
    func bindData(BriefIntroData: BriefIntroResponse?, delegate: BriefIntroHeaderDelegate?){
        self.briefIntroResponse = BriefIntroData
        self.headerDelegate = delegate
        
    }
    func setData(viewModel: [BriefIntroViewModel],row: Int, delegate: BriefIntroHeaderDelegate){
        self.headerDelegate = delegate
        self.briefDetailViewModel = viewModel
        self.numberOfRow = row
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
    
    func setGrid(model: [BulbShareByBriefResponse]){
        self.bsByBriefsResponse = model
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
    /**
     This method register the cells
     */
    func registerCell(){
        self.register(UINib(nibName: BriefIntro.BriefHeaderVideoViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefHeaderVideoViewCell)
        self.register(UINib(nibName: BriefIntro.BriefHeaderImageViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefHeaderImageViewCell)
        self.register(UINib(nibName: BriefIntro.BriefDetailCollectionViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefDetailCollectionViewCell)
        self.register(UINib(nibName: BriefIntro.LoadingCollectionViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.LoadingCollectionViewCell)
        
    }
}
// MARK: - Collection View Delegate and Data Source
extension BriefIntroCollectionView: UICollectionViewDelegate, UICollectionViewDataSource,                                    UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if briefDetailViewModel != nil{
            if briefDetailViewModel.first?.cellType == LoadingCollectionViewCell.self{
                return 1
            }
            return 2
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Put Data Type Condition
        guard let cell: BriefIntroBaseCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: self.briefDetailViewModel[indexPath.row].cellType), for: indexPath) as? BriefIntroBaseCollectionCell else {
            return UICollectionViewCell()
        }
        cell.setBriefHeaderData(model: (briefDetailViewModel.first?.model), delegate: headerDelegate)
        cell.setBriefDetailData(model: briefDetailViewModel, row: numberOfRow)
        cell.callBackToController = { [weak self] in
            self?.headerDelegate?.showPollBriefController()
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width , height: self.frame.height)
    }
}
