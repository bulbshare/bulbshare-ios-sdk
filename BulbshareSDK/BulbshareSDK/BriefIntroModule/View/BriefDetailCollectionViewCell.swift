import UIKit
class BriefDetailCollectionViewCell: BriefIntroBaseCollectionCell {
    // MARK: - IBOutlets
    @IBOutlet weak var briefDetailCollectionViewTopMargin: NSLayoutConstraint!
    @IBOutlet weak var briefDetailCollectionView: UICollectionView!
    @IBOutlet weak var surveyButton: UIButton!
    // MARK: - Local Variables
    var briefIntroDataModel: BriefIntroResponse!
    var itemCount = 0
    var briefIntroViewModel: [BriefIntroViewModel]!
    var bsByBriefResponse: [BulbShareByBriefResponse]!
    // MARK: - Life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func buttonAction(_ sender: Any) {
        if let callback = self.callBackToController {
            callback()
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        registerBriefDetailCollectionViewCell()
        briefDetailCollectionView.delegate = self
        briefDetailCollectionView.dataSource = self
        briefDetailCollectionView.contentInsetAdjustmentBehavior = .never
        
    }
    // MARK: - Helper methods
    /**
     method to register collection view cells
     */
    func registerBriefDetailCollectionViewCell(){
        briefDetailCollectionView.register(UINib(nibName: BriefIntro.BriefDetailProgressViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefDetailProgressViewCell)
        briefDetailCollectionView.register(UINib(nibName: BriefIntro.BriefDetailTextLabelCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefDetailTextLabelCell)
        briefDetailCollectionView.register(UINib(nibName: BriefIntro.BriefDetailImageCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefDetailImageCell)
        briefDetailCollectionView.register(UINib(nibName: BriefIntro.BriefDetailGridCollectionViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefDetailGridCollectionViewCell)
        
    }
    
    override func setBriefDetailData(model: [BriefIntroViewModel],row: Int){
        self.briefIntroViewModel = model
        self.briefIntroDataModel = model.first?.model
        if briefIntroDataModel.userHasResponded! {
            self.surveyButton.setTitle(BriefIntro.completed.localised, for: .normal)
            self.surveyButton.backgroundColor = UIColor(hexString: ColorConstants.greenColor)
        } else {
            self.surveyButton.setTitle(BriefIntro.takePart.localised, for: .normal)
            self.surveyButton.backgroundColor = UIColor(hexString: ColorConstants.blueColor)
        }
        self.itemCount = row
        DispatchQueue.main.async {
            self.briefDetailCollectionView.reloadData()
        }
    }
    /**
     method to set size for item
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.briefDetailCollectionView.frame.width, height: briefIntroViewModel[indexPath.row + 2].cellHeight)
    }
}
// MARK: - Collection view delegate and data source
extension BriefDetailCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return itemCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: BriefIntroBaseCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: self.briefIntroViewModel[indexPath.row + 2].cellType), for: indexPath) as? BriefIntroBaseCollectionCell else {
            return UICollectionViewCell()
        }
        cell.setData(model: (briefIntroViewModel.first?.model)!, row: indexPath.row)
        cell.setGrid(model: briefIntroViewModel.last?.gridModel)
        return cell
    }
}
// MARK: - String extension to set height
extension String {
    func height(withConstrainedWidth width: CGFloat = UIScreen.main.bounds.width-26, font: UIFont = UIFont.init(name: "Avenir", size: 18)!) -> CGFloat? {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
}
