import UIKit

class BriefIntroVerticalCell: UICollectionViewCell {
    
    @IBOutlet weak var briefIntroCollectionView: BriefIntroCollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        briefIntroCollectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        briefIntroCollectionView.contentInsetAdjustmentBehavior = .never
    }
    func setData(data: [BriefIntroViewModel]?, numberOfRows: Int,delegate: BriefIntroHeaderDelegate){
        DispatchQueue.main.async {
            self.briefIntroCollectionView.setData(viewModel: data ?? [BriefIntroViewModel(cellType: LoadingCollectionViewCell.self, cellHeight: 100)], row: numberOfRows, delegate: delegate)
        }
    }
}
