import UIKit
import AVKit

class BriefIntroViewController: BaseViewController {
    
    @IBOutlet weak var briefHorizontalCollectionView: UICollectionView!
    // MARK: - Local variables
    var topMargin: CGFloat = 0
    var briefIntroResponse: BriefIntroResponse?
    var bsByBriefsResponse: [BulbShareByBriefResponse]?
    var presenter: BriefIntroPresenter!
    var briefID: String?
    var callBackToBriefIntroInteractor: ((String) -> Void)?
    var currentBriefData = true
    var currentBriefIndex = 0
    var leftBriefIndex = 0
    var rightBriefIndex = 0
    var horizontalData: [HorizontalData] = [HorizontalData](repeating: HorizontalData(model: nil, numberOfrow: nil), count: BriefRefList.sharedInstance.briefRefs.count)
    var indexPath: IndexPath!
    var feedTitle: String = ""
    /**
     method to set status bar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = BriefIntroPresenter(delegate: self)
        briefHorizontalCollectionView.delegate = self
        briefHorizontalCollectionView.dataSource = self
        briefHorizontalCollectionView.contentInsetAdjustmentBehavior    = .never
        registerCell()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBulbshareList(_:)), name: NSNotification.Name(rawValue: "bulbshareListCallback"), object: nil)
    }
    @objc func showBulbshareList(_ notification: NSNotification) {
        if let briefRef = notification.userInfo?["briefRef"] as? String {
            let storyBoard: UIStoryboard = UIStoryboard(name: "BulbshareList", bundle: SDKBundles.sdkBundle)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BulbshareListViewController") as! BulbshareListViewController
            nextVC.input = BSBulbshareListInput(briefRef: briefRef, count: 50, sortType: 2)
            nextVC.feedTitle = self.feedTitle
            self.navigationController?.pushViewController(nextVC, animated: true)

                }
          }
    
    
    func registerCell(){
        briefHorizontalCollectionView.register(UINib(nibName: BriefIntro.BriefIntroVerticalCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.BriefIntroVerticalCell)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let id = briefID {
            showLoader()
            presenter.getBriefIntro(briefRefID: id)
            currentBriefIndex = BriefRefList.sharedInstance.briefRefs.firstIndex(of: id) ?? 0
            leftBriefIndex = currentBriefIndex - 1
            rightBriefIndex = currentBriefIndex + 1
        }
        
    }
}

// MARK: - CollectionView

extension BriefIntroViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if horizontalData[currentBriefIndex].model != nil{
            return BriefRefList.sharedInstance.briefRefs.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = briefHorizontalCollectionView.dequeueReusableCell(withReuseIdentifier: BriefIntro.BriefIntroVerticalCell, for: indexPath) as! BriefIntroVerticalCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if horizontalData.count != 0{
            if indexPath.row > 0 {
                if horizontalData[indexPath.row - 1].model == nil{
                    loadLeftBriefIntro(index: indexPath.row - 1)
                }
            }
            if indexPath.row < BriefRefList.sharedInstance.briefRefs.count-1{
                if horizontalData[indexPath.row + 1].model == nil{
                    loadLeftBriefIntro(index: indexPath.row + 1)
                }
            }
            if let cell = cell as? BriefIntroVerticalCell{
                self.feedTitle = horizontalData[indexPath.row].model?.first?.model?.feedTitle ?? ""
                cell.setData(data: horizontalData[indexPath.row].model  , numberOfRows: horizontalData[indexPath.row].numberOfrow ?? 0, delegate: self)
                currentBriefIndex = indexPath.row
            }
        }
    }
}


// MARK: - View controller Delegates
extension BriefIntroViewController: BriefIntroDelegate, BriefIntroHeaderDelegate{
    func getBSByBriefsData(data: [BulbShareByBriefResponse]) {
        self.bsByBriefsResponse = data
        DispatchQueue.main.async { [weak self] in
            self?.briefHorizontalCollectionView.reloadData()
        }
    }
    func setupView(data: [BriefIntroViewModel], numberOfRows: Int) {
        let index = BriefRefList.sharedInstance.briefRefs.firstIndex(of: (data.first?.model?.briefref)!)
        horizontalData[index ?? 0] = HorizontalData(model: data, numberOfrow: numberOfRows)
        if currentBriefData {
            currentBriefData = false
            DispatchQueue.main.async { [weak self] in
                self?.briefHorizontalCollectionView.reloadData()
                self?.briefHorizontalCollectionView.scrollToItem(at: IndexPath(row: self!.currentBriefIndex, section: 0), at: .right, animated: false)
                
            }
            if leftBriefIndex >= 0{
                self.presenter.getBriefIntro(briefRefID: BriefRefList.sharedInstance.briefRefs[leftBriefIndex])
                leftBriefIndex -= 1
            }
            if rightBriefIndex < BriefRefList.sharedInstance.briefRefs.count{
                self.presenter.getBriefIntro(briefRefID: BriefRefList.sharedInstance.briefRefs[rightBriefIndex])
                rightBriefIndex += 1
            }
        } else{
            if index == currentBriefIndex{
                DispatchQueue.main.async { [weak self] in
                    self?.briefHorizontalCollectionView.reloadData()
                    self?.briefHorizontalCollectionView.scrollToItem(at: IndexPath(row: self!.currentBriefIndex, section: 0), at: .right, animated: false)
                }
            }
        }
    }
    /**
     This method is used to load left and right brief intro data
     */
    func loadLeftBriefIntro(index: Int) {
        self.presenter.getBriefIntro(briefRefID: BriefRefList.sharedInstance.briefRefs[index])
    }
    
    /**
     This method is used to give callback to pollBrief view controller
     */
    func showPollBriefController() {
        callBackToBriefIntroInteractor?(briefID!)
    }
    
    /**
     method to dismiss view
     */
    func closeBriefIntroView() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    /**
     method to get data
     */
    func getBriefIntroData(data: BriefIntroResponse) {
        self.briefIntroResponse = data
    }
    /**
     method to navigate to comment brief
     */
    func showCommentController() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: Feeds.Feed, bundle: SDKBundles.sdkBundle)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: BriefIntro.CommentViewController) as! CommentViewController
            nextVC.briefRefID = BriefRefList.sharedInstance.briefRefs[self.currentBriefIndex]
            self.briefID = BriefRefList.sharedInstance.briefRefs[self.currentBriefIndex]
            self.navigationController?.pushViewController(nextVC, animated: false)
        }
    }
    /**
     method for like unlike
     */
    func likeButtonTapped(likeOrUnlike: String) {
        self.presenter.getLikeUnlike(ref: BriefRefList.sharedInstance.briefRefs[currentBriefIndex], type: likeOrUnlike)
        if likeOrUnlike == "like"{
            horizontalData[currentBriefIndex].model?.first?.model?.wasLikedByMe = true
            horizontalData[currentBriefIndex].model?.first?.model?.likesCount! += 1
        } else{
            horizontalData[currentBriefIndex].model?.first?.model?.wasLikedByMe = false
            horizontalData[currentBriefIndex].model?.first?.model?.likesCount! -= 1
        }
    }
}
