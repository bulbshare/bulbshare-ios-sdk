import UIKit

class BriefIntroBaseCollectionCell: UICollectionViewCell {
    
    var callBackToController:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func setData(model: BriefIntroResponse, row: Int) {
    }
    
     
    func validate() {
    }
    
    func setBriefHeaderData(model: BriefIntroResponse?, delegate: BriefIntroHeaderDelegate?){
        
    }
    
    func setBriefDetailData(model: [BriefIntroViewModel],row: Int){
        
    }
    func setGrid(model: [BulbShareByBriefResponse]?){
        
    }
}
