import UIKit
import AVKit
import AVFoundation

class BriefHeaderVideoViewCell: BriefIntroBaseCollectionCell {

    // MARK: - IBOutlets
    @IBOutlet weak var introText: UILabel!
    @IBOutlet weak var swipeUpGif: UIImageView!
    @IBOutlet weak var swipeupGifView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var bulbshareButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: - Local variables
    var briefIntroDataModel: BriefIntroResponse!
    var isliked: Bool = false
    weak var delegate: BriefIntroHeaderDelegate?
    var likeCountNumber: Int = 0
    var player: AVPlayer!
    // MARK: - Life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    // MARK: - IBActions
    
    @IBAction func bulbshareButtonTapped(_ sender: Any) {
    }
    
    @IBAction func likeButtonTapped(_ sender: Any) {
        if isliked{
            likeButton.setImage(UIImage(named: BriefIntro.like), for: .normal)
            likeCountNumber = likeCountNumber == 0 ? 0 : likeCountNumber - 1
            likeButton.setTitle(String(likeCountNumber), for: .normal)
            delegate?.likeButtonTapped(likeOrUnlike: "unlike")
        } else{
            likeButton.setImage(UIImage(named: BriefIntro.liked), for: .normal)
            likeCountNumber += 1
            likeButton.setTitle(String(likeCountNumber), for: .normal)
            delegate?.likeButtonTapped(likeOrUnlike: "like")
        }
        isliked = !isliked

    }
    
    @IBAction func commentButtonTapped(_ sender: Any) {
        delegate?.showCommentController()
    }
    
    @IBAction func cancelActionButton(_ sender: Any) {
        delegate?.closeBriefIntroView()
    }
    /**
     method to bind data
     */
    override func setBriefHeaderData(model: BriefIntroResponse?, delegate: BriefIntroHeaderDelegate?){
        
        self.delegate = delegate
        if model != nil {
            swipeUpGif.loadGif(name: BriefIntro.swipeUp)
            self.briefIntroDataModel = model
            introText.text = briefIntroDataModel.introText
            bulbshareButton.setTitle(String(briefIntroDataModel.bulbsharesCount ?? 0), for: .normal)
            likeButton.setTitle(String(briefIntroDataModel.likesCount ?? 0), for: .normal)
            commentButton.setTitle(String(briefIntroDataModel.commentsCount ?? 0), for: .normal)
            isliked = briefIntroDataModel.wasLikedByMe ?? false
            likeCountNumber = briefIntroDataModel.likesCount ?? 0
            if(isliked){
                likeButton.setImage(UIImage(named: BriefIntro.liked), for: .normal)
            } else{
                likeButton.setImage(UIImage(named: BriefIntro.like), for: .normal)
            }
            callvideo()
        }
    }
    /**
     method to call video
     */
    func callvideo(){
        self.delegate?.showLoader()
        let videoURl = URL(string: briefIntroDataModel.coverVideoMobile ?? "")
        player = AVPlayer(url: videoURl!)

        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = UIScreen.main.bounds

        print(self.contentView.bounds.width, self.contentView.bounds.height)
        playerLayer.videoGravity = .resizeAspectFill
        print(playerLayer.videoRect)

        self.videoView.layer.insertSublayer(playerLayer, at: 0)
        self.delegate?.hideLoader()
        player.play()
        self.videoView.bringSubviewToFront(introText)
        self.videoView.bringSubviewToFront(swipeUpGif)
        self.videoView.bringSubviewToFront(swipeupGifView)
        self.videoView.bringSubviewToFront(stackView)
    }

}
