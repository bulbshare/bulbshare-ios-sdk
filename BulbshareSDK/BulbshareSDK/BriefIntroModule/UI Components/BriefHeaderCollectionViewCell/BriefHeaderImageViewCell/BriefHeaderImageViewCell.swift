import UIKit
class BriefHeaderImageViewCell: BriefIntroBaseCollectionCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var swipeUpGif: UIImageView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var introText: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var bulbshareButton: UIButton!
    // MARK: - local variables
    var isliked: Bool = false
    var briefIntroDataModel: BriefIntroResponse!
    weak var delegate: BriefIntroHeaderDelegate?
    var likeCountNumber: Int = 0
    // MARK: - Life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    // MARK: - IBActions
    @IBAction func bulbshareButtonTapped(_ sender: Any) {
    }
    
    @IBAction func likeButtonTapped(_ sender: Any) {
        if isliked{
            likeButton.setImage(UIImage(named: BriefIntro.like), for: .normal)
            likeCountNumber = likeCountNumber == 0 ? 0 : likeCountNumber - 1
            likeButton.setTitle(String(likeCountNumber), for: .normal)
            delegate?.likeButtonTapped(likeOrUnlike: "unlike")
        } else{
            likeButton.setImage(UIImage(named: BriefIntro.liked), for: .normal)
            likeCountNumber += 1
            likeButton.setTitle(String(likeCountNumber), for: .normal)
            delegate?.likeButtonTapped(likeOrUnlike: "like")
        }
        isliked = !isliked
    }
    
    @IBAction func commentButtonTapped(_ sender: Any) {
        delegate?.showCommentController()
    }
    
    @IBAction func cancelActionButton(_ sender: Any) {
        delegate?.closeBriefIntroView()
    }
    /**
     method to bind data
     */
    override func setBriefHeaderData(model: BriefIntroResponse?, delegate: BriefIntroHeaderDelegate?){
        
        self.delegate = delegate
        if model != nil {
            swipeUpGif.loadGif(name: BriefIntro.swipeUp)
            self.briefIntroDataModel = model
    
            DispatchQueue.main.async {
                self.backgroundImage.sd_setShowActivityIndicatorView(true)
                self.backgroundImage.sd_setIndicatorStyle(.gray)
                self.backgroundImage.sd_setImage(with: URL(string: self.briefIntroDataModel.coverPhoto ?? ""), placeholderImage: nil)
            }
            introText.text = briefIntroDataModel.introText
            introText.fadeIn(duration: 1.0, delay: 1.0) { finished in
            }
            bulbshareButton.setTitle(String(briefIntroDataModel.bulbsharesCount ?? 0), for: .normal)
            likeButton.setTitle(String(briefIntroDataModel.likesCount ?? 0), for: .normal)
            commentButton.setTitle(String(briefIntroDataModel.commentsCount ?? 0), for: .normal)
            isliked = briefIntroDataModel.wasLikedByMe ?? false
            if isliked {
                likeButton.setImage(UIImage(named: BriefIntro.liked), for: .normal)
            } else{
                likeButton.setImage(UIImage(named: BriefIntro.like), for: .normal)
            }
            likeCountNumber = briefIntroDataModel.likesCount ?? 0
            
        }
        
    }
}
