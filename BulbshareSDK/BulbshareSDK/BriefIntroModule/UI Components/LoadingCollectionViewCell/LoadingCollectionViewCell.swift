import UIKit

class LoadingCollectionViewCell: BriefIntroBaseCollectionCell {
    
    @IBOutlet weak var imageView: UIImageView!
    weak var delegate: BriefIntroHeaderDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setBriefHeaderData(model: BriefIntroResponse?, delegate: BriefIntroHeaderDelegate?) {
        self.delegate = delegate
    }
    
}
