import UIKit

class BriefDetailGridCollectionViewCell: BriefIntroBaseCollectionCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var gridCollectionView: UICollectionView!
    @IBOutlet weak var showAllBulbshares: UIButton!
    var model: [BulbShareByBriefResponse]!
    var modelCount: Int!
    let gridSize: Int = 12
    // MARK: - Life Cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        showAllBulbshares.isHidden = true
        showAllBulbshares.isUserInteractionEnabled = false
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        gridCollectionView.register(UINib(nibName: BriefIntro.GridCollectionViewCell, bundle: SDKBundles.sdkBundle), forCellWithReuseIdentifier: BriefIntro.GridCollectionViewCell)
        gridCollectionView.delegate = self
        gridCollectionView.dataSource = self
    }
    override func setGrid(model: [BulbShareByBriefResponse]?){
        if model != nil{
            self.model = model
            self.modelCount = (model?.count ?? 0)
            if self.modelCount > self.gridSize {
                showAllBulbshares.isHidden = false
                showAllBulbshares.isUserInteractionEnabled = true
                showAllBulbshares.setTitle("Show all Bulbshares (\(String(describing: modelCount!)))", for: .normal)
            } else{
                showAllBulbshares.isHidden = true
                showAllBulbshares.isUserInteractionEnabled = false
            }
            gridCollectionView.reloadData()
        }
    }
    
}
// MARK: - Collection view dataSource and delegate
extension  BriefDetailGridCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if model != nil{
            if model.count < gridSize {
                return model.count
            }
            showAllBulbshares.isHidden = false
            showAllBulbshares.isUserInteractionEnabled = true
            return gridSize
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BriefIntro.GridCollectionViewCell, for: indexPath) as! GridCollectionViewCell
        cell.setCellData(model: model[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageDataDict: [String: String] = ["briefRef": self.model[indexPath.row].brief.briefref]
        NotificationCenter.default.post(name: Notification.Name("bulbshareListCallback"), object: nil, userInfo: imageDataDict)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth =  (gridCollectionView.frame.width-3)/3
        return CGSize(width: cellWidth , height: cellWidth)
    }
    
    func setData(model: [BulbShareByBriefResponse]){
        self.model = model
    }
    
    
}
