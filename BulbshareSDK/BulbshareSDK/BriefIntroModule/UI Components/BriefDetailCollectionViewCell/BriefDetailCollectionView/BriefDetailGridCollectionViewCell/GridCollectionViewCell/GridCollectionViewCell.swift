import UIKit

class GridCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var placeHolderImage: UIImageView!
    @IBOutlet weak var imageCell: UIImageView!
    // MARK: - Life Cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setCellData(model: BulbShareByBriefResponse){
        self.imageCell.sd_setShowActivityIndicatorView(true)
        self.imageCell.sd_setIndicatorStyle(.gray)
        if model.picture == "" {
            //imageCell.image = UIImage(named: "no_brief")
            imageCell.isHidden = true
            placeHolderImage.isHidden = false
            self.backgroundColor = UIColor().rgbToHex(red: model.commentStyle.bgColor.r, green: model.commentStyle.bgColor.g, blue: model.commentStyle.bgColor.b, alpha: model.commentStyle.bgColor.a)
            placeHolderImage.tintColor = UIColor().rgbToHex(red: model.commentStyle.fontColor.r, green: model.commentStyle.fontColor.g, blue: model.commentStyle.fontColor.b, alpha: model.commentStyle.fontColor.a)
        } else {
            placeHolderImage.isHidden = true
            imageCell.isHidden = false
            imageCell.sd_setImage(with: URL(string: model.picture), placeholderImage: nil)
        }
    }
}
