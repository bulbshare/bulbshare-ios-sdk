import UIKit

class BriefDetailProgressViewCell: BriefIntroBaseCollectionCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var rightMargin: NSLayoutConstraint!
    @IBOutlet weak var labelBox: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var feedTitle: UILabel!
    @IBOutlet weak var leftDays: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var view: UIView!
    var briefIntroDataModel: BriefIntroResponse!
    @IBOutlet weak var triangleRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    // MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        progressView.layer.sublayers![1].cornerRadius = 7
        progressView.subviews[1].clipsToBounds = true
        progressView.clipsToBounds = true
        labelBox.layer.cornerRadius = 15
        makeRound()
        if !UIDevice.current.hasNotch {
            progressViewHeight.constant = (self.view.frame.height/2) - 12
        } else {
            progressViewHeight.constant = self.view.frame.height + 20
        }
    }
    // MARK: - Helper Methods
    /**
     method to set progress view and days left
     */
    func setprogress(){
        let endDate = briefIntroDataModel.endsOn
        let startDate = briefIntroDataModel.startsOn
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Briefs.dateFormat
        let endsDate = dateFormatter.date(from: endDate!)
        let currentTimeToString = dateFormatter.string(from: Date())
        let startsDate = dateFormatter.date(from: startDate!)
        let currentDate = dateFormatter.date(from: currentTimeToString)!
        let TimeLeft = Calendar.current.dateComponents([.day,.hour,.minute], from: currentDate, to: endsDate ?? currentDate)
        if TimeLeft.day! >= 1{
            if let dayTime = TimeLeft.day{
                leftDays.text = "\(dayTime)d left"
            }
        } else if TimeLeft.hour! >= 1 {
            if let hourTime = TimeLeft.hour{
                leftDays.text = "\(hourTime)h left"
            }
        } else if TimeLeft.minute! >= 1{
            if let minTime = TimeLeft.minute{
                leftDays.text = "\(minTime)m left"
            }
        } else{
            leftDays.text = Briefs.completed
        }
        
        let totalDays = Calendar.current.dateComponents([.day], from: startsDate!, to: endsDate!).day
        let leftDay = Calendar.current.dateComponents([.day], from: currentDate, to: endsDate!).day
        let progressBarWidth = (Float(leftDay!)/Float(totalDays!))
        
        let progressViewDone = UIScreen.main.bounds.width * CGFloat(1 - progressBarWidth)
        let labelBoxWidth = labelBox.frame.width
        if progressViewDone < labelBoxWidth{
            rightMargin.constant = UIScreen.main.bounds.width - labelBoxWidth
            triangleRightConstraint.constant = labelBoxWidth - CGFloat(progressViewDone) - 5
        } else if progressViewDone > UIScreen.main.bounds.width - 20 {
            rightMargin.constant = 0
            triangleRightConstraint.constant = CGFloat(progressViewDone)
        } else {
            rightMargin.constant = UIScreen.main.bounds.width*CGFloat(progressBarWidth) - 20
        }
        progressView.setProgress(1 - progressBarWidth, animated: true)
    }
    /**
     Method to make UIImages round
     */
    func makeRound(){
        
        logoImage.layer.borderWidth = 0
        logoImage.layer.masksToBounds = false
        logoImage.layer.cornerRadius = logoImage.frame.height/2
        logoImage.clipsToBounds = true
    }
    /**
     method to bind progress cell view data
     */
    override func setData(model: BriefIntroResponse?, row: Int){
        if model != nil {
            self.briefIntroDataModel = model
            setprogress()
            logoImage.sd_setImage(with: URL(string: briefIntroDataModel.brand.logo ?? ""), placeholderImage: UIImage(named: "ic_transparent_logo"))
            let startsOn = briefIntroDataModel.startsOn?.components(separatedBy: " ").first
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let showDate = inputFormatter.date(from: startsOn!)
            inputFormatter.dateFormat = "dd/MM/yy"
            let resultString = inputFormatter.string(from: showDate!)
            startDate.text = resultString
            feedTitle.text = briefIntroDataModel.feedTitle!.uppercased()
        }
    }
    
}
