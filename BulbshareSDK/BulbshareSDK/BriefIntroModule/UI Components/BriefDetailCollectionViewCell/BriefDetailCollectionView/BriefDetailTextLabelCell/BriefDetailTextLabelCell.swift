import UIKit

class BriefDetailTextLabelCell: BriefIntroBaseCollectionCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var textLabl: UILabel!
    var briefIntroDataModel: BriefIntroResponse!
    
    // MARK: - Life cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /**
     method to bind detail text label cell data
     */
    override func setData(model: BriefIntroResponse?, row: Int){
        if model != nil {
            self.briefIntroDataModel = model
            if row == 1{
                textLabl.text = briefIntroDataModel.bannerText
            } else {
                textLabl.text = briefIntroDataModel.dataDescription
            }
            
            
        }
        
    }
}
