import UIKit

class BriefDetailImageCell: BriefIntroBaseCollectionCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var briefDetailImage: UIImageView!
    @IBOutlet weak var briefImageText: UILabel!
    var briefIntroDataModel: BriefIntroResponse!
    // MARK: - Life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    /**
     method to bind data
     */
    override func setData(model: BriefIntroResponse?, row: Int){
        if model != nil {
            self.briefIntroDataModel = model
            if row == 2{
                DispatchQueue.main.async {
                    self.briefDetailImage.sd_setShowActivityIndicatorView(true)
                    self.briefDetailImage.sd_setIndicatorStyle(.gray)
                    self.briefDetailImage.sd_setImage(with: URL(string: self.briefIntroDataModel.ad1_Image ?? ""), placeholderImage: nil)
                    
                }
                briefImageText.isHidden = true
                
            } else {
                DispatchQueue.main.async {
                    self.briefDetailImage.sd_setShowActivityIndicatorView(true)
                    self.briefDetailImage.sd_setIndicatorStyle(.gray)
                    self.briefDetailImage.sd_setImage(with: URL(string: self.briefIntroDataModel.ad2_Image ?? ""), placeholderImage: nil)
                    
                }
                briefImageText.isHidden = false
                briefImageText.text = briefIntroDataModel.ad2_Text
            }
        }
    }
}
