import Foundation

class BriefIntroPresenter{
    // MARK: - Variables
    fileprivate weak var delegate: BriefIntroDelegate?
    fileprivate var briefIntroManager: BriefIntroManager = BriefIntroManager()
    fileprivate var bsByBriefsManager: BSByBriefsManager = BSByBriefsManager()
    
    // MARK: - Initializers
    init(delegate: BriefIntroDelegate) {
        self.delegate = delegate
    }
    /**
     this method is used to map to brief intro
     */
    func getBriefIntro(briefRefID: String){
        briefIntroManager.getBriefIntro(briefref: briefRefID) { [weak self] (response, error) in
            self?.delegate?.hideLoader()
            if let responseData = response {
                if responseData.canSubmitBulbshares ?? false {
                    self?.getBSByBriefs(briefRefID: briefRefID, model: responseData)
                } else{
                    self?.mapBriefIntroView(model: responseData)
                }
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    
    /**
     this method is used to map to bulbshare by briefs
     */
    func getBSByBriefs(briefRefID: String, model: BriefIntroResponse){
        self.delegate?.showLoader()
        bsByBriefsManager.getBSByBriefs(briefref: briefRefID) { [weak self] (response, error) in
            self?.delegate?.hideLoader()
            if let responseData = response {
                self?.delegate?.getBSByBriefsData(data: responseData)
                self?.mapBriefIntroView(model: model, gridModel: responseData)
            } else if let errorTemp = error as? SDKError {
                self?.mapBriefIntroView(model: model)
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    
    /**
     this method is used to map the likeUnlikeBrief
     */
    func getLikeUnlike(ref: String,type: String){
        briefIntroManager.likeUnlikeBrief(ref: ref, type: type) { [weak self] (response,error) in
            if response != nil{
                Logger.debug(arg: "get Like Unlike: \(response)")
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    func mapBriefIntroView(model: BriefIntroResponse,gridModel: [BulbShareByBriefResponse]? = nil){
        let result = BriefIntroMapper.map(model: model, gridModel: gridModel)
        let responseData = result.0
        let row = result.1
        self.delegate?.setupView(data: responseData, numberOfRows: row)
    }
}
