class CommentBriefPresenter{
    
    // MARK: - Variables
    fileprivate weak var delegate: CommentBriefDelegate?
    fileprivate var commentBriefManager: CommentBriefManager = CommentBriefManager()
    fileprivate var submitBfCommentManager: SubmitBfCommentManager = SubmitBfCommentManager()
    fileprivate var getBriefLikesManager: GetBriefLikesManager = GetBriefLikesManager()
    fileprivate var removeBfCommentManager: RemoveBfCommentManager = RemoveBfCommentManager()
    fileprivate var likeUnlikeBriefManager: LikeUnlikeBriefManager = LikeUnlikeBriefManager()
    
    // MARK: - Initializers
    init(delegate: CommentBriefDelegate) {
        self.delegate = delegate
    }
    /**
     This method is used to map the comment brief
     */
    func getCommentBrief(briefRefID: String){
        commentBriefManager.getCommentBrief(ref: briefRefID) { [weak self] (response, error) in
            if let err = error {
                Logger.debug(arg: "the error is \(err)")
                self?.delegate?.failure(error: "")
            } else {
                if let responseData = response {
                    self?.delegate?.getCommentBriefData(data: responseData)
                }
            }
        }
       }
    func getLikeUnlike(ref: String,type: String){
        likeUnlikeBriefManager.likeBrief(ref: ref, type: type) { response, error in
            if let response = response{
                Logger.debug(arg: "\(response)")
                self.delegate?.likeButtonTappedResponse(response: true)
            } else{
                if let error = error {
                    Logger.debug(arg: "\(String(describing: error))")
                    self.delegate?.failure(error: "")
                }
                
            }
        }
    }
    
    func getBriefLikes(ref: String){
        getBriefLikesManager.getBriefLikes(ref: ref) { [weak self] (response, error) in
                    if let err = error {
                        Logger.debug(arg: "the error is \(err)")
                        self?.delegate?.failure(error: "Error in liked people")
                    } else {
                        if let responseData = response {
                            self?.delegate?.getBriefLikesData(data: responseData)
                        }
                    }
                }

    }
    
    func deleteRemoveBfComment(commentRef: String){
        removeBfCommentManager.deleteRemoveBfComment(commentRef: commentRef) { [weak self] (response, error) in
                    if let err = error {
                        Logger.debug(arg: "the error is \(err)")
                        self?.delegate?.failure(error: "")
                    } else {
                        if let responseData = response {
                            Logger.debug(arg: "Presenter Response Model \(responseData)")
                        }
                    }
                }
    }
    
    func postSubmitBfComment(briefRefID: String, comment: String){
        submitBfCommentManager.postSubmitBfComment(ref: briefRefID, comment: comment) { [weak self] (response, error) in
            if let err = error {
                Logger.debug(arg: "the error is \(err)")
                self?.delegate?.failure(error: "")
            } else {
                if let responseData = response {
                    Logger.debug(arg: "Presenter Response Model \(response)")
                    //self?.delegate?.getSubmitBfCommentData(data: responseData)
                    self?.getCommentBrief(briefRefID: briefRefID)
                }
            }
        }
        
    }
       
}
