import UIKit
class CommentTableView: UITableView{
    // MARK: - Local Variables
    var commentBriefResponseData: [CommentResponse]?
    var getBriefLikesData: GetBriefLikeResponse?
    var userRef: String = ""
    weak var commentDelegate: CommentBriefDelegate!
    
    // MARK: - Life Cycle Methods
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        self.allowsMultipleSelectionDuringEditing = true
        let keychainAccessHandler = KeychainAccessHandler()
        userRef = keychainAccessHandler.getUserRef() ?? ""
        self.register(UINib(nibName: BriefIntro.LikeTableViewCell, bundle: SDKBundles.sdkBundle), forCellReuseIdentifier: BriefIntro.LikeTableViewCell)
        self.register(UINib(nibName: BriefIntro.CommentsTableViewCell, bundle: SDKBundles.sdkBundle), forCellReuseIdentifier: BriefIntro.CommentsTableViewCell)
    }
    // MARK: - Helper Methods
    func setCommentData(commentBriefData: [CommentResponse]?, delegate: CommentBriefDelegate){
        self.commentBriefResponseData = commentBriefData
        self.commentDelegate = delegate
        DispatchQueue.main.async {
            self.reloadData()
            self.scrollToRow(at: IndexPath(row: self.commentBriefResponseData?.count ?? 0, section: 0), at: .bottom, animated: true)
        }
    }
    func setGetLikeData(getBriefLikeData: GetBriefLikeResponse?){
        self.getBriefLikesData = getBriefLikeData
        DispatchQueue.main.async {
            
            self.reloadData()
        }
    }
    func deleteRow(row: Int){
        self.beginUpdates()
        self.commentBriefResponseData!.remove(at: row - 1)
        self.deleteRows(at: [IndexPath(row: row, section: 0)], with: .fade)
        self.endUpdates()
    }
}
// MARK: - TableVIew Delegate and Data Source
extension CommentTableView: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row{
        case 0:
            let cell = self.dequeueReusableCell(withIdentifier: BriefIntro.LikeTableViewCell, for: indexPath) as!  LikeTableViewCell
            if let getBriefLikesData = getBriefLikesData{
                cell.bind(model: getBriefLikesData , delegate: self.commentDelegate)
            }
            return cell
        default:
            let cell = self.dequeueReusableCell(withIdentifier: BriefIntro.CommentsTableViewCell, for: indexPath) as! CommentsTableViewCell
            if let commentBriefResponseData = commentBriefResponseData{
                cell.setData(model: commentBriefResponseData[indexPath.row-1])
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if commentBriefResponseData !=  nil{
            return (commentBriefResponseData!.count) + 1
        } else if getBriefLikesData != nil{
            return 1
        } else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 40
        } else{
            return 70
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if indexPath.row != 0 && commentBriefResponseData?[indexPath.row-1].user.userref == userRef
        {
            return true
        } else{
            return false
        }
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            self.commentDelegate.showAlert(row: indexPath.row, message: "", title: "")
        }
    }
}
