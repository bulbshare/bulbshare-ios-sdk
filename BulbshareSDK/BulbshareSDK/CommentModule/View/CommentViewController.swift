import UIKit
import KeychainAccess
@_implementationOnly import IQKeyboardManagerSwift

class CommentViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var commentTableView: CommentTableView!
    @IBOutlet weak var commentTextField: UITextField!
    
    // MARK: - Local Variables
    var presenter: CommentBriefPresenter!
    var briefRefID: String!
    var commentBriefResponseData: [CommentResponse]?
    var getBriefLikesData: GetBriefLikeResponse?
    var result: String = ""
    
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = CommentBriefPresenter(delegate: self)
        presenter.getCommentBrief(briefRefID: briefRefID)
        presenter.getBriefLikes(briefRefID: briefRefID)
        self.commentTextField.becomeFirstResponder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    // MARK: - IBActions
    @IBAction func backToNavigate(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        self.commentTextField.resignFirstResponder()
        presenter.sendButtonValidate(briefRefID: briefRefID!, comment: commentTextField.text ?? "")
        commentTextField.text = ""
    }
}

// MARK: - Comment Brief Delegate
extension CommentViewController: CommentBriefDelegate{

    func showAlert(row: Int?, message: String, title: String) {
        if row != nil{
            showAlertController(withTitle: Alert.Oops, message: Alert.WantToDeleteComment, acceptTitle: Alert.Ok, acceptCompletionBlock: {
                self.presenter.deleteRemoveBfComment(briefRefID: self.commentBriefResponseData![row!-1].bcommentref)
                self.commentTableView.deleteRow(row: row!)
            }, cancelTitle: Alert.Cancel, cancelCompletionBlock: nil)
        } else {
            showAlertController(withTitle: title, message: message, acceptTitle: Alert.Ok)
        }
    }
    
    func likeButtonTapped(likeOrUnlike: String) {
        self.presenter.getLikeUnlike(briefRefID: briefRefID, type: likeOrUnlike)
    }
    
    func getBriefLikes(data: GetBriefLikeResponse) {
        self.getBriefLikesData = data
        self.commentTableView.setGetLikeData(getBriefLikeData: self.getBriefLikesData)
    }
    
    func likeButtonTappedResponse(response: Bool) {
        if response{
            presenter.getBriefLikes(briefRefID: briefRefID)
        }
    }
    
    // MARK: - Helper Methods
    /**
     This method set the comment brief data
     */
    func getCommentBrief(data: [CommentResponse]) {
        self.commentBriefResponseData = data
        self.commentTableView.setCommentData(commentBriefData: self.commentBriefResponseData, delegate: self)
        callBackToInteractor?(true, nil)
    }
}
