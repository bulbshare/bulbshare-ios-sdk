/**
 Login callbacks from the SDK
 */
protocol BriefResponseDelegate: AnyObject {
    /**
     Callback delegate if data load successfully
     */
   func briefDataGotSuccessfully()

    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func briefThrowError(error: SDKError)
}

