import Foundation

/**
 Login callbacks from the SDK
 */
protocol BriefCommentResponseDelegate: AnyObject {
    /**
     Callback delegate if data load successfully
     */
   func briefCommentDataGotSuccessfully()

    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func briefCommentThrowError(error: SDKError)
}
