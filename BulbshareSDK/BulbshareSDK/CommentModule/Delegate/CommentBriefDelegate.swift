import Foundation
protocol CommentBriefDelegate: BaseDelegate {
    /**
     callback delegate
     */
    func getCommentBrief(data: [CommentResponse])
    func getBriefLikes(data: GetBriefLikeResponse)
    func likeButtonTapped(likeOrUnlike: String)
    func likeButtonTappedResponse(response: Bool)
    func showAlert(row: Int?, message: String, title: String)
}
 
