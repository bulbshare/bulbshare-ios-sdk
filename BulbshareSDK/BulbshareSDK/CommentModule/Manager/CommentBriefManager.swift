import Foundation
class CommentBriefManager{
    // MARK: - Variables
    fileprivate var service: CommentBriefService = CommentBriefService()
    /**
     Method to get comment brief data
     - parameter CommentBriefRequestModel
     */
    func getCommentBrief(ref: String, completion: @escaping (_ response: [CommentResponse]?,_ error: Error?) -> Void){
        let arg = CommentBriefRequestModel(briefref: ref)
        let model = APIRequest<CommentBriefRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.getCommentBrief(model: model) { response, error in
            completion(response,error)
        }
    }
    /**
     Method to get comment brief data
     - parameter SubmitBfCommentRequestModel
     */
    func postSubmitBfComment(ref: String, comment: String, _ completion: @escaping (_ response: SubmitBriefCommentResponse?,_ error: Error?) -> Void){
        let arg = SubmitBfCommentRequestModel(briefref: ref, comment: comment)
        let model = APIRequest<SubmitBfCommentRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.postSubmitBfComment(model: model) { response, error in
            completion(response,error)
        }
    }
    /**
     Method to get comment brief data
     - parameter RemoveBfCommentRequestModel
     */
    func deleteRemoveBfComment(commentRef: String, _ completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        let arg = RemoveBfCommentRequestModel(bcommentref: commentRef)
        let model = APIRequest<RemoveBfCommentRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.deleteRemoveBfComment(model: model) { response, error in
            completion(response,error)
        }
    }
    /**
     Method to get comment brief data
     - parameter GetBriefLikesRequestModel
     */
    func getBriefLikes(ref: String, _ completion: @escaping (_ response: GetBriefLikeResponse?,_ error: Error?) -> Void){
        let arg = GetBriefLikesRequestModel(briefref: ref, page: 0, count: 0, with_my_status: true)
        let model = APIRequest<GetBriefLikesRequestModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.getBriefLikes(model: model) { response, error in
            completion(response,error)
        }
    }
}
