import UIKit

class LikeTableViewCell: UITableViewCell {
    
    weak var delegate: CommentBriefDelegate?
    var isliked: Bool = false
    
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likedByLabel: UILabel!
    // MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        if isliked{
                    likeImage.image = UIImage(named: BriefIntro.likeBordered)
        
                    delegate?.likeButtonTapped(likeOrUnlike: "unlike")
                } else{
                    likeImage.image = UIImage(named: BriefIntro.liked)
                    likeImage.tintColor = UIColor.clear
                    delegate?.likeButtonTapped(likeOrUnlike: "like")
                }
                isliked = !isliked
    }
    
    @IBAction func likedByButton(_ sender: Any) {
    }
    
    func bind(model: GetBriefLikeResponse?, delegate: CommentBriefDelegate?){
        self.delegate = delegate
        if model != nil{
            isliked = model?.was_liked_by_me ?? false
                        if isliked {
                            likeImage.image = UIImage(named: BriefIntro.liked)
                        } else {
                            likeImage.image = UIImage(named: BriefIntro.likeBordered)
                        }
            
            if model?.list.count == 0 {
                self.likedByLabel.text = "Be the first to like!"
            } else if model?.list.count == 1{
                self.likedByLabel.text = "\(model?.list[0].display_name ?? "") likes this. "
            } else if model?.list.count == 2{
                self.likedByLabel.text = "\(model?.list.first?.display_name ?? "" ) & \(model?.list[1].display_name ?? "") liked this "
            } else if model?.list.count == 3 {
                self.likedByLabel.text = "\(model?.list.first?.display_name ?? "" ), \(model?.list[1].display_name ?? "") & 1 other liked this "
            } else{
                self.likedByLabel.text = "\(model?.list.first?.display_name ?? "" ) , \(model?.list[1].display_name ?? "") & \(model?.list.count ?? 0-3) others liked this "
            }
            
            
        }
        
    }
    
    
}
