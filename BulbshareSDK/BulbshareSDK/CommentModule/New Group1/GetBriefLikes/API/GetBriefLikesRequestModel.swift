import Foundation

struct GetBriefLikesRequestModel: Codable {
    let briefref: String
    let page: Int
    let count: Int
    let with_my_status: Bool
}

class GetBriefLikesRequest {
    func params(model: APIRequest<GetBriefLikesRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
    
}
