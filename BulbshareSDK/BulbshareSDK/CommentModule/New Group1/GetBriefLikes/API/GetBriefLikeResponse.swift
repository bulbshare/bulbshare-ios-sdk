import Foundation
// MARK: - Get Brief Like Response
struct GetBriefLikeResponse: Codable {
    let was_liked_by_me: Bool
    let list: [GetBriefLikesDataList]

}
struct GetBriefLikesDataList: Codable {
    let display_name: String
}
