import Foundation

enum GetBriefLikesAPI{
    case GetBriefLikes(model: APIRequest<GetBriefLikesRequestModel>)
}
extension GetBriefLikesAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .GetBriefLikes:
            return "getbrieflikes"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .GetBriefLikes(let model):
            return GetBriefLikesRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
