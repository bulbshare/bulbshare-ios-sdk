import UIKit
class CommentViewInteractor {
    // MARK: - Local Variables
    fileprivate weak var delegate: BriefCommentResponseDelegate!
    // MARK: - Initializer
    init(delegate: BriefCommentResponseDelegate) {
        self.delegate = delegate
    }
    /**
     This method show comment view
     */
    func showCommentView(navigation: UINavigationController, briefID: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: Feeds.Feed, bundle: SDKBundles.sdkBundle)
        let vc = storyBoard.instantiateViewController(withIdentifier: BriefIntro.CommentViewController) as! CommentViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.briefRefID = briefID
        vc.callBackToInteractor  = { [weak self] (value, error) in
            if value {
                self?.delegate.briefCommentDataGotSuccessfully()
            } else if let error = error {
                self?.delegate.briefCommentThrowError(error: error)
            }
        }
        navigation.present(vc, animated: false, completion: nil)
    }
}
