import Foundation

class CommentBriefPresenter {
    
    // MARK: - Variables
    fileprivate weak var delegate: CommentBriefDelegate?
    fileprivate var commentBriefManager: CommentBriefManager = CommentBriefManager()
    fileprivate var likeUnlikeBriefManager: LikeUnlikeBriefManager = LikeUnlikeBriefManager()
    
    // MARK: - Initializers
    init(delegate: CommentBriefDelegate) {
        self.delegate = delegate
    }
    /**
     This method is used to map the comment brief
     */
    func getCommentBrief(briefRefID: String){
        commentBriefManager.getCommentBrief(ref: briefRefID) { [weak self] (response, error) in
            if let responseData = response {
                self?.delegate?.getCommentBrief(data: responseData)
            } else if let errorTemp = error as? SDKError,error != nil {
                self?.delegate?.showError(error: errorTemp)
            }
            
        }
    }
    func getLikeUnlike(briefRefID: String,type: String){
        likeUnlikeBriefManager.likeBrief(ref: briefRefID, type: type) { [weak self] response, error in
            if response != nil {
                self?.delegate?.likeButtonTappedResponse(response: true)
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    
    func getBriefLikes(briefRefID: String){
        commentBriefManager.getBriefLikes(ref: briefRefID) { [weak self] (response, error) in
                if let responseData = response {
                    self?.delegate?.getBriefLikes(data: responseData)
                } else if let errorTemp = error as? SDKError {
                    self?.delegate?.showError(error: errorTemp)
                }
        }
    }
    
    func deleteRemoveBfComment(briefRefID: String){
        commentBriefManager.deleteRemoveBfComment(commentRef: briefRefID) { [weak self] (_, error) in
            if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    
    func postSubmitBfComment(briefRefID: String, comment: String){
        commentBriefManager.postSubmitBfComment(ref: briefRefID, comment: comment) { [weak self] (response, error) in
            if response != nil {
                self?.getCommentBrief(briefRefID: briefRefID)
            } else if let errorTemp = error as? SDKError {
                self?.delegate?.showError(error: errorTemp)
            }
        }
    }
    
    func sendButtonValidate(briefRefID: String, comment: String) {
        if comment.count == 0{
            self.delegate?.showAlert(row: nil, message: Alert.PleaseEnterYourComment, title: Alert.Oops)
        } else if comment.count >= 500{
            self.delegate?.showAlert(row: nil, message: Alert.YourCommentIsTooLong, title: Alert.Oops)
        } else{
            postSubmitBfComment(briefRefID: briefRefID, comment: comment)
        }
    }
    
}
