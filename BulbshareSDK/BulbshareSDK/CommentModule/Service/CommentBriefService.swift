import Foundation
class CommentBriefService {
    // MARK: - Variables
    private let commentBriefRouter = ServiceManager<CommentBriefAPI>()
    private let submitBfCommentRouter = ServiceManager<SubmitBfCommentAPI>()
    private let removeBfCommentRouter = ServiceManager<RemoveBfCommentAPI>()
    private let getBriefLikesRouter = ServiceManager<GetBriefLikesAPI>()
    
    func getCommentBrief(
        model: APIRequest<CommentBriefRequestModel>,
        completion: @escaping (_ response: [CommentResponse]?,
                               _ error: Error?) -> Void) {
        commentBriefRouter.request(.CommentBrief(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<[CommentResponse]>.self, from: response)
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
    func postSubmitBfComment(
        model: APIRequest<SubmitBfCommentRequestModel>,
        completion: @escaping (_ response: SubmitBriefCommentResponse?,
                               _ error: Error?) -> Void) {
        submitBfCommentRouter.request(.SubmitBfComment(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<SubmitBriefCommentResponse>.self, from: response)
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
    func deleteRemoveBfComment(
        model: APIRequest<RemoveBfCommentRequestModel>,
        completion: @escaping (_ response: Bool?,
                               _ error: Error?) -> Void) {
        removeBfCommentRouter.request(.RemoveBfComment(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard (response as? Data) != nil else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                completion(true, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
    func getBriefLikes(
        model: APIRequest<GetBriefLikesRequestModel>,
        completion: @escaping (_ response: GetBriefLikeResponse?,
                               _ error: Error?) -> Void) {
        getBriefLikesRouter.request(.GetBriefLikes(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<GetBriefLikeResponse>.self, from: response)
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
}
