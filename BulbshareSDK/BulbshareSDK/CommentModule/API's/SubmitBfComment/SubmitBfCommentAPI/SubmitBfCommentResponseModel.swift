import Foundation
//MARK: - SubmitBfCommentResponseModel
struct SubmitBriefCommentResponse: Codable {
    let bcommentref: String
}
