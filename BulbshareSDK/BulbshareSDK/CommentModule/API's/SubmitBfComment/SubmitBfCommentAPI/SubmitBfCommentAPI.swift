import Foundation

enum SubmitBfCommentAPI{
    case SubmitBfComment(model: APIRequest<SubmitBfCommentRequestModel>)
}
extension SubmitBfCommentAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .SubmitBfComment:
            return "submitbfcomment"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .SubmitBfComment(let model):
            return SubmitBfCommentRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
