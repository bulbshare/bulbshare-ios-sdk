import Foundation

struct SubmitBfCommentRequestModel: Codable {
    let briefref: String
    let comment: String
}

class SubmitBfCommentRequest {
    func params(model: APIRequest<SubmitBfCommentRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
