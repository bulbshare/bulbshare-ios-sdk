import Foundation

struct RemoveBfCommentRequestModel: Codable {
    let bcommentref: String
}

class RemoveBfCommentRequest {
    func params(model: APIRequest<RemoveBfCommentRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
