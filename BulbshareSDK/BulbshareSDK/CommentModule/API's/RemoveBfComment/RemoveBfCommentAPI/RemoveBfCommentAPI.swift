import Foundation

enum RemoveBfCommentAPI{
    case RemoveBfComment(model: APIRequest<RemoveBfCommentRequestModel>)
}
extension RemoveBfCommentAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .RemoveBfComment:
            return "removebfcomment"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .delete
    }
    
    var parameters: Parameters? {
        switch self{
        case .RemoveBfComment(let model):
            return RemoveBfCommentRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
