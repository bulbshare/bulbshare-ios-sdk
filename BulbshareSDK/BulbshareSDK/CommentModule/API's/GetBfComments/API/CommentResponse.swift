import Foundation
// MARK: - Comment Response
struct CommentResponse: Codable {
    let bcommentref: String
    let user: CommentUser
    let comment: String
    let likes_count: Int
    let submitted_on, is_blocked_by_me: String
}

// MARK: - Comment User
struct CommentUser: Codable {
    let userref: String?
    let name: String?
    let first_name: String?
    let last_name: String?
    let display_name: String?
    let referral_code: String?
    let referraluserid: String?
    let avatar: String?
    let custom_background: String?
    let ethnicity: String?
    let phone_number: String?
    let country_id: Int?
    let country_name: String?
    let city_id: Int?
    let city_name: String?
    let bio: String?
    let likes_count: Int?
    let followers_count: Int?
    let bulbshares_count: Int?
    let follow_friends_count: Int?
    let follow_brands_count: Int?
    let facebook_friends_count: Int?
    let apple_id: String?
    let google_plusfollowers_count: Int?
    let twitter_id: String?
    let twitter_followers_count: Int?
    let instagram_id: String?
    let instagram_followers_count: Int?
    let youtube_id: String?
    let youtube_subscribers_count: Int?
    let social_reach: Int?
    let status: String?
    let is_email_verified: String?
    let last_connection: String?
    let deactivated_reason: String?

    enum CodingKeys: String, CodingKey {

        case userref = "userref"
        case name = "name"
        case first_name = "first_name"
        case last_name = "last_name"
        case display_name = "display_name"
        case referral_code = "referral_code"
        case referraluserid = "referraluserid"
        case avatar = "avatar"
        case custom_background = "custom_background"
        case ethnicity = "ethnicity"
        case phone_number = "phone_number"
        case country_id = "country_id"
        case country_name = "country_name"
        case city_id = "city_id"
        case city_name = "city_name"
        case bio = "bio"
        case likes_count = "likes_count"
        case followers_count = "followers_count"
        case bulbshares_count = "bulbshares_count"
        case follow_friends_count = "follow_friends_count"
        case follow_brands_count = "follow_brands_count"
        case facebook_friends_count = "facebook_friends_count"
        case apple_id = "apple_id"
        case google_plusfollowers_count = "google_plusfollowers_count"
        case twitter_id = "twitter_id"
        case twitter_followers_count = "twitter_followers_count"
        case instagram_id = "instagram_id"
        case instagram_followers_count = "instagram_followers_count"
        case youtube_id = "youtube_id"
        case youtube_subscribers_count = "youtube_subscribers_count"
        case social_reach = "social_reach"
        case status = "status"
        case is_email_verified = "is_email_verified"
        case last_connection = "last_connection"
        case deactivated_reason = "deactivated_reason"
    }

//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        userref = try values.decodeIfPresent(String.self, forKey: .userref)
//        name = try values.decodeIfPresent(String.self, forKey: .name)
//        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
//        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
//        display_name = try values.decodeIfPresent(String.self, forKey: .display_name)
//        referral_code = try values.decodeIfPresent(String.self, forKey: .referral_code)
//        referraluserid = try values.decodeIfPresent(String.self, forKey: .referraluserid)
//        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
//        custom_background = try values.decodeIfPresent(String.self, forKey: .custom_background)
//        ethnicity = try values.decodeIfPresent(String.self, forKey: .ethnicity)
//        phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
//        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
//        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
//        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
//        city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
//        bio = try values.decodeIfPresent(String.self, forKey: .bio)
//        likes_count = try values.decodeIfPresent(Int.self, forKey: .likes_count)
//        followers_count = try values.decodeIfPresent(Int.self, forKey: .followers_count)
//        bulbshares_count = try values.decodeIfPresent(Int.self, forKey: .bulbshares_count)
//        follow_friends_count = try values.decodeIfPresent(Int.self, forKey: .follow_friends_count)
//        follow_brands_count = try values.decodeIfPresent(Int.self, forKey: .follow_brands_count)
//        facebook_friends_count = try values.decodeIfPresent(Int.self, forKey: .facebook_friends_count)
//        apple_id = try values.decodeIfPresent(String.self, forKey: .apple_id)
//        google_plusfollowers_count = try values.decodeIfPresent(Int.self, forKey: .google_plusfollowers_count)
//        twitter_id = try values.decodeIfPresent(String.self, forKey: .twitter_id)
//        twitter_followers_count = try values.decodeIfPresent(Int.self, forKey: .twitter_followers_count)
//        instagram_id = try values.decodeIfPresent(String.self, forKey: .instagram_id)
//        instagram_followers_count = try values.decodeIfPresent(Int.self, forKey: .instagram_followers_count)
//        youtube_id = try values.decodeIfPresent(String.self, forKey: .youtube_id)
//        youtube_subscribers_count = try values.decodeIfPresent(Int.self, forKey: .youtube_subscribers_count)
//        social_reach = try values.decodeIfPresent(Int.self, forKey: .social_reach)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//        is_email_verified = try values.decodeIfPresent(String.self, forKey: .is_email_verified)
//        last_connection = try values.decodeIfPresent(String.self, forKey: .last_connection)
//        deactivated_reason = try values.decodeIfPresent(String.self, forKey: .deactivated_reason)
//    }

}
