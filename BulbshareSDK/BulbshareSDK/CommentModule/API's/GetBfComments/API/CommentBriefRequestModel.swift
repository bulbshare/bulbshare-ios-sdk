import Foundation

struct CommentBriefRequestModel: Codable {
    let briefref: String
}

class CommentBriefRequest {
    func params(model: APIRequest<CommentBriefRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
