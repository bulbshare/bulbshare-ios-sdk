import Foundation

enum CommentBriefAPI{
    case CommentBrief(model: APIRequest<CommentBriefRequestModel>)
}
extension CommentBriefAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .CommentBrief:
            return "getbfcomments"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .CommentBrief(let model):
            return CommentBriefRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
