import Foundation
struct AuthenticationRequestModel: Codable {
    let email: String
    let password: String
    let deviceType: Int
    let deviceUniqueId: String
    let deviceOs: String
    let deviceName: String
    let appVersion: String
    let appName: String
    
    private enum CodingKeys: String, CodingKey {
        case email = "email"
        case password = "password"
        case deviceType = "device_type"
        case deviceUniqueId = "device_unique_id"
        case deviceOs = "device_os"
        case deviceName = "device_name"
        case appVersion = "app_version"
        case appName = "app_name"
    }
}

class AuthenticationRequest {
    func params(model: APIRequest<AuthenticationRequestModel>) -> [String: AnyObject] {
        do {
            let encoded = try JSONEncoder().encode(model)
            return try String.init(decoding: encoded, as: UTF8.self).convertJsonToDictionary()
        } catch {
            Logger.debug(arg: "\(error)")
        }
        return [String: AnyObject]()
    }
}
