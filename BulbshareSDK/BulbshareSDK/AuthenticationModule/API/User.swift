import Foundation
struct User: Decodable {
    let userref: String
    let deviceref: String
    let authSecret: String
    private enum CodingKeys: String, CodingKey {
        case userref = "userref"
        case deviceref = "deviceref"
        case authSecret = "auth_secret"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userref = try values.decode(String.self, forKey: .userref)
        deviceref = try values.decode(String.self, forKey: .deviceref)
        authSecret = try values.decode(String.self, forKey: .authSecret)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(userref, forKey: .userref)
        try container.encode(deviceref, forKey: .deviceref)
        try container.encode(authSecret, forKey: .authSecret)
    }
}
