import Foundation

/**
 Enum Login API
 
 - case login: LoginRequestModel
 */
 enum AuthenticationAPI {
    case authenticate(model: APIRequest<AuthenticationRequestModel>)
}
extension AuthenticationAPI: EndPointType {
   
    var parameters: Parameters? {
        switch self {
        case .authenticate(let model):
          return  AuthenticationRequest().params(model: model)
        }
    }

    var additionalHeaders: Bool? {
       return false
    }

    var endpoint: String {
        switch self {
        case .authenticate:
          return "login"
        }
    }

    var httpMethod: HTTPMethod {
        return .post

    }
}
