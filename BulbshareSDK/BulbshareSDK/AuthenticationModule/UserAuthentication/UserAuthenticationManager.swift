import Foundation
class UserAuthenticationManager {
    fileprivate var service: UserAuthenticationService = UserAuthenticationService()
    /**
     method for authenticate user into the sdk
     
     - parameter extref: extref of user who wants to login into the SDK
     */
    func authenticate(input: BSAuthenticationInput, completion: @escaping (_ response: User?, _ error: Error?) -> Void) {
        let device = DeviceManager().device()
        let args = AuthenticationRequestModel(email: input.email, password: input.password, deviceType: device.deviceType, deviceUniqueId: device.device_unique_id, deviceOs: device.device_os, deviceName: device.device_name, appVersion: sdkConfig.appVersion, appName: sdkConfig.appName)
        let model = APIRequest<AuthenticationRequestModel>.init(deviceref: nil, app_id: sdkConfig.appId, args: args)
        self.service.makeAuthenticationAPICall(model: model) { (data, error) in
            completion(data, error)
        }
    }
    
    /**
     method for logout from the sdk
     */
    func logout() {
        UserDefault.resetDefaults()
    }
    
}
