import Foundation

/**
 Login callbacks from the SDK
 */
protocol UserAuthenticationDelegate: AnyObject {
    /**
     Callback delegate if login successfully
     */
   func authenticateSuccessfully()

    /**
     Callback delegate for throwing error, if any
     
     - parameter error: Error
     */
    func throwError(error: Error)
}
