import Foundation
import KeychainAccess

class UserAuthenticationInteractor {
    fileprivate weak var delegate: UserAuthenticationDelegate!
    fileprivate lazy var loginManager: UserAuthenticationManager = {
        let manager = UserAuthenticationManager()
        return manager
    }()

    init(delegate: UserAuthenticationDelegate) {
        self.delegate = delegate
    }
    // handle functionality for user authentication
    func authenticate(input: BSAuthenticationInput) {
        loginManager.authenticate(input: input) {[weak self] (authModel, error) in
            if let weakSelf: UserAuthenticationInteractor = self {
                guard error == nil else {
                    weakSelf.delegate.throwError(error: error!)
                    return
                }
                KeychainAccessHandler().saveUser(model: authModel!)
                weakSelf.delegate.authenticateSuccessfully()
            }
        }
    }
    func checkIfUserAlreadyLoggedin() -> Bool {
        let keychainAccessHandler = KeychainAccessHandler()
        let result = keychainAccessHandler.isUserAuthorized()
        return result
    }
    func checkIfUserAuthenticated() -> Bool {
        guard   UserDefault.getDecryptedData(key: Keys.authorization) != nil else {
            return false
        }
        return true
    }

    func logout() {
        self.loginManager.logout()
    }
}
