import Foundation
class UserAuthenticationService {
    private let router = ServiceManager<AuthenticationAPI>()
    func makeAuthenticationAPICall(
                  model: APIRequest<AuthenticationRequestModel>,
                  completion: @escaping (_ response: User?,
                  _ error: Error?) -> Void) {
        router.request(.authenticate(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard let response = response as? Data else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseData = try jsonDecoder.decode(APIResponse<User>.self, from: response)
                completion(responseData.data, nil)
            } catch {
                completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
            }
        }
    }
}
