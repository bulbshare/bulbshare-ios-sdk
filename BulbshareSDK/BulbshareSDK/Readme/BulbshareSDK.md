# **Bulbshare SDK**

Bulbshare Library is a Ios Library which Collaborate with the causes you care about and
the brands you love from the palm of your hand. Follow your friends and be inspired by Bulbshares submitted from a community of
creative collaborators from across the globe.

Use Bulbshare to:

- connect with the brands and causes you care about
- share multiple images and video
- discover Bulbshares from your friends and followers
- access exclusive rewards and opportunities

## Compatibility

1. iOS 12+
2. Xcode 13.0+

## SDK Installation

 Add **BulbshareSDK.framework** for Application target.
 
 
## Project Dependencies

Following are the dependencies of the project:

|Dependency|Version|
|----------|-------|
 
|**pod 'SDWebImage', '~> 4.0'**| This library provides an async image downloader with cache support. 

|**pod 'SwiftLint'**| SwiftLint enforces the style guide rules that are generally accepted by the Swift community.

|**pod 'KeychainAccess'**| KeychainAccess is a simple Swift wrapper for Keychain that works on iOS.

|**pod 'IQKeyboardManagerSwift'**| Often while developing an app, We ran into an issues where the iPhone keyboard slide up and cover the UITextField/UITextView. IQKeyboardManager allows you to prevent issues of the keyboard sliding up and cover UITextField/UITextView without needing you to enter any code and no additional setup required.
 
|**pod 'Koloda'**| KolodaView is a class designed to simplify the implementation of animate like cards on iOS.
 
|**pod 'MBProgressHUD'**| MBProgressHUD is an iOS drop-in class that displays a translucent HUD with an indicator.
 


## Project Description

--------------------------------------------------------------------------------------


## SDK Usage:

You can integrate the SDK in your application by first importing the sdk in your project,
then by choosing any of the following methods after authenticating to sdk.

## Authenticate to BulbShare
To use the BulbShare feature we need to Authenticate the client first by using BulbshareSdkClient class instance and call the authenticate method.


## Prepare Application

Make your ViewController class

	import UIKit
	import BulbshareSDK
	
	class ViewController: UIViewController {}


Create BulbshareSdkClient instance by providing BSSDKConfig

	let sharedinst = BulbshareSDK.shared(config: BSSDKConfig()
	
Create to object and pass the required params i.e email and password in viewDidLoad and call authenticate method using shared instance

		let obj = BSAuthenticationInput.init(email: "swati.agarwal@unthinkable.co", 	password: "sdktest903$")
        sharedinst.authenticate(input: obj)
        
## Calling the BulbShare Activity

Or we can directly call BulbShareActivity

	sharedinst.showFeed(navigation: navigation, input: BSBriefInput(privateBrandId: 73))


## Error Handling 

For error handling, refer the [ErrorHandling.md](ErrorHandling.md)


## Changelog 

For change log, refer the [Changelog.md](Changelog.md)

