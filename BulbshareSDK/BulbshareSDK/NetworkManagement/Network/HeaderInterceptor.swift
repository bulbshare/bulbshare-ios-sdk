import Foundation

protocol  HeaderProtocol {
    static func accessTokenHeader(apiPath: String,json: String) -> HTTPHeaders?
}

class HeaderInterceptor: HeaderProtocol {
    static func accessTokenHeader(apiPath: String,json: String) -> HTTPHeaders? {
        let keychainAccessHandler = KeychainAccessHandler()
        guard  let secret = keychainAccessHandler.getUserSecret(), let clientId = keychainAccessHandler.getUserRef() else {
            return nil
        }
        let deocdingManager = BSDecodingManager()
        let requestId =  "\(Date().timeIntervalSince1970*1000)"
        let requestIdMD5 = deocdingManager.hashedMD5Vaule(with: requestId + clientId)
        guard  let md5 = requestIdMD5 else {
            return nil
        }
        let hash = deocdingManager.hashedSha256Value(withSecret: secret, andData: ("\(apiPath):"+"\(json):"+"\(md5)"))
        guard  let hash = hash else {
            return nil
        }
        let authentication_header = clientId + ":" + hash + ":" + md5
        let basicAuthCredentials =   authentication_header.data(using: .utf8)
        guard let authorizationToken = basicAuthCredentials?.base64EncodedString() else {
            return nil
        }
        Logger.debug(arg: "auth token \(authorizationToken)")
        return ["Authorization": "Basic \(authorizationToken)"]
        
    }
}
