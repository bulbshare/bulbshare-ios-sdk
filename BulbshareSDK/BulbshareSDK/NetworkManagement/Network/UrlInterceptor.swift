import Foundation
class URLInterceptor {
    func buildRequest(base: String? = nil, from route: EndPointType) -> URLRequest {
           let apiPath = route.path+route.endpoint
           let baseURL: URL = (base != nil ) ? URL(string: base!)!.appendingPathComponent(route.path+route.endpoint):  URL(string: apiPath)!
           var request = URLRequest.init(url: baseURL)
           request.httpMethod = route.httpMethod.rawValue
           if  let json = route.parameters?.convertDictionaryToJson()! {
               if route.additionalHeaders == true {
                   request.addHTTPHeader(HeaderInterceptor.accessTokenHeader(apiPath: apiPath, json: json))
               }
               request.httpBody = json.data(using: .utf8)
           }
           return request
       }
    
    
    func buildMultiPartRequest(baseURL: String, from route: EndPointType) -> URLRequest {
           var request = URLRequest.init(url: URL(string: baseURL)!)
           request.httpMethod = route.httpMethod.rawValue
           if  let json = route.parameters?.convertDictionaryToJson()! {
               request.httpBody = json.data(using: .utf8)
           }
           return request
       }
    
    /**
     this method configure URLRequest's parameters
     */
    func configureParamters(urlParameters: Parameters?, request: inout URLRequest) throws {
        do {
            try URLParameterEncoder.encode(urlRequest: &request, with: urlParameters)
            
        } catch {
            throw error
        }
    }
    /**
     this method configure JSONRequest's parameters
     */
    func configurePostParamters(urlParameters: Parameters?, request: inout URLRequest) throws {
        do {
            try JSONParameterEncoder.encode(urlRequest: &request, with: urlParameters)
            
        } catch {
            throw error
        }
    }
}
