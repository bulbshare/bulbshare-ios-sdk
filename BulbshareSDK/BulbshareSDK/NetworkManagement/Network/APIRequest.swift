import Foundation
struct APIRequest<Wrapped: Encodable>: Encodable {
    let deviceref: String?
    let app_id: String
    var args: Wrapped
}
