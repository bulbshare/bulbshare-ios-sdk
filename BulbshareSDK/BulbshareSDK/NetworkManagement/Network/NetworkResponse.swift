import Foundation
protocol  NetworkProtocol {
    static func handleNetworkResponse(response: HTTPURLResponse, data: Data?) -> Result
}
class NetworkResponse: NetworkProtocol {
    static func handleNetworkResponse(response: HTTPURLResponse, data: Data?) -> Result {
        if data != nil {
            
            do {
                _ = try JSONSerialization.jsonObject(with: data!, options: [])
            } catch {
            }
        }
        switch response.statusCode {
        case 200:
            return .success
        case 400...409:
            return .failure(Validation().validate(responseData: data))
        case 500:
            return .failure(SDKError.networkError(error: .internalServerError))
        default:
            return .failure(SDKError.networkError(error: .unknownError))
        }
    }
}
