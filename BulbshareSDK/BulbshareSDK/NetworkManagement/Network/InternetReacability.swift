import Foundation
import SystemConfiguration
protocol ReachabiltyProtocol {
    static func isInternetAvailable() -> Bool
}
class InternetReachabilty: ReachabiltyProtocol {
    fileprivate static var  rechability = Rechability()
    static func isInternetAvailable() -> Bool {
        let isRechable = rechability.isInternetAvilable()
        return isRechable
    }
    static func updateRechability(rechability: Rechability) {
        InternetReachabilty.rechability = rechability
    }
}

class Rechability {
    func isInternetAvilable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
