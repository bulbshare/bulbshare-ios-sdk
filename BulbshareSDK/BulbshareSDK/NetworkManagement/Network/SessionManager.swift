import Foundation
//@_implementationOnly import TrustKit
enum Result {
    case success
    case failure(Error)
}

class SessionManager: NSObject {
    fileprivate var session: URLSession!
    func sessionManager() -> URLSession {
        let session = self.session
        return session!
    }
    public static let `default`: SessionManager = {
        let configuration = URLSessionConfiguration.default
        return SessionManager(configuration: configuration)
    }()
    init(
        configuration: URLSessionConfiguration = URLSessionConfiguration.default) {
        super.init()
        self.session = URLSession(configuration: configuration)
       // self.setSSLPinning()
    }
    deinit {
        session.invalidateAndCancel()
    }
    // for testing session
    func update(session: URLSession) {
        self.session = session
    }
//    func setSSLPinning() {
//        Logger.debug(arg: "Set SSL Pinning called...")
//        let sslKeys = SSLPinning.sslKeys
//        let sslDomain = SSLPinning.sslDomain
//        guard  sslKeys.count > 0, sslDomain.count > 0 else {return}
//        let trustKitConfig = [
//            kTSKSwizzleNetworkDelegates: false,
//            kTSKPinnedDomains: [
//                sslDomain: [
//                    kTSKPublicKeyHashes: sslKeys ]]] as [String: Any]
//        TrustKit.initSharedInstance(withConfiguration: trustKitConfig)
//    }
}
