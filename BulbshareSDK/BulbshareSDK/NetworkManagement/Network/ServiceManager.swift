import Foundation
import UIKit

class ServiceManager<EndPoint: EndPointType>: NetworkRouter {
    private let session = SessionManager.default
    fileprivate var task: URLSessionTask!
    fileprivate var isInternetShown = false
    /**
     this method is responsible to call APIs with the provided endpoints
     - Parameters:
     - route:EndPoint
     - completion:NetworkRouterCompletion
     */
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        guard  InternetReachabilty.isInternetAvailable() == true else {
            completion(nil, SDKError.networkError(error: .noInternetConnection) )
            return
        }
        do {
            isInternetShown = false
            let request =  URLInterceptor().buildRequest(base: URLs.baseURL, from: route)
            Logger.debug(arg: "request : \(request)")

            task = self.session.sessionManager().dataTask(with: request, completionHandler: { (data, response, error) in
                guard  error == nil else {
                    completion(nil, SDKError.networkError(error: .connectionTimeOut) )
                    return
                }
                if let response = response as? HTTPURLResponse {
                    do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: Any]
                        Logger.debug(arg: "response data : \(json)")
                    } catch let error as NSError {
                            print(error)
                        }
                    let result = self.handleNetworkResponse(response: response, data: data)
                    switch result {
                    case .success:
                        completion(data, error)
                    case .failure(let error):
                        Logger.debug(arg: "result error : \(error)")
                        completion(nil, error )
                    }
                }
                
            })
            self.task.resume()
        } catch {
            completion(nil, error )
        }
    }
    
    func multipartRequest(_ route: EndPoint,url: String, data: Data, completion: @escaping NetworkRouterCompletion) {
        guard  InternetReachabilty.isInternetAvailable() == true else {
            completion(nil, SDKError.networkError(error: .noInternetConnection) )
            return
        }
        do {
            isInternetShown = false
            let request =  URLInterceptor().buildMultiPartRequest(baseURL: url, from: route)

            task = self.session.sessionManager().uploadTask(with: request, from: data, completionHandler: { (data, response, error) in
                guard  error == nil else {
                    completion(nil, SDKError.networkError(error: .connectionTimeOut) )
                    return
                }
                if let response = response as? HTTPURLResponse, response.statusCode == 200  {
                    let result = self.handleNetworkResponse(response: response, data: data)
                    switch result {
                    case .success:
                        completion(true, nil)
                    case .failure(let error):
                        completion(nil, error )
                    }
                }
            })
            self.task.resume()
        } catch {
            completion(nil, error )
        }
    }
    
    /**
     this method cancels the current running task
     */
    func cancel() {
        self.task.cancel()
    }
    
    /**
     this method handles network
     */
    fileprivate func handleNetworkResponse(response: HTTPURLResponse, data: Data?) -> Result {
        let result = NetworkResponse.handleNetworkResponse(response: response, data: data)
        return result
    }
    
}
