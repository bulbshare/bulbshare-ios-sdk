import Foundation
struct APIResponse<Wrapped: Decodable>: Decodable {
    let status: String
    let code: Int
    var data: Wrapped
}
