import Foundation
protocol EndPointType {

    var path: String { get }
    var endpoint: String { get }
    var httpMethod: HTTPMethod { get }
    var parameters: Parameters? { get }
    var additionalHeaders: Bool? { get }
}
extension EndPointType {
    var path: String {
            return "/api/"
    }
}
