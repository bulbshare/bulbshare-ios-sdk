import Foundation
struct JSONParameterEncoder: ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with paramters: Parameters?) throws {
        guard urlRequest.url != nil else {
            throw SDKError.networkError(error: .missingUrl)
        }
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        if urlRequest.value(forHTTPHeaderField: "Accept") == nil {
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        }
        if  let paramters = paramters {
            let data = try JSONSerialization.data(withJSONObject: paramters, options: [])
            Logger.debug(arg: "paramters \(paramters)")
            Logger.debug(arg: "paramters \(try JSONSerialization.jsonObject(with: data, options: []))")

            urlRequest.httpBody = data
        }
    }
}
