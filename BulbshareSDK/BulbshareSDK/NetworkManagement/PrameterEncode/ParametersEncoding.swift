import Foundation
 typealias Parameters = [String: AnyObject]

// A ParameterEncoder performs one function which is to encode parameters. This method can fail so it throws an error and we need to handle.
 protocol ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with paramters: Parameters?) throws
}
