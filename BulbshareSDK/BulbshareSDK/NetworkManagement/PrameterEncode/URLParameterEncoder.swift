import Foundation
struct URLParameterEncoder: ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with paramters: Parameters?) throws {
        guard let url = urlRequest.url else {
            throw SDKError.networkError(error: .missingUrl)
        }
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }
        if urlRequest.value(forHTTPHeaderField: "Accept") == nil {
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        }
        if  let paramters = paramters {
            if var urlComponent = URLComponents.init(url: url, resolvingAgainstBaseURL: false), !paramters.isEmpty {
                urlComponent.queryItems = [URLQueryItem]()
                for (key, value) in paramters {
                    let queryItem: URLQueryItem = URLQueryItem.init(name: key, value: "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
                    urlComponent.queryItems?.append(queryItem)
                    
                }
                urlRequest.url = urlComponent.url
            }
        }
    }
    
}
