import Foundation
class DeleteBulbshareService {
    private let deleteBulbshareRouter = ServiceManager<DeleteBulbshareAPI>()
    func deleteBulbshare(
        model: APIRequest<CommonBulbshareReqModel>,
        completion: @escaping (_ response: Bool?,
                               _ error: Error?) -> Void) {
        deleteBulbshareRouter.request(.deleteBulbshare(model: model)) { (response, error) in
            guard  error == nil else {
                completion(nil, error)
                return
            }
            do {
                guard (response as? Data) != nil else {
                    completion(nil, SDKError.jsonError(reason: .jsonDecodeError))
                    return
                }
                completion(true, nil)
            }
        }
    }
}
