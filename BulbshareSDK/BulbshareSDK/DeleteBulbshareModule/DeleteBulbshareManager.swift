import Foundation
class DeleteBulbshareManager{
    
    fileprivate var service: DeleteBulbshareService = DeleteBulbshareService()
    func deleteBulbshare(ref: String, completion: @escaping (_ response: Bool?,_ error: Error?) -> Void){
        let arg = CommonBulbshareReqModel(bulbshareref: ref)
        let model = APIRequest<CommonBulbshareReqModel>.init(deviceref: KeychainAccessHandler().getDeviceRef(), app_id: sdkConfig.appId, args: arg)
        self.service.deleteBulbshare(model: model) { response, error in
            completion(response,error)
        }
    }
}
