import Foundation

enum DeleteBulbshareAPI{
    case deleteBulbshare(model: APIRequest<CommonBulbshareReqModel>)
}
extension DeleteBulbshareAPI: EndPointType{
  
    var endpoint: String {
        switch self {
        case .deleteBulbshare:
            return "removebulbshare"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters? {
        switch self{
        case .deleteBulbshare(let model):
            return CommonBulbshareRequest().params(model: model)
        }
    }
    
    var additionalHeaders: Bool? {
        return true
    }
    
}
