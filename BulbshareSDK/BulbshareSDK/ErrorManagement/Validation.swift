import Foundation

class Validation {
    fileprivate typealias ErrorReason = SDKError.ServerError
    fileprivate func checkForError(serverError: ServerError) -> ErrorReason {
        guard  let bundle = SDKBundles.sdkBundle else {
            Logger.debug(arg: "unknow error when bundle not found")
            return ErrorReason.unknownError
        }
        Logger.debug(arg: "status code server error ======= \(serverError.code)")
        let error = NSLocalizedString("\(serverError.code)", tableName: nil, bundle: bundle, value: "", comment: "")
        guard  let errorreason = ErrorReason(rawValue: error) else {
            Logger.debug(arg: "unknow error when error reason not found")
            return ErrorReason.unknownError
        }
        return  errorreason
    }
    func validate(responseData: Data?) -> Error {
        guard responseData != nil else {
            return  (SDKError.serverError(reason: .unknownError))
        }
        do {
            let decoder = JSONDecoder()
            let serverError = try decoder.decode(ServerError.self, from: responseData!)
            let error = self.checkForError(serverError: serverError)
            return  (SDKError.serverError(reason: error))
        } catch {
            return  (SDKError.jsonError(reason: .jsonDecodeError))
        }
    }
}
