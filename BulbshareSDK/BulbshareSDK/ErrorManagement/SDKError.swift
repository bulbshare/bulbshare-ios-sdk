import Foundation

/**
 localized error of Error
 
 - parameter errorDescription: String
 */
extension SDKError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .applicationError(error: let error):
            return error.localizedDescription
            
        case .networkError(let error):
            return error.localizedDescription
            
        case .serverError(let error):
            return error.localizedDescription
            
        case .jsonError(let error):
            return error.localizedDescription
        }
    }
    
    public var failureReason: String? {
        switch self {
        case .applicationError(error: let error):
            return error.failureReason
            
        case .networkError(let error):
            return error.failureReason
            
        case .serverError(let error):
            return error.failureReason
            
        case .jsonError(let error):
            return error.failureReason
        }
    }
}

/**
 localized error of ApplicationError
 
 - parameter localizedDescription: String
 */
extension SDKError.ApplicationError {
    var localizedDescription: String {
        let rawValue = self.rawValue
        return rawValue
    }
    var failureReason: String {
        let rawValue = self.localizedDescription.localised
        return rawValue
    }
}

/**
 localized error of ServerError
 
 - parameter localizedDescription: String
 */
extension SDKError.ServerError {
    var localizedDescription: String {
        let rawValue = self.rawValue
        return rawValue
    }
    var failureReason: String {
        let rawValue = self.localizedDescription.localised
        return rawValue
    }
}

/**
 localized error of JSONError
 
 - parameter localizedDescription: String
 */
extension SDKError.JSONError {
    var localizedDescription: String {
        let rawValue = self.rawValue
        return rawValue
        
    }
    var failureReason: String {
        let rawValue = self.localizedDescription.localised
        return rawValue
    }
}

/**
 localized error of NetworkError
 
 - parameter localizedDescription: String
 */
extension SDKError.NetworkError {
    var localizedDescription: String {
        let rawValue = self.rawValue
        return rawValue
        
    }
    var failureReason: String {
        let rawValue = self.localizedDescription.localised
        return rawValue
    }
}

/**
 enum of Error
 */
public enum SDKError: Error {
    /**
     enum for Application errors
     */
    public enum ApplicationError: String, Error {
        case unknownError
        case unauthorized
        case deviceIsJailBreak
    }
    
    /**
     enum for Network errors
     */
    public enum NetworkError: String, Error {
        
        case noInternetConnection
        
        case connectionTimeOut
        
        case missingUrl
        
        case unknownError
        
        case internalServerError
    }
    
    /**
     enum for Server errors
     */
    public enum ServerError: String, Error {
        case unAuthorized
        case unknownError
        case userDoesNotExist
        case invalidPassword
        case invalidArguments
        case invalidBrief
        case briefNotExist
        case briefIsRemoved
        case invalidBriefType
        case invalidComment
        case invalidBriefCommentReference
        case briefCommentNotExist
        case briefCommentRemoved
        case invalidPermissions
        case invalidItemCount
        case invalidPage
        case invalidReturnStatusValue
        case brandNotExist
        case brandRemoved
        case pollNotExist
        case invalidMediaFile
        case invalidMediaType
        case invalidPollitemID
        case failedToUploadUrl
        case invalidPollitem
        case accessDenied
        case blockedAppVersion
        case deviceRemoved
        case deviceMismatch
        case NoAuthenticationHeader
        case invalidUserReference
        case invalidToken
        case wrongUserReference
        case inactiveCredentials
        case wrongAuthSecret
        case userInactive
        case userRemoved
        case requestIdExpired
        case invalidRequest
        case wrongDeviceReference
        case invalidDeviceReference
    }
    
    /**
     enum for JSON error
     */
    public enum JSONError: String, Error {
        case jsonDecodeError
    }
    
    case applicationError(error: ApplicationError)
    case networkError(error: NetworkError)
    case serverError(reason: ServerError)
    case jsonError(reason: JSONError)
    
}

struct ServerError: Decodable {
    let status: String
    let code: Int
}
