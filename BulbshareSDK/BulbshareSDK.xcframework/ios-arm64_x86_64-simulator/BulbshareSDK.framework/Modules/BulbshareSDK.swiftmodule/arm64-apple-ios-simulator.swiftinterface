// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.1 (swiftlang-1300.0.31.4 clang-1300.0.29.6)
// swift-module-flags: -target arm64-apple-ios12.1-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name BulbshareSDK
import AVFoundation
import AVKit
@_exported import BulbshareSDK
import CommonCrypto
import CoreData
import Foundation
import ImageIO
import KeychainAccess
import MBProgressHUD
import SDWebImage
import Swift
import SystemConfiguration
import UIKit
import _Concurrency
extension BulbshareSDK.SDKError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
  public var failureReason: Swift.String? {
    get
  }
}
public enum SDKError : Swift.Error {
  public enum ApplicationError : Swift.String, Swift.Error {
    case unknownError
    case unauthorized
    case deviceIsJailBreak
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum NetworkError : Swift.String, Swift.Error {
    case noInternetConnection
    case connectionTimeOut
    case missingUrl
    case unknownError
    case internalServerError
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum ServerError : Swift.String, Swift.Error {
    case unAuthorized
    case unknownError
    case userDoesNotExist
    case invalidPassword
    case invalidArguments
    case invalidBrief
    case briefNotExist
    case briefIsRemoved
    case invalidBriefType
    case invalidComment
    case invalidBriefCommentReference
    case briefCommentNotExist
    case briefCommentRemoved
    case invalidPermissions
    case invalidItemCount
    case invalidPage
    case invalidReturnStatusValue
    case brandNotExist
    case brandRemoved
    case pollNotExist
    case invalidMediaFile
    case invalidMediaType
    case invalidPollitemID
    case failedToUploadUrl
    case invalidPollitem
    case accessDenied
    case blockedAppVersion
    case deviceRemoved
    case deviceMismatch
    case NoAuthenticationHeader
    case invalidUserReference
    case invalidToken
    case wrongUserReference
    case inactiveCredentials
    case wrongAuthSecret
    case userInactive
    case userRemoved
    case requestIdExpired
    case invalidRequest
    case wrongDeviceReference
    case invalidDeviceReference
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum JSONError : Swift.String, Swift.Error {
    case jsonDecodeError
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  case applicationError(error: BulbshareSDK.SDKError.ApplicationError)
  case networkError(error: BulbshareSDK.SDKError.NetworkError)
  case serverError(reason: BulbshareSDK.SDKError.ServerError)
  case jsonError(reason: BulbshareSDK.SDKError.JSONError)
}
public struct BSSingleBulbshareInput : Swift.Encodable {
  public init(bulbshareRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSAuthenticationInput : Swift.Encodable {
  public init(email: Swift.String, password: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSSDKConfig {
  public init()
}
public protocol BSAuthenticatioinDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessAuthentication()
}
public protocol BSBriefDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSErrorDelegate : AnyObject {
  func onSDKError(error: Swift.Error)
}
public protocol BSBulbshareDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSPollBriefDdelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessPollBriefLoaded()
}
public protocol BSBriefIntroDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefIntroLoaded()
}
public protocol BSSingleBulbshareDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessSingleBulbshareLoaded()
}
public protocol BSBriefCommentDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefCommentLoaded()
}
public protocol BSFeedDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessFeedLoaded()
}
extension UIKit.UIImageView {
  @_Concurrency.MainActor(unsafe) public func loadGif(name: Swift.String)
}
extension UIKit.UIImage {
  public class func gif(data: Foundation.Data) -> UIKit.UIImage?
  public class func gif(url: Swift.String) -> UIKit.UIImage?
  public class func gif(name: Swift.String) -> UIKit.UIImage?
}
@_hasMissingDesignatedInitializers public class BulbshareClient {
  public static func shared(config: BulbshareSDK.BSSDKConfig) -> BulbshareSDK.BulbshareClient
  weak public var authenticationDelegate: BulbshareSDK.BSAuthenticatioinDelegate?
  public func authenticate(input: BulbshareSDK.BSAuthenticationInput)
  weak public var feedDelegate: BulbshareSDK.BSFeedDelegate?
  public func showFeed(navigation: UIKit.UINavigationController, input: BulbshareSDK.BSBriefInput)
  weak public var pollBriefDelegate: BulbshareSDK.BSPollBriefDdelegate?
  public func showPollBrief(navigation: UIKit.UINavigationController, input: Swift.String)
  weak public var briefDelegate: BulbshareSDK.BSBriefDelegate?
  public func showBrief(navigation: UIKit.UINavigationController, input: BulbshareSDK.BSBriefInput)
  weak public var briefIntroDelegate: BulbshareSDK.BSBriefIntroDelegate?
  public func showBriefIntro(navigation: UIKit.UINavigationController, input: BulbshareSDK.BSBriefRefInput)
  weak public var singleBulbshareDelegate: BulbshareSDK.BSSingleBulbshareDelegate?
  public func showSingleBulbshare(navigation: UIKit.UINavigationController, input: BulbshareSDK.BSSingleBulbshareInput)
  weak public var bulbshareListDelegate: BulbshareSDK.BSBulbshareDelegate?
  public func showBulbshareList(navigation: UIKit.UINavigationController, input: BulbshareSDK.BSBulbshareListInput)
  weak public var briefCommentDelegate: BulbshareSDK.BSBriefCommentDelegate?
  public func showBriefComment(navigation: UIKit.UINavigationController, input: BulbshareSDK.BSBriefRefInput)
  @objc deinit
}
public struct BSBulbshareListInput : Swift.Encodable {
  public init(briefRef: Swift.String, count: Swift.Int, sortType: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol ImagePickerDelegate : AnyObject {
  func didSelect(image: UIKit.UIImage?)
}
@objc open class ImagePicker : ObjectiveC.NSObject {
  public init(presentationController: UIKit.UIViewController, delegate: BulbshareSDK.ImagePickerDelegate)
  public func presentImagePicker(from sourceView: UIKit.UIView, sourcetype: UIKit.UIImagePickerController.SourceType)
  @objc deinit
}
extension BulbshareSDK.ImagePicker : UIKit.UIImagePickerControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerControllerDidCancel(_ picker: UIKit.UIImagePickerController)
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerController(_ picker: UIKit.UIImagePickerController, didFinishPickingMediaWithInfo info: [UIKit.UIImagePickerController.InfoKey : Any])
}
extension BulbshareSDK.ImagePicker : UIKit.UINavigationControllerDelegate {
}
public struct BSBriefRefInput : Swift.Encodable {
  public init(briefRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol VideoPickerDelegate : AnyObject {
  func didSelect(url: Foundation.URL?)
}
@objc open class VideoPicker : ObjectiveC.NSObject {
  public init(presentationController: UIKit.UIViewController, delegate: BulbshareSDK.VideoPickerDelegate)
  public func present(from sourceView: UIKit.UIView, sourceType: UIKit.UIImagePickerController.SourceType)
  @objc deinit
}
extension BulbshareSDK.VideoPicker : UIKit.UIImagePickerControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerControllerDidCancel(_ picker: UIKit.UIImagePickerController)
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerController(_ picker: UIKit.UIImagePickerController, didFinishPickingMediaWithInfo info: [UIKit.UIImagePickerController.InfoKey : Any])
}
extension BulbshareSDK.VideoPicker : UIKit.UINavigationControllerDelegate {
}
public protocol SDKSettingsProtocol {
  static func sdkVersion() -> Swift.String
}
@_hasMissingDesignatedInitializers public class SDKSettings : BulbshareSDK.SDKSettingsProtocol {
  public static func sdkVersion() -> Swift.String
  @objc deinit
}
public struct BSBriefInput : Swift.Encodable {
  public init(privateBrandId: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
extension BulbshareSDK.SDKError.ApplicationError : Swift.Equatable {}
extension BulbshareSDK.SDKError.ApplicationError : Swift.Hashable {}
extension BulbshareSDK.SDKError.ApplicationError : Swift.RawRepresentable {}
extension BulbshareSDK.SDKError.ServerError : Swift.Equatable {}
extension BulbshareSDK.SDKError.ServerError : Swift.Hashable {}
extension BulbshareSDK.SDKError.ServerError : Swift.RawRepresentable {}
extension BulbshareSDK.SDKError.JSONError : Swift.Equatable {}
extension BulbshareSDK.SDKError.JSONError : Swift.Hashable {}
extension BulbshareSDK.SDKError.JSONError : Swift.RawRepresentable {}
extension BulbshareSDK.SDKError.NetworkError : Swift.Equatable {}
extension BulbshareSDK.SDKError.NetworkError : Swift.Hashable {}
extension BulbshareSDK.SDKError.NetworkError : Swift.RawRepresentable {}
